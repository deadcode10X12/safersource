package tronbox.guardiansfunctionality;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class LocationUpdateServer extends AsyncTask<String, String, String>{

    private Context context;
    private String verificationCode;

    public LocationUpdateServer(Context context){

        this.context = context;

    }

    /*
        params[0] = State --> Normal Or Distress
        params[1] = url where data needs to be send.
        params[2] = Data to be sent.

     */


    @Override
    protected String doInBackground(String... params) {

        try{

            HttpURLConnection httpUrlConnection = null;
            URL url = new URL("http://54.169.182.72/api/v0/sos/");
            httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setUseCaches(false);
            httpUrlConnection.setDoOutput(true);

            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
            httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
            httpUrlConnection.addRequestProperty("content-type","application/json");
            httpUrlConnection.addRequestProperty("authorization","bearer "+SharedPrefrenceStorage.GetAuthToken(context));
            httpUrlConnection.setDoOutput(true);

            if (params[0] != null) {
                OutputStreamWriter wr = new OutputStreamWriter(httpUrlConnection.getOutputStream());
                wr.write(params[0]);
                wr.flush();
            }

            InputStream responseStream = new BufferedInputStream(httpUrlConnection.getInputStream());

            BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
            String line = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = responseStreamReader.readLine()) != null)
            {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();

            String response = stringBuilder.toString();
            Log.w("ServerResponseFromServer", response);

            JSONObject jsonObject = new JSONObject(response);
            String status = jsonObject.getString("status");

              if(params[0].equals("Normal"))
            {
                  if(status.equals("success")) // user is new to the server.
                {

                }
            }
              else if(params[0].equals("Distress"))
              {
                   if(status.equals("success")) // user is new to the server.
                  {
                      SharedPrefrenceStorage.StoreActivityid(context, jsonObject.getString("id"));
                  }
              }


        } catch(IOException io){}
          catch (JSONException e) {e.printStackTrace();}

        return null;
    }

}
