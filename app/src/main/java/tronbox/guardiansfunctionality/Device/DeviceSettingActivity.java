package tronbox.guardiansfunctionality.Device;

        import android.app.Activity;
        import android.app.Dialog;
        import android.content.Context;
        import android.content.Intent;
        import android.os.Bundle;
        import android.os.Handler;
        import android.view.View;
        import android.view.Window;
        import android.view.WindowManager;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.ProgressBar;
        import android.widget.RelativeLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import tronbox.guardiansfunctionality.Home.HomeActivity;
        import tronbox.guardiansfunctionality.R;

/**
 * Created by Shrinivas Mishra on 3/29/2015.
 * Showing Device Setting screen
 */
public class DeviceSettingActivity extends Activity {
    Context myContext=DeviceSettingActivity.this;
    private int mProgressStatus=0;
    private TextView txtPercentageBattery;
    private ProgressBar prgBar;
    private Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_setting);
        LinearLayout llSafetyLevel=(LinearLayout)findViewById(R.id.llSafetyLevel);
        prgBar=(ProgressBar)findViewById(R.id.progressDeviceBar);
        txtPercentageBattery=(TextView)findViewById(R.id.txtPercentageBattery);
        llSafetyLevel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(myContext,"Select Safety Level",Toast.LENGTH_SHORT).show();
            }
        });
        RelativeLayout llEditDevice=(RelativeLayout)findViewById(R.id.rlDeviceEditName);
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        llEditDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txtEditDevice=(EditText)findViewById(R.id.txtEditDevice);
                if(txtEditDevice.isEnabled())
                {
                    txtEditDevice.setEnabled(false);
                    txtEditDevice.clearFocus();
                    txtEditDevice.setSelection(0);
                }else {
                    txtEditDevice.setEnabled(true);
                    txtEditDevice.selectAll();
                    txtEditDevice.requestFocus();
                    imm.showSoftInput(txtEditDevice, InputMethodManager.SHOW_IMPLICIT);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

                }
            }
        });
        LinearLayout llTestDevice=(LinearLayout)findViewById(R.id.llTestDevice);
        llTestDevice.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                final Dialog dialog = new Dialog(myContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.spinner_dailog);
                final ProgressBar progBar = (ProgressBar) dialog.findViewById(R.id.customProgressBar);
                dialog.show();
                progBar.setVisibility(View.VISIBLE);

                //final ProgressDialog progressDialog=ProgressDialog.show(
                //        myContext,"Device Testing","Checking Device. Please wait....",true);
                /*new Thread()
                {
                    public void run()
                    {
                        try
                        {
                            dialog.show();
                            progBar.setVisibility(View.VISIBLE);
                            sleep(1000);
                        }catch(Exception ex)
                        {
                        }
                        progBar.setVisibility(View.GONE);
                        dialog.dismiss();
                    }
                }.start();*/
            }
        });
        LinearLayout llForgetDevice=(LinearLayout)findViewById(R.id.llForgetDevice);
        llForgetDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(myContext,"Forget Device",Toast.LENGTH_SHORT).show();
            }
        });
        ImageView imgBackDS=(ImageView)findViewById(R.id.imgBackDS);
        imgBackDS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, HomeActivity.class);
                startActivity(intent);
            }
        });
        doProgress();
    }
    public void doProgress() {
        new Thread(new Runnable() {
            public void run() {
                final int presentage=0;
                while (mProgressStatus < 70) {
                    mProgressStatus += 1;
                    mHandler.post(new Runnable() {
                        public void run() {
                            prgBar.setProgress(mProgressStatus);
                            txtPercentageBattery.setText(""+mProgressStatus+"%");
                        }
                    });
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
