package tronbox.guardiansfunctionality.Device;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.R;

public class SelectNearDevice extends DialogFragment {

    ProgressDialog progressDialog;
    MobileDatabase database;
    String TAG = "HomeControlPanel";

    List<String> names = new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.select_near_device, container, false);

        adapter = new ArrayAdapter<String>(getActivity(), R.layout.opaque_text_view, names);

        ListView friendsListView = (ListView) view.findViewById(R.id.near_devices);
        friendsListView.setAdapter(adapter);

        friendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String mac = parent.getAdapter().getItem(position).toString().split("_")[1];

                Intent intent1 = new Intent("StartScan");
                intent1.putExtra("data", "connect_device");
                intent1.putExtra("mac", mac);
                getActivity().sendBroadcast(intent1);

                Toast.makeText(getActivity(), mac, Toast.LENGTH_SHORT).show();

                dismiss();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("search_mac"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String macAdd = intent.getExtras().getString("mac");
            String deviceName = intent.getExtras().getString("name");

              for(String n : names)
            {
                 if(n.equals(deviceName+"_"+macAdd))
                {
                    return;
                }
            }

            names.add(deviceName+"_"+macAdd);
            adapter.notifyDataSetChanged();
        }
    };

}
