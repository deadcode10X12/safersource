package tronbox.guardiansfunctionality.Home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import tronbox.guardiansfunctionality.R;

/**
 * Created by Shrinivas Mishra on 3/22/2015.
 * Page provide functionality for Raise Question by guardian
 */
public class GuardianQuestionActivity extends Activity
{
    Context myContext=GuardianQuestionActivity.this;
    LinearLayout llWhereGQ,llHowGQ,llWhatGQ,llCallGQ,llMessageGQ;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guardian_question);
        llWhereGQ=(LinearLayout)findViewById(R.id.llWhereGQ);
        llHowGQ=(LinearLayout)findViewById(R.id.llHowGQ);
        llWhatGQ=(LinearLayout)findViewById(R.id.llWhatGQ);
        llCallGQ=(LinearLayout)findViewById(R.id.llCallGQ);
        llMessageGQ=(LinearLayout)findViewById(R.id.llMessageGQ);
        llWhereGQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, GaurdianAnswerActivity.class);
                startActivity(intent);
            }
        });
        llHowGQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, GaurdianAnswerActivity.class);
                startActivity(intent);
            }
        });
        llWhatGQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, GaurdianAnswerActivity.class);
                startActivity(intent);
            }
        });
        ImageView imgBackGQ=(ImageView)findViewById(R.id.imgBackGQ);
        imgBackGQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
