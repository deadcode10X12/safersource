package tronbox.guardiansfunctionality.Home;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;

/**
 * Created by Shrinivas Mishra on 3/31/2015.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>
{

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private String mNavTitles[];
    private int mIcons[];
    private String mNotyNumber;
    private String name;
    private Bitmap profile;
    private String email;
    private String feedback;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener
    {
        int Holderid;
        TextView textView;
        ImageView imageView;
        ImageView profile;
        TextView Name, location;
        Context context;
        Typeface robotoFont;

        public ViewHolder(View itemView,int ViewType,Context c)
        {
            super(itemView);
            context = c;
            itemView.setClickable(true);
            itemView.setOnClickListener(this);

            robotoFont = Typeface.createFromAsset(c.getAssets(), "roboto.ttf");


              if(ViewType == TYPE_ITEM)
            {
                textView = (TextView) itemView.findViewById(R.id.rowText);
               // textView.setTypeface(robotoFont);

                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);
                if (textView.getText().toString().isEmpty())
                {
                 itemView.setBackgroundColor(Color.WHITE);
                }
                Holderid = 1;
            }

               else if(ViewType == TYPE_HEADER)
            {
                Name = (TextView) itemView.findViewById(R.id.name);
                //Name.setTypeface(robotoFont);

                location = (TextView) itemView.findViewById(R.id.location);
                //location.setTypeface(robotoFont);

                String temp = null;

                 if(SharedPrefrenceStorage.GetUserSubLocality(c) != null)
                {
                    temp = SharedPrefrenceStorage.GetUserSubLocality(c);
                }

                if(SharedPrefrenceStorage.GetUserLocality(c) != null)
                {
                     if(temp != null)
                    {
                        temp = temp + ",\n"+SharedPrefrenceStorage.GetUserLocality(c);
                    }
                      else {

                         temp = SharedPrefrenceStorage.GetUserLocality(c);
                     }

                }

                 if(temp != null)
                {
                    location.setText(temp);
                }else {
                     location.setVisibility(View.GONE);
                 }

                profile = (ImageView) itemView.findViewById(R.id.circleView);
                Holderid = 0;
            }
        }
        @Override
        public void onClick(View v)
        {
            Toast.makeText(context,"The Item Clicked is: "+getPosition(), Toast.LENGTH_SHORT).show();
        }
    }
    MyAdapter(String Titles[], int Icons[], String notification, String Name, String Email, Bitmap Profile, Context passContext){
        mNavTitles = Titles;
        mIcons = Icons;
        name = Name;
        email = Email;
        profile = Profile;
        mNotyNumber=notification;
        this.context=passContext;
    }
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false);
            ViewHolder vhItem = new ViewHolder(v,viewType,context);
            return vhItem;

        } else if (viewType == TYPE_HEADER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false);

            ViewHolder vhHeader = new ViewHolder(v,viewType,context);
            return vhHeader;
        }
        return null;
    }
    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
        if(holder.Holderid ==1)
        {
            holder.textView.setText(mNavTitles[position - 1]);
            holder.imageView.setImageResource(mIcons[position -1]);

        }else if(holder.Holderid ==0){
            holder.profile.setImageBitmap(profile);
            holder.Name.setText(name);
        }
    }
    @Override
    public int getItemCount() {
        return mNavTitles.length+1;
    }
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }
    private boolean isPositionHeader(int position) {
        return position == 0;
    }
}