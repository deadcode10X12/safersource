package tronbox.guardiansfunctionality.Home;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.Device.SelectNearDevice;
import tronbox.guardiansfunctionality.Maps.ShareLocationConfirm;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class HomeListFragment extends Fragment {

    HomeList homeListAdapter;
    Bundle data;
    int screenValue;
    MobileDatabase database;
    GuardianDetails holder = null;

    ProgressDialog progressDialog;

    String TAG = "HomeActivityLogs";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.fragment_home_list, container, false);
        database = new MobileDatabase(getActivity(), "GUARDIAN", null, 1);

        data = getArguments();
        screenValue = data.getInt("Screen");
        //dummy data setting coding here

        ArrayList<GuardianDetails> detailsArrayList = new ArrayList<GuardianDetails>();


          for(GuardianDetails guardianDetails : database.getAllGuardians())
        {
           detailsArrayList.add(guardianDetails);
        }

        boolean check = false;

          for(final GuardianDetails guardianDetails : database.getAllWard())
        {

             for(GuardianDetails g : database.getAllGuardians())
            {
                 if(g.getMobile().equals(guardianDetails.getMobile()))
                {
                    check = true;
                }

            }

             if(check == false) // not found
            {
                detailsArrayList.add(guardianDetails);
            }
               else if(check == true)
             {
                 check = false;
                 guardianDetails.setCategory(StaticMaster.Category.Both);
             }

        }



        //end here
        homeListAdapter = new HomeList(getActivity(), R.layout.custom_home_list, detailsArrayList);


        ListView friendsListView = (ListView) view.findViewById(R.id.lstHomeList);


        friendsListView.setAdapter(homeListAdapter);
        friendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

                StaticMaster.tempGuardianDetails = (GuardianDetails)parent.getAdapter().getItem(position);

                Log.w(TAG, StaticMaster.tempGuardianDetails.getStatus());

                HomeControlPanel homeControlPanel = new HomeControlPanel();
                homeControlPanel.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar);
                homeControlPanel.show(getActivity().getSupportFragmentManager().beginTransaction(), "dialog");

/*
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.home_control_panel);

                dialog.show();*/

                /*TextView txtGNameHL = (TextView) dialog.findViewById(R.id.txtGNameHL);
                txtGNameHL.setText(holder.getName());


                  if(holder.getImage() != null)
                {
                    ImageView imageView = ((CircleImagePhoto)dialog.findViewById(R.id.imgDialogHLPhoto));
                    imageView.setImageBitmap(Networking.decodeSampledBitmapFromResource(getActivity().getApplicationContext(), holder, 40, 40, imageView));

                }else {

                      Log.w(TAG, "Image Not Found");
                  }

                dialog.show();
                RelativeLayout rlDialogCall = (RelativeLayout) dialog.findViewById(R.id.rlDialogCall);
                rlDialogCall.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {

                        dialog.dismiss();

                          if(holder.getId() != null)
                        {

                            Intent intent = new Intent(getActivity(), ShareLocationConfirm.class);
                            intent.putExtra("user_id", holder.getId());
                            startActivity(intent);
                        }

                    }
                });
                RelativeLayout rlDialogMailHL = (RelativeLayout) dialog.findViewById(R.id.rlDialogMailHL);
                rlDialogMailHL.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        dialog.dismiss();
                        confirmDelete();
                    }
                });*/

            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

      //  getActivity().registerReceiver(refreshRequest, new IntentFilter("Refresh_Home"));
    }

    BroadcastReceiver refreshRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Toast.makeText(getActivity().getApplicationContext(), "Refreshed", Toast.LENGTH_SHORT).show();

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {


                }
            });

        }
    };

    @Override
    public void onPause() {
        super.onResume();
        Toast.makeText(getActivity().getApplicationContext(), "Refreshed", Toast.LENGTH_SHORT).show();

       // getActivity().unregisterReceiver(refreshRequest);
    }


}
