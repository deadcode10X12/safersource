package tronbox.guardiansfunctionality.Home;

/**
 * Created by Shrinivas Mishra on 3/24/2015.
 * Created Adapter for Home page
 */

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.HashSet;
import java.util.List;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.ViewHolder;
import tronbox.guardiansfunctionality.Login.CommonFunctions;
import tronbox.guardiansfunctionality.R;


public class HomeAdapter extends   CursorAdapter
{
    LayoutInflater inflater;
    Context context;
    CommonFunctions comFunc=new CommonFunctions();
    private List<GuardianDetails> list;
    private int resource;

    HashSet<String> set = new HashSet<String>();
    public HomeAdapter(Context context)
    {
        super(context, null, 0);
        inflater = LayoutInflater.from(context);
        context = context;
    }
    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
       if(cursor.getString(1).length() >= 2)
        {
            final ViewHolder holder = (ViewHolder)view.getTag();
            final Context c = context;
            holder.text1.setText(cursor.getString(0));
            holder.text2.setText(cursor.getString(1));
            holder.name = cursor.getString(0);
            holder.invite.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    try {
                        Log.w("SMS", holder.text2.getText().toString());
                        SmsManager.getDefault().sendTextMessage(holder.text2.getText().toString(), null, "Check out this amazing game QuizJab. Download it today from "+"https://play.google.com/store/apps/details?id=ideapot.talentseal.buz&hl=en", null, null);
                        Toast.makeText(c, "QuizJab Invitation Sent", Toast.LENGTH_LONG).show();
                    }
                    catch (IllegalArgumentException ex){
                        ex.printStackTrace();
                    }
                    catch (Throwable ex){
                        ex.printStackTrace();
                    }
                }
            });
            String photoUri = cursor.getString(2);
            if(photoUri != null){
                Uri photo = Uri.parse(photoUri);
                AssetFileDescriptor afd = null;
                InputStream inputStream = null;
                try {
                    inputStream = context.getContentResolver().openInputStream(photo);
                    holder.pic.setImageBitmap(comFunc.imageCirclexClip(context,BitmapFactory.decodeStream(inputStream)));
                } catch (Exception e) {
                    holder.pic.setImageBitmap(comFunc.imageCirclexClip(context,BitmapFactory.decodeResource(context.getResources(), R.drawable.face)));
                    e.printStackTrace();
                }
            }else{
                holder.pic.setImageBitmap(comFunc.imageCirclexClip(context,BitmapFactory.decodeResource(context.getResources(), R.drawable.face)));
            }
        }
    }

    @Override
    public View newView(final Context context, Cursor cursor, ViewGroup parent) {
        View itemLayout =  inflater.inflate(R.layout.custom_home_list, parent, false);
        final ViewHolder holder = new ViewHolder();
        holder.text1 = (TextView)itemLayout.findViewById(R.id.txtHomeName);
        holder.text2 = (TextView)itemLayout.findViewById(R.id.txtHomeComment);
        holder.pic = (ImageView)itemLayout.findViewById(R.id.imgHomePhoto);
        holder.value = false;
        itemLayout.setTag(holder);
        return itemLayout;
    }
}
