package tronbox.guardiansfunctionality.Home;


        import android.app.Activity;
        import android.app.AlertDialog;
        import android.bluetooth.BluetoothAdapter;
        import android.content.ActivityNotFoundException;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.content.IntentFilter;
        import android.content.res.Resources;
        import android.database.Cursor;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.graphics.Matrix;
        import android.location.Address;
        import android.location.Criteria;
        import android.location.Geocoder;
        import android.location.Location;
        import android.location.LocationListener;
        import android.location.LocationManager;
        import android.media.Ringtone;
        import android.media.RingtoneManager;
        import android.net.ConnectivityManager;
        import android.net.NetworkInfo;
        import android.net.Uri;
        import android.net.wifi.WifiManager;
        import android.os.BatteryManager;
        import android.os.Bundle;
        import android.os.Environment;
        import android.os.Handler;
        import android.os.Vibrator;
        import android.provider.*;
        import android.util.Log;
        import android.view.View;
        import android.view.WindowManager;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.CompoundButton;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.ProgressBar;
        import android.widget.Switch;
        import android.widget.TextView;

        import java.io.ByteArrayOutputStream;
        import java.io.File;
        import java.io.FileNotFoundException;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.OutputStream;
        import java.util.List;
        import java.util.Locale;

        import tronbox.guardiansfunctionality.Login.SendProfileImage;
        import tronbox.guardiansfunctionality.Networking.Networking;
        import tronbox.guardiansfunctionality.R;
        import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
        import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 4/10/2015.
 * User Profile Screen
 */
public class UserProfileActivity extends Activity
{
    Context myContext=UserProfileActivity.this;
    private int mProgressStatus=100;
    private TextView txtPercentageBattery, userName, userAddress;
    private ProgressBar prgBar;
    private Handler mHandler = new Handler();
    int presentage=0;
    int cntGuardian=5;
    CircleImagePhoto imgPhotoUP;
    BluetoothAdapter saferBTAdapter;
    private ImageView imgBatteryUP;
    private LocationManager locationManager;
    private String provider;
    private Bitmap bitmap;

    private EditText txteNameUP, txteEmailUP;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        userName = (TextView)findViewById(R.id.txtBatteryLevel);
        userName.setText(SharedPrefrenceStorage.GetName(getApplicationContext()));

        userAddress = (TextView)findViewById(R.id.txtAddressUP);
        handler.post(runnable);

        prgBar = (ProgressBar) findViewById(R.id.progressBarUP);
        ImageView imgEditNameUP=(ImageView)findViewById(R.id.imgEditNameUP);


        txteNameUP = (EditText) findViewById(R.id.txteNameUP);
        txteNameUP.setText(SharedPrefrenceStorage.GetName(getApplicationContext()));

        txtPercentageBattery =(TextView)findViewById(R.id.txtProfileUpdatePercentage);
        txtPercentageBattery.setText(SharedPrefrenceStorage.GetSecurityPrefrence(getApplicationContext())+"% secured");

        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        txteNameUP.setEnabled(false);
        imgEditNameUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txteNameUP.isEnabled()) {
                    txteNameUP.setEnabled(true);
                    txteNameUP.selectAll();
                    txteNameUP.requestFocus();
                    imm.showSoftInput(txteNameUP, InputMethodManager.SHOW_IMPLICIT);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                }else{
                    userName.setText(txteNameUP.getText().toString());
                    SharedPrefrenceStorage.StoreName(getApplicationContext(), txteNameUP.getText().toString());
                    txteNameUP.setEnabled(false);
                    txteNameUP.clearFocus();
                    txteNameUP.setSelection(0);

                }
            }
        });
        ImageView imgBackUP=(ImageView)findViewById(R.id.imgBackUP);
        imgBackUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, HomeActivity.class);
                startActivity(intent);
            }
        });

        ImageView imgEditPhoneUP=(ImageView)findViewById(R.id.imgEditPhoneUP);


        txteEmailUP = (EditText) findViewById(R.id.txtePhoneNumberUP);
        txteEmailUP.setText(SharedPrefrenceStorage.GetEmail(getApplicationContext()));
        txteEmailUP.setEnabled(false);

        imgEditPhoneUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txteEmailUP.isEnabled())
                {
                    txteEmailUP.setEnabled(true);
                    txteEmailUP.selectAll();
                    txteEmailUP.requestFocus();
                    imm.showSoftInput(txteEmailUP, InputMethodManager.SHOW_IMPLICIT);
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                }else
                {
                    SharedPrefrenceStorage.StoreEmail(getApplicationContext(), txteEmailUP.getText().toString());
                    txteEmailUP.setEnabled(false);
                    txteEmailUP.clearFocus();
                    txteEmailUP.setSelection(0);
                }
            }
        });

        imgPhotoUP=(CircleImagePhoto)findViewById(R.id.imgPhotoUP);

         if(StaticMaster.profilePic != null)
        {
            imgPhotoUP.setImageBitmap(StaticMaster.profilePic);
        }

        imgPhotoUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectNewProfilePhoto();
            }
        });


        //Sound Switch declaration
        Switch switchbarSound=(Switch)findViewById(R.id.switchbarSound);
        switchbarSound.setChecked(false);
        switchbarSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Code here isChecked true or false
                Uri ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone ringtoneSound = RingtoneManager.getRingtone(getApplicationContext(), ringtoneUri);
                if (isChecked)
                {
                    if (ringtoneSound != null) {
                        ringtoneSound.play();
                    }
                }
                else
                {
                    ringtoneSound.stop();
                }
            }
        });
        //Vibration Swith declaration
        Switch swithbarVibrate=(Switch)findViewById(R.id.swithbarVibrate);
        swithbarVibrate.setChecked(false);
        swithbarVibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //Code here isChecked true or false
                Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (isChecked)
                {
                    vib.vibrate(1000);
                }
                else
                {
                    vib.cancel();
                }
            }
        });

    }

    public void doProgress() {
        new Thread(new Runnable() {
            public void run() {
                txtPercentageBattery.setText("");
                if (mProgressStatus<=10)
                {
                    Resources res = getResources();
                    prgBar.setProgressDrawable(res.getDrawable(R.drawable.progress_red));
                }
                else if(mProgressStatus<=45)
                {
                    Resources res = getResources();
                    prgBar.setProgressDrawable(res.getDrawable(R.drawable.progress_yellow));
                }
                while (presentage<mProgressStatus) {
                    presentage += 1;
                    mHandler.post(new Runnable() {
                        public void run() {
                            prgBar.setProgress(presentage);
                            txtPercentageBattery.setText(""+presentage+"%");
                        }
                    });
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
    //method for select or take photo
    private void SelectNewProfilePhoto()
    {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(myContext);
        builder.setTitle("Add Your Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    try {
                        startActivityForResult(intent, 1);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    try {
                        startActivityForResult(intent, 2);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == 1) {
                    Bundle imgExtra=data.getExtras();
                    bitmap=(Bitmap)imgExtra.get("data");
                    bitmap= Networking.getCompressBit(bitmap);
                    imgPhotoUP.setImageBitmap(bitmap);



                    String path = Environment.getExternalStorageDirectory() + File.separator + "Phoenix" + File.separator + "default";
                    try {
                        File dir = new File(path);
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }
                        OutputStream fOut;
                        File file = new File(path, "safer_profile_photo.png");
                        if (file.exists())
                        {
                            file.delete();
                        }
                        file.createNewFile();
                        fOut = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                        Log.e("compresspath", file.getAbsolutePath().toString());
                        MediaStore.Images.Media.insertImage(myContext.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
                        fOut.flush();
                        fOut.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (requestCode == 2) {
                    if (data!=null) {
                        Uri selectedImage = data.getData();
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        String picturePath = c.getString(columnIndex);
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        bitmap=BitmapFactory.decodeFile(picturePath, bitmapOptions);
                        bitmap= Networking.getCompressBit(bitmap);
                        imgPhotoUP.setImageBitmap(bitmap);



                        c.close();
                        String path = Environment.getExternalStorageDirectory() + File.separator + "Phoenix" + File.separator + "default";
                        try {
                            File dir = new File(path);
                            if (!dir.exists()) {
                                dir.mkdirs();
                            }
                            OutputStream fOut = null;
                            File file = new File(path, "safer_profile_photo.png");
                            if (file.exists())
                            {
                                file.delete();
                            }
                            file.createNewFile();
                            fOut = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                            Log.e("compresspath", file.getAbsolutePath().toString());
                            MediaStore.Images.Media.insertImage(myContext.getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
                            fOut.flush();
                            fOut.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    public void uploadAndSavePic()
    {
        if(bitmap != null)
        {

            try {

                Log.d(StaticMaster.LoginScreenLogs, "User is using the local signup, saving his profile_pic interanlly");

                FileOutputStream out1  = openFileOutput(StaticMaster.UserProfilePic, Context.MODE_PRIVATE);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                byte[] buffer = bos.toByteArray();
                out1.write(buffer, 0, buffer.length);
                out1.close();

                new SendProfileImage(getApplicationContext()).execute(bitmap);

            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            } catch (Exception e){
            }

        }

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {


            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.ENGLISH);
            String[] coordinates = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");

            try {
                List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]), 1);
                if(addresses != null) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder strReturnedAddress = new StringBuilder();
                    for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    userAddress.setText(strReturnedAddress.toString());
                }
                else{
                    userAddress.setText("No Address identified!");
                }
            } catch (IOException e) {
                e.printStackTrace();
                userAddress.setText("Cannot get Address!");
            }

        }
    };


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {

        uploadAndSavePic();

        super.onPause();
    }

}
