package tronbox.guardiansfunctionality.Home;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;

import java.util.concurrent.TimeUnit;

import tronbox.guardiansfunctionality.HandleGuardians.GuardianAlertActivity;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 3/2/2015.
 * Purpose: define the user profile for show user details
 * user can edit details and show alert for security level
 */
public class AlertActivity extends Activity
{
    TextView txtCountDown;
    Context myContext=AlertActivity.this;
    LinearLayout llCountdown;
    private static final String FORMAT = "%02d";
    public   long startTimer = 10000;
    public  long interval = 1000;
    private CountDownClass timer;
    int seconds , minutes;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        txtCountDown = (TextView)findViewById(R.id.txtCountDown);
        txtCountDown.setText("00");
        llCountdown = (LinearLayout)findViewById(R.id.llCountdown);
        llCountdown.setEnabled(true);
        timer=new CountDownClass(startTimer,interval);
        timer.start();
        llCountdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmDialog();

            }
        });
    }

    @Override
    public void finish() {

        confirmDialog();

    }

    public long minLeft;

    public class CountDownClass extends CountDownTimer
    {
        public CountDownClass(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish()
        {

            txtCountDown.setText("Alert Sent!");
            llCountdown.setEnabled(false);

            // Send the SOS

          StaticMaster.sosDuration = StaticMaster.sosDistressDuration;
          StaticMaster.CurrentState = StaticMaster.States.Distress;
          sendBroadcast(new Intent("LocationHandler"));

            StaticMaster.sendCommonData(getApplicationContext(), "Send_SOS", "http://54.169.182.72/api/v0/sos", Request.Method.POST);

              if(SharedPrefrenceStorage.GetRunningActivity(getApplicationContext()) != null && SharedPrefrenceStorage.GetRunningActivity(getApplicationContext()).equals("SafeWalk"))
            {
                sendBroadcast(new Intent("StopSaferWalk"));
            }

            Intent intent = new Intent(getApplicationContext(), GuardianAlertActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
         @Override
        public void onTick(long millisUntilFinished)
        {
            minLeft = millisUntilFinished;
            Log.w("MinutesLeft", ""+minLeft);
            String hms = String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(minLeft) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(minLeft)));
            txtCountDown.setText(hms);
        }

    }

    @Override
    public void onBackPressed() {

        confirmDialog();

        super.onBackPressed();
    }

    public void callHome()
    {

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

      public void confirmDialog()
    {
        timer.cancel();

        final Dialog dialog = new Dialog(myContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dailog_without_image);
        TextView txtMiddleHeadingW = (TextView) dialog.findViewById(R.id.txtMiddleHeadingW);
        txtMiddleHeadingW.setText("Do you want to cancel ?");
        dialog.show();
        timer.cancel();
        RelativeLayout rlDialogAcceptW = (RelativeLayout) dialog.findViewById(R.id.rlDialogAcceptW);
        rlDialogAcceptW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                llCountdown.setEnabled(false);
                dialog.dismiss();

                callHome();


            }
        });
        RelativeLayout rlDialogDeclineW = (RelativeLayout) dialog.findViewById(R.id.rlDialogDeclineW);
        rlDialogDeclineW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

                timer=new CountDownClass(minLeft, interval);
                timer.start();


            }
        });

    }
}
