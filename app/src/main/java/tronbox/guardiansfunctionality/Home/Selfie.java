package tronbox.guardiansfunctionality.Home;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.List;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class Selfie extends Activity {

    private Camera cameraObject;
    private ShowCamera showCamera;
    private Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
    private AnimationDrawable mScanningAnimation;

    public Camera openChoosenCamera(boolean backCamera) {

        int cameraCount = 0;
        Camera cam = null;
        cameraCount = Camera.getNumberOfCameras();

          for (int camIdx = 0; camIdx < cameraCount; camIdx++)
        {

            Camera.getCameraInfo(camIdx, cameraInfo);

              if(backCamera == true)
            {
                  if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                {
                      try
                    {
                        cam = Camera.open(camIdx);
                    }
                      catch (RuntimeException e) {
                        Log.e("Selfie", "Camera failed to open: " + e.getLocalizedMessage());
                    }
                }
            }
                else if(backCamera == false)
              {
                   if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                  {
                      try
                      {
                          cam = Camera.open(camIdx);
                      }
                      catch (RuntimeException e) {
                          Log.e("Selfie", "Camera failed to open: " + e.getLocalizedMessage());
                      }
                  }
              }

        }

        return cam;
    }


    public  Camera isCameraAvailiable(boolean value){
        Camera object = null;

        try
        {
            object = openChoosenCamera(value);

             for(Camera.Size s: object.getParameters().getSupportedPictureSizes())
            {
                Log.w("ScreenSize" , s.width+"/"+s.height);

            }

            Camera.Parameters parameters = object.getParameters();
            parameters.setPreviewSize(240, 320);
            parameters.setPictureSize(288, 352);
            cameraObject.setParameters(parameters);

        }

        catch (Exception e){
        }
        return object;
    }

    //****** Variables *******//

    File file, folder;
    FileOutputStream out;

    //***** Bitmap to Uri converter ******//

      public Uri getUri(Bitmap bitmap)
    {
        Uri bitmapUri = null;

        folder =  new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "selfie");
        folder.mkdirs();

        file = new File (folder, "selfie.jpg");

        if (file.exists ()) file.delete ();

        try {

            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            out.flush();
            out.close();

            bitmapUri = Uri.fromFile(file);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmapUri;
    }


    private Uri bitmapUri;

    private Camera.PictureCallback capturedIt = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, final Camera camera) {

            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

        //    progBar.setVisibility(View.GONE);

            Toast.makeText(getApplicationContext(), "Pic Taken", Toast.LENGTH_SHORT).show();

              if(bitmap != null)
            {
               bitmapUri = getUri(bitmap);
               bottomButtons.setVisibility(View.VISIBLE);
               mScanningAnimation.stop();
            }

        }
    };


    LinearLayout bottomButtons;

    ProgressDialog progressDialog;

    private ImageView switchCamera, flash;

    private boolean flashFlip = false;

    Bitmap flashOff, flashOn;

    Camera.Parameters p;

    Dialog dialog;
    ProgressBar progBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selfie);

        dialog = new Dialog(Selfie.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.selfie_loading_text);

        mScanningAnimation = (AnimationDrawable) getResources().getDrawable(R.drawable.scanning_animation);

        //((TextView)dialog.findViewById(R.id.photo_text)).setBackgroundResource(R.drawable.oval_button_shape);

        progBar = (ProgressBar)findViewById(R.id.progress_bar);

        flashOff = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_flash_off);
        flashOn = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_flash_on);

        bottomButtons = (LinearLayout)findViewById(R.id.bottom_buttons);
        bottomButtons.setVisibility(View.GONE);

        switchCamera = (ImageView)findViewById(R.id.switch_camera);
        switchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StaticMaster.flip = !StaticMaster.flip;

                finish();
                Intent intent = new Intent(getApplicationContext(), Selfie.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });


        flash = (ImageView)findViewById(R.id.flash);
        flash.setImageBitmap(flashOff);
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  if(StaticMaster.flip == true) // it's a back camera
                {
                     if(flashFlip == false) // start the flash
                    {
                        flash.setImageBitmap(flashOn);

                        p = cameraObject.getParameters();
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        cameraObject.setParameters(p);

                    }
                      else {  // stop the flash

                         flash.setImageBitmap(flashOff);
                         p = cameraObject.getParameters();
                         p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                         cameraObject.setParameters(p);
                     }

                    flashFlip = !flashFlip;

                }
                  else {

                      Toast.makeText(getApplicationContext(), "No Flash For Front Camera", Toast.LENGTH_SHORT).show();
                  }

            }
        });


        ((Button)bottomButtons.findViewById(R.id.retake)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bottomButtons.setVisibility(View.GONE);
                showCamera.startPreview();

            }
        });

        ((Button)bottomButtons.findViewById(R.id.cool)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 if(bitmapUri != null)
                {
                    cameraObject.release();

                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, bitmapUri);
                    shareIntent.setType("image/jpeg");
                    startActivity(Intent.createChooser(shareIntent, null));

                }
                  else { // image not taken properly

                     bottomButtons.setVisibility(View.GONE);
                     showCamera.startPreview();

                 }

                Toast.makeText(getApplicationContext(), "taken", Toast.LENGTH_SHORT).show();

                finish();

            }
        });

        cameraObject = isCameraAvailiable(StaticMaster.flip);
        showCamera = new ShowCamera(this, cameraObject);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(showCamera);
    }

    @Override
    protected void onResume() {
        super.onResume();


        SharedPrefrenceStorage.StorePosition(getApplicationContext(), "SelfieActivity");
        registerReceiver(broadcastReceiver, new IntentFilter("Selfie"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        cameraObject.release();
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

//            progBar.setVisibility(View.VISIBLE);


            mScanningAnimation.start();

            cameraObject.takePicture(null, null, capturedIt);



        }
    };

    Handler handler = new Handler();

    Runnable runnable = new Runnable() {
        @Override
        public void run() {


        }
    };

    class thread extends Thread {
        @Override
        public void run() {


            progressDialog = ProgressDialog.show(Selfie.this, "title", "loading");

            super.run();

        }
    }

    public class ShowCamera extends SurfaceView implements SurfaceHolder.Callback {

        private SurfaceHolder holdMe;
        private Camera theCamera;

        public ShowCamera(Context context,Camera camera) {
            super(context);
            theCamera = camera;
            holdMe = getHolder();
            holdMe.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            holdMe.addCallback(this);
        }

        @Override
        public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {


        }


        @Override
        public void surfaceCreated(SurfaceHolder holder) {

            startPreview();

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder arg0) {
        }

          public void startPreview()
        {
            try   {

                if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                    theCamera.setDisplayOrientation(90);
                } else {
                    theCamera.setDisplayOrientation(0);
                }

                theCamera.setPreviewDisplay(getHolder());
                theCamera.startPreview();
            } catch (IOException e) {
            }

        }
    }
}

