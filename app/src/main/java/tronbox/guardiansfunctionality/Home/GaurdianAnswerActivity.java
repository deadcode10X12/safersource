package tronbox.guardiansfunctionality.Home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import tronbox.guardiansfunctionality.R;

/**
 * Created by Shrinivas Mishra on 3/22/2015.
 * for send answer to guardian by children
 */
public class GaurdianAnswerActivity extends Activity
{
    Context myContext=GaurdianAnswerActivity.this;
    ArrayAdapter<String> AnsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guardian_answer);
        ListView lstContact = (ListView) findViewById(R.id.lvAnswerGA);
        String[] answers=new String[]{"At Home", "At Office","Write to Us"};
        AnsAdapter = new AnswerCustomAdapter(myContext,answers);
        lstContact.setAdapter(AnsAdapter);
        ImageView imgBackGA=(ImageView)findViewById(R.id.imgBackGA);
        imgBackGA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, HomeActivity.class);
                startActivity(intent);
            }
        });
    }

}
