package tronbox.guardiansfunctionality.Home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import tronbox.guardiansfunctionality.R;

/**
 * Created by Shrinivas Mishra on 3/30/2015.
 * Showing About Us Page
 */
public class AboutUsActivity extends Activity {
    Context myContext = AboutUsActivity.this;

    String[] links = {
            
            "http://www.leafwearables.com/terms-of-use/",
            "https://www.leafwearables.com/privacy-policy/",
            "https://www.facebook.com/LeafCorporations",
            "https://twitter.com/leafwearables",
            "https://www.linkedin.com/company/leaf-wearables",
            "http://leafwearables.com/"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        LinearLayout llAUTC=(LinearLayout)findViewById(R.id.llAUTC);
        llAUTC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(links[0])));

            }
        });
        LinearLayout llAUPrivacyPolicy=(LinearLayout)findViewById(R.id.llAUPrivacyPolicy);
        llAUPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(links[1])));
            }
        });
        LinearLayout llAUFacebook=(LinearLayout)findViewById(R.id.llAUFacebook);
        llAUFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(links[2])));
            }
        });
        LinearLayout llAUTwitter=(LinearLayout)findViewById(R.id.llAUTwitter);
        llAUTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(links[3])));
            }
        });
        LinearLayout llAULinkin=(LinearLayout)findViewById(R.id.llAULinkin);
        llAULinkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(links[4])));
            }
        });
        LinearLayout llAUWebsite=(LinearLayout)findViewById(R.id.llAUWebsite);
        llAUWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(links[5])));
            }
        });
        ImageView imgBackAU=(ImageView)findViewById(R.id.imgBackAU);
        imgBackAU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, HomeActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
