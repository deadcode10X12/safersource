package tronbox.guardiansfunctionality.Home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import tronbox.guardiansfunctionality.R;

/**
 * Created by Shrinivas Mishra on 3/23/2015.
 * Showing the Custom Answer Adapter
 */
public class AnswerCustomAdapter extends ArrayAdapter<String>
{
    private final Context context;
    private final String[] values;

    public AnswerCustomAdapter(Context context, String[] values) {
        super(context, R.layout.guardian_answer_list, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.guardian_answer_list, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.txtAnswerGA);
        textView.setText(values[position]);
        return rowView;
    }
}

