package tronbox.guardiansfunctionality.Home;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.Login.CommonFunctions;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.StaticMaster;


public class HomeList extends ArrayAdapter<GuardianDetails> {
    private Context context;
    private List<GuardianDetails> list;
    private int resource;
    private Set<Integer> set = new HashSet<Integer>();
    CommonFunctions comFunc=new CommonFunctions();

    String TAG = "HomeActivityLogs";

    public HomeList(Context context, int resource, List<GuardianDetails> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.list = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(resource, parent, false);

        GuardianDetails holder = list.get(position);
        TextView name = (TextView) view.findViewById(R.id.txtHomeName);
        name.setText(holder.getName());
        TextView mobile = (TextView) view.findViewById(R.id.txtHomeComment);
        mobile.setText(holder.getRemarks());

        if (holder.getImage() != null) {

            ImageView imageView = ((CircleImagePhoto) view.findViewById(R.id.imgHomePhoto));

            if (holder.getImage().contains("https")) {

                Log.w(TAG, "Image Found");

                imageView.setImageBitmap(Networking.decodeSampledBitmapFromResource(context, holder, 40, 40, imageView));

            }

        }

        if (holder.getCategory().compareTo(StaticMaster.Category.Ward) == 0) {

            ((ImageView) view.findViewById(R.id.imgPendingExcl)).setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.minor_64));

        }

        if (holder.getStatus().equals("true")) {

            if (holder.getCategory().compareTo(StaticMaster.Category.Guardian) == 0) {

                ((ImageView) view.findViewById(R.id.imgPendingExcl)).setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.guardian_64));

            } else if (holder.getCategory().compareTo(StaticMaster.Category.Ward) == 0) {

                ((ImageView) view.findViewById(R.id.imgPendingExcl)).setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.minor_64));

            }
              else if (holder.getCategory().compareTo(StaticMaster.Category.Both) == 0) {

                ((ImageView) view.findViewById(R.id.imgPendingExcl)).setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.mutual_64));

            }

        }

            return view;
        }

    public void addToSet(int value){
        if(set.contains(value)){
            set.remove(value);
        }else {
            set.add(value);
        }
        notifyDataSetChanged();
    }
    public Set<Integer> getSelectedList(){
        return set;
    }

}
