package tronbox.guardiansfunctionality.Home;


import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.Device.BlueGigBrain;
import tronbox.guardiansfunctionality.Device.DeviceSettingActivity;
import tronbox.guardiansfunctionality.Device.DeviceSyncService;
import tronbox.guardiansfunctionality.GuardianBrain;
import tronbox.guardiansfunctionality.HandleGuardians.InvitePendingGuardians;
import tronbox.guardiansfunctionality.Login.ContactListActivity;
import tronbox.guardiansfunctionality.Login.DeviceSyncActivity;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

  public class HomeActivity extends ActionBarActivity implements HomeControlPanel.deleteCallback
{
    Context myContext=HomeActivity.this;
    private Toolbar toolbar;
    String TITLES[] = {"Blog","Invitations","About Us","Write Feedback","Notifications", "Settings"};
    int ICONS[] = {R.drawable.blog,R.drawable.invitation,R.drawable.about_us,R.drawable.feedback, R.drawable.notification, R.drawable.setting};

    String userName = null;

    String EMAIL = "";
    String NotifyNo="3";

    private int hot_number = 3;
    private TextView ui_hot = null;


    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle

    public ImageView refresh;

    HomeControlPanel homeControlPanel;
    HomeListFragment fg;

    LinearLayout homeMainView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Loading Profile

        userName = SharedPrefrenceStorage.GetName(getApplicationContext());

        try {

            InputStream inputStream = openFileInput(StaticMaster.UserProfilePic);
            StaticMaster.profilePic = BitmapFactory.decodeStream(inputStream);

        } catch (FileNotFoundException e){

            StaticMaster.profilePic = BitmapFactory.decodeResource(getResources(), R.drawable.face);

        }

        //*****************//


        setContentView(R.layout.activity_home);

        homeMainView = (LinearLayout)findViewById(R.id.home_main_view);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setNavigationIcon(R.drawable.nav_drawer);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView)findViewById(R.id.RecyclerView);

        mRecyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter(TITLES,ICONS,NotifyNo,userName,EMAIL,StaticMaster.profilePic,myContext);
        mRecyclerView.setAdapter(mAdapter);

        final GestureDetector mGestureDetector = new GestureDetector(myContext, new GestureDetector.SimpleOnGestureListener()
        {
            @Override
            public boolean onSingleTapUp(MotionEvent e)
            {
                return true;
            }
        });
        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener()
        {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent)
            {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(),motionEvent.getY());
                if(child!=null && mGestureDetector.onTouchEvent(motionEvent))
                {
                    Drawer.closeDrawers();
                    Toast.makeText(myContext, "The Item Clicked is: " + recyclerView.getChildPosition(child), Toast.LENGTH_SHORT).show();
                    if (recyclerView.getChildPosition(child)==3) {

                        /*Intent intent = new Intent(myContext, AboutUsActivity.class);
                        startActivity(intent);
                        */
                    }else if (recyclerView.getChildPosition(child)==2) {

                        Intent intent = new Intent(myContext, InvitePendingGuardians.class);
                        intent.putExtra("type", "ward");
                        startActivity(intent);

                    }
                    else if(recyclerView.getChildPosition(child)==0)
                    {

                        Intent intent = new Intent(myContext, UserProfileActivity.class);
                        startActivity(intent);

                    }else if(recyclerView.getChildPosition(child)==1){

                      startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.leafwearables.com/blog/")));

                    }
                     else if(recyclerView.getChildPosition(child)==3)
                    {

                        Intent intent = new Intent(myContext, AboutUsActivity.class);
                        startActivity(intent);

                    }
                      else if (recyclerView.getChildPosition(child)==4)
                    {
                        final Dialog dialog = new Dialog(myContext);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.custom_feedback_dailog);
                        dialog.show();
                        RelativeLayout rlMSendFDailog = (RelativeLayout) dialog.findViewById(R.id.rlMSendFDailog);
                        rlMSendFDailog.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                dialog.dismiss();
                            }
                        });
                        RelativeLayout rlMCancelFDailog = (RelativeLayout) dialog.findViewById(R.id.rlMCancelFDailog);
                        rlMCancelFDailog.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                dialog.dismiss();
                            }
                        });
                    }

                    return true;
                }
                return false;
            }
            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent)
            {

            }
        });
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);



            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }

        };

        Drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        Fragment hf = new HomeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, hf).commit();

        fg=new HomeListFragment();
        Bundle bd=new Bundle();
        bd.putInt("Screen", 2);
        fg.setArguments(bd);
        getSupportFragmentManager().beginTransaction().replace(R.id.home_list_container, fg).commit();

        StaticMaster.getBatteryPct(getApplicationContext()); // calculate and update the battery percentage

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        Toast.makeText(getApplicationContext(), "Activity Is Destroyed", Toast.LENGTH_SHORT).show();

        startService(new Intent(getApplicationContext(), DeviceSyncService.class));


        // sendBroadcast(new Intent("AfterDelete"));
    }

    BroadcastReceiver securityReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // security paramtres are available

            StaticMaster.SecurityParametres securityParametres =  StaticMaster.securityLevel(context);

             if(securityParametres != null && intent.getExtras().containsKey("battery_pct"))
            {
                int level = securityParametres.getLevel();

                float batteryPct = intent.getExtras().getFloat("battery_pct");

                  if(batteryPct < 20)
                {
                    level = level - 10;
                }

                 for(String reason : securityParametres.getReasons())
                {
                    Log.w("SecurityLevel", reason);
                }

                SharedPrefrenceStorage.StoreSecurityPrefrence(getApplicationContext(), ""+level);

                //Toast.makeText(context, "You are "+level+"% secure", Toast.LENGTH_LONG).show();
            }

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

         if(StaticMaster.logsList.size() > 0)
        {
              // Print the logs

              for(String s : StaticMaster.logsList)
            {
                String[] ss = s.split("@");

            //    Log.w("SaferWalkLogs", ss[0]+" "+ss[1]);
            }

            StaticMaster.logsList.clear();
        }

        Log.w("ConnectionStatus", SharedPrefrenceStorage.GetConnectionStatus(getApplicationContext()));

        SharedPrefrenceStorage.StorePosition(myContext, "HomeActivity");


        registerReceiver(refreshRequest, new IntentFilter("Refresh_Home"));

        registerReceiver(broadcastReceiver, new IntentFilter("connect"));
        registerReceiver(securityReciever, new IntentFilter("security_parameters"));

     //   startService(new Intent(getApplicationContext(), DeviceSyncService.class));

        //sendBroadcast(new Intent("AfterDelete"));

        Intent intent1 = new Intent("StartScan");
        intent1.putExtra("data", "reconnect");
        sendBroadcast(intent1);


    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
        unregisterReceiver(securityReciever);
        unregisterReceiver(refreshRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

/*

        MenuItem item = menu.findItem(R.id.action_refresh);
        MenuItemCompat.setActionView(item, R.layout.action_bar_notifitcation_icon);
        View view = MenuItemCompat.getActionView(item);
        refresh = (ImageView)view.findViewById(R.id.hotlist_bell);

        */
/*ui_hot = (TextView) view.findViewById(R.id.hotlist_hot);
        updateHotCount(hot_number);
*//*

        new MyMenuItemStuffListener(view, "Show hot message") {
            @Override
            public void onClick(View v) {
                //Calling Device;


            }
        };
*/

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add)
        {
            Intent intent = new Intent(myContext, ContactListActivity.class);
            intent.putExtra("data", "AddGuardians");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }
        else if(id==R.id.action_refresh)
        {
            Intent intent = null;

             if(StaticMaster.bluetoothGattService == null)
            {
                SharedPrefrenceStorage.StoreMacAdd(getApplicationContext(), null); // forget the previous device
                intent = new Intent(myContext, DeviceSyncActivity.class);

            }
               else if(StaticMaster.bluetoothGattService != null)
             {
                 intent = new Intent(myContext, DeviceSettingActivity.class);
             }


            startActivity(intent);

            return true;
        }

          else if(id==R.id.action_selfie)
        {
            Toast.makeText(getApplicationContext(), "Selfie", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(myContext, Selfie.class);
            startActivity(intent);

            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    /*public void updateHotCount(final int new_hot_number) {
        hot_number = new_hot_number;
        if (ui_hot == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (new_hot_number == 0)
                    ui_hot.setVisibility(View.INVISIBLE);
                else {
                    ui_hot.setVisibility(View.VISIBLE);
                    ui_hot.setText(Integer.toString(new_hot_number));
                }
            }
        });
    }
    */


    BroadcastReceiver refreshRequest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Toast.makeText(getApplicationContext(), "Refreshed", Toast.LENGTH_SHORT).show();

            Intent intent1 = new Intent(context, HomeActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent1);
            
        }
    };

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            /*Bundle bundle = intent.getExtras();

             if(bundle != null && bundle.containsKey("data"))
            {
                String data = bundle.getString("data");

                 if(data.equals("connected"))
                {

                    refresh.setImageBitmap(BitmapFactory.decodeResource(getR_esources(), R.drawable.ic_action_refresh_green));
                }
                  else  if(data.equals("disconnected"))
                 {

                     refresh.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_refresh_red));
                 }

            }*/

        }
    };


    @Override
    public void delete() {


        fg=new HomeListFragment();
        Bundle bd=new Bundle();
        bd.putInt("Screen", 2);
        fg.setArguments(bd);
        getSupportFragmentManager().beginTransaction().replace(R.id.home_list_container, fg).commit();

    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    static abstract class MyMenuItemStuffListener implements View.OnClickListener, View.OnLongClickListener {
        private String hint;
        private View view;

        MyMenuItemStuffListener(View view, String hint) {
            this.view = view;
            this.hint = hint;
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override abstract public void onClick(View v);

        @Override public boolean onLongClick(View v) {
            final int[] screenPos = new int[2];
            final Rect displayFrame = new Rect();
            view.getLocationOnScreen(screenPos);
            view.getWindowVisibleDisplayFrame(displayFrame);
            final Context context = view.getContext();
            final int width = view.getWidth();
            final int height = view.getHeight();
            final int midy = screenPos[1] + height / 2;
            final int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
            Toast cheatSheet = Toast.makeText(context, hint, Toast.LENGTH_SHORT);
            if (midy < displayFrame.height()) {
                cheatSheet.setGravity(Gravity.TOP | Gravity.RIGHT,
                        screenWidth - screenPos[0] - width / 2, height);
            } else {
                cheatSheet.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, height);
            }
            cheatSheet.show();
            return true;
        }


    }


}

