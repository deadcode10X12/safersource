package tronbox.guardiansfunctionality.Home;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.Maps.SafeWalkSetup;
import tronbox.guardiansfunctionality.Maps.ShareLocationConfirm;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class HomeControlPanel extends DialogFragment implements View.OnClickListener{

    ProgressDialog progressDialog;

    MobileDatabase database;
    String TAG = "HomeControlPanel";


    Activity activity;

    public interface deleteCallback{
        public void delete();
    }

    public deleteCallback localCallback;

    public HomeList homeListAdapter;

    private Dialog dialog;
    private String message;
    TextView txtMiddleHeadingW, connect, skip;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            localCallback = (deleteCallback)activity;

        }catch (ClassCastException ex){

            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");

        }

        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(activity).inflate(R.layout.home_control_panel, container, false);


        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dailog_without_image);

        connect = (TextView)dialog.findViewById(R.id.yes);
        connect.setText("Connect");

        skip = (TextView)dialog.findViewById(R.id.no);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });

        skip.setText("Skip");

        txtMiddleHeadingW = (TextView) dialog.findViewById(R.id.txtMiddleHeadingW);

        ((LinearLayout)view.findViewById(R.id.home_safer_walk)).setOnClickListener(this);
        ((LinearLayout)view.findViewById(R.id.home_call)).setOnClickListener(this);
        ((LinearLayout)view.findViewById(R.id.home_message)).setOnClickListener(this);
        ((LinearLayout)view.findViewById(R.id.home_share_location)).setOnClickListener(this);
        ((LinearLayout)view.findViewById(R.id.home_delete)).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

         switch (v.getId())
        {
            case R.id.home_safer_walk :{


                 if(StaticMaster.internetStateAndSouce(getActivity().getApplicationContext()).isConnected())
                {
                    StaticMaster.SecurityParametres securityParametres = StaticMaster.isGpsAvailable(getActivity());

                     if(securityParametres.isLocationViaGps())
                    {
                        if(StaticMaster.tempGuardianDetails.getStatus().equals("true") || StaticMaster.tempGuardianDetails.getCategory().compareTo(StaticMaster.Category.Ward) == 0)
                        {
                            Intent intent = new Intent(activity.getApplicationContext(), SafeWalkSetup.class);
                            startActivity(intent);
                        }
                        else {

                            dialog.setContentView(R.layout.custom_dailog_single_button);

                            ((TextView)dialog.findViewById(R.id.message)).setText("Guardianship is not confirmed yet");

                            TextView ok = (TextView)dialog.findViewById(R.id.yes);
                            ok.setText("Ok");
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    dialog.dismiss();
                                }
                            });


                            dialog.show();
                        }

                    }else {

                         connect.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 activity.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                 dialog.dismiss();
                             }
                         });

                         launchDialog("Gps is off");
                     }

                }
                  else {

                     connect.setOnClickListener(new View.OnClickListener() {
                         @Override
                         public void onClick(View v) {
                             activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                             dialog.dismiss();
                         }
                     });

                     launchDialog("Internet is not connected");
                 }


            }break;

            case R.id.home_call :{

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + StaticMaster.tempGuardianDetails.getMobile()));
                startActivity(callIntent);


            }break;


            case R.id.home_message :{

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Write Your Message");
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, null));

            }break;


            case R.id.home_share_location :{

                    if(StaticMaster.tempGuardianDetails.getId() != null && StaticMaster.tempGuardianDetails.getStatus().equals("false"))
                {
                    Intent intent = new Intent(activity.getApplicationContext(), ShareLocationConfirm.class);
                    intent.putExtra("user_id", StaticMaster.tempGuardianDetails.getId());
                    startActivity(intent);
                }
                      else
                    {

                        Toast.makeText(getActivity().getApplicationContext(), "Guardianship is not confirmed yet", Toast.LENGTH_SHORT).show();
                    }

            }break;


            case R.id.home_delete :{

                 confirmDelete();

            }break;

        }

        dismiss();
    }


      public void launchDialog(String message)
    {
        txtMiddleHeadingW.setText(message);

        dialog.show();
    }

      public Dialog getDialog()
    {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return  dialog;
    }

    public void confirmDelete(){

        database = StaticMaster.database(activity);

        dialog.setContentView(R.layout.custom_dailog_without_image);


        String message = "Removing "+StaticMaster.tempGuardianDetails.getName()+" from your Safer family";

        TextView txtMiddleHeadingW = (TextView) dialog.findViewById(R.id.txtMiddleHeadingW);
        txtMiddleHeadingW.setText(message);


        dialog.show();
        RelativeLayout rlDialogAcceptW = (RelativeLayout) dialog.findViewById(R.id.rlDialogAcceptW);
        rlDialogAcceptW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

//                if(StaticMaster.tempGuardianDetails.getCategory().equals("guardian")) {

                    progressDialog = ProgressDialog.show(activity, "Deleting " + StaticMaster.tempGuardianDetails.getCategory(), "Please wait....", true);

                    String url = "";

                      if (StaticMaster.tempGuardianDetails.getCategory().compareTo(StaticMaster.Category.Guardian) == 0)
                    {
                        url = Networking.CommonUrl + "guardian/" + StaticMaster.tempGuardianDetails.getId();
                    }
                         else if (StaticMaster.tempGuardianDetails.getCategory().compareTo(StaticMaster.Category.Ward) == 0)
                      {
                        url = Networking.CommonUrl + "ward/" + StaticMaster.tempGuardianDetails.getId();
                      }


                    Log.w(TAG, "Delete Guardian :- " + url);

                    RequestQueue requestQueue = Volley.newRequestQueue(activity);

                    if (requestQueue != null) {


                        requestQueue.add(new JsonObjectRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject object) {

                                Log.w(TAG, object.toString());

                                try {

                                    if (object.has("status") && object.getString("status").equals("success")) {

                                        if (progressDialog != null) {
                                            progressDialog.dismiss();
                                        }

                                          if (StaticMaster.tempGuardianDetails.getCategory().compareTo(StaticMaster.Category.Guardian) == 0)
                                        {
                                            database.deleteGuardian(StaticMaster.tempGuardianDetails.getId());

                                        }
                                           else if (StaticMaster.tempGuardianDetails.getCategory().compareTo(StaticMaster.Category.Ward) == 0)

                                          {
                                            database.deleteWard(StaticMaster.tempGuardianDetails.getId());
                                          }

                                        localCallback.delete();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Networking.commonVolleyLogs(error, TAG);


                                if (progressDialog != null) {
                                    progressDialog.dismiss();
                                }

                                if (StaticMaster.tempGuardianDetails.getCategory().compareTo(StaticMaster.Category.Guardian) == 0)
                                {
                                    database.deleteGuardian(StaticMaster.tempGuardianDetails.getId());

                                }
                                else if (StaticMaster.tempGuardianDetails.getCategory().compareTo(StaticMaster.Category.Ward) == 0)

                                {
                                    database.deleteWard(StaticMaster.tempGuardianDetails.getId());
                                }

                                localCallback.delete();

                            }
                        }) {
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {

                                return StaticMaster.commonHttpHeaders(activity);
                            }

                        });

                    }

                    /*
                }else {

                    Toast.makeText(activity, "Ward Cannot be deleted", Toast.LENGTH_SHORT).show();
                }*/

            }
        });
        RelativeLayout rlDialogDeclineW = (RelativeLayout) dialog.findViewById(R.id.rlDialogDeclineW);
        rlDialogDeclineW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
    }

}
