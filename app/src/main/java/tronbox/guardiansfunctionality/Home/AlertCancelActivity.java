package tronbox.guardiansfunctionality.Home;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import tronbox.guardiansfunctionality.R;

/**
 * Created by Shrinivas Mishra on 3/3/2015.
 * page will be use for display alert message for send cancel response to server
 */
public class AlertCancelActivity extends Activity
{
    Button btnAlertCancelYes;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_cancel);
        btnAlertCancelYes = (Button)findViewById(R.id.btnAlertCancelYes);
        btnAlertCancelYes.setOnClickListener(new View.OnClickListener()
        {
        @Override
        public void onClick(View v) {
            new AlertDialog.Builder(AlertCancelActivity.this)
                .setTitle("Confirmation")
                .setMessage("Are you sure?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {

                        dialog.dismiss();


                        //TO-DO's send confirmation message for stop alert on server
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {

                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
});
}

}
