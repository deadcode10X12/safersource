/*
   This single file will be used for the following purposes :-

   1) Send Mobile Number to the server.
   2) Receive verfication code via SMS from the server.
   3) Send the Mobile Number + Verfication Code to the server.
   4) Send User Profile to the server.

 */

package tronbox.guardiansfunctionality.Login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;


public class SendCommonDataServer extends AsyncTask<String, String, String> {

    private Context context;
    private MobileDatabase database;
    private String TAG = "GuardianProgress";

    public SendCommonDataServer(Context context){

        this.context = context;


        database = new MobileDatabase(context, "GUARDIAN", null, 1);



    }

    /*
        params[0] = url where data needs to be send.
        params[1] = Data to be sent.
        params[2] = HTTP Operation Type :-  POST/PUT/GET

     */


    @Override
    protected String doInBackground(String... params) {

        try{

            HttpURLConnection httpUrlConnection = null;
            URL url = new URL(params[0]);
            httpUrlConnection = (HttpURLConnection) url.openConnection();

            httpUrlConnection.setRequestMethod(params[2]);
            httpUrlConnection.addRequestProperty("content-type", "application/json");



             if(params[2].equals("GET"))
            {
                httpUrlConnection.setDoInput(true);

            }else {

                 httpUrlConnection.setDoInput(true);
                 httpUrlConnection.setDoOutput(true);
             }

             if(SharedPrefrenceStorage.GetAuthToken(context) != null)
            {
                httpUrlConnection.addRequestProperty("authorization", "bearer " + SharedPrefrenceStorage.GetAuthToken(context));
                Log.w("AccessTokennnn", SharedPrefrenceStorage.GetAuthToken(context));

            }



            if (params[1] != null) {


                Log.w("ServerData", params[1]);

                OutputStreamWriter wr = new OutputStreamWriter(httpUrlConnection.getOutputStream());
                wr.write(params[1]);
                wr.flush();
            }



          /*  InputStream errorResponseStream = new BufferedInputStream(httpUrlConnection.getErrorStream());

            BufferedReader errorResponseStreamReader = new BufferedReader(new InputStreamReader(errorResponseStream));


            String liner = "";

            StringBuilder errorStringBuilder = new StringBuilder();
            while ((liner = errorResponseStreamReader.readLine()) != null)
            {
                errorStringBuilder.append(liner).append("\n");
            }
            errorResponseStreamReader.close();

            String errorResponse = errorStringBuilder.toString();

            Log.w("ServerErrorResponseFromServer", errorResponse);*/


            InputStream responseStream = new BufferedInputStream(httpUrlConnection.getInputStream());

            BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));

            String line = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = responseStreamReader.readLine()) != null)
            {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();

            String response = stringBuilder.toString();

            Log.w("ServerResponseFromServer", response);

            JSONObject jsonObject = new JSONObject(response);

              if(jsonObject.getString("status").equals("success"))
            {

                 switch (params[3])
                {
                    case "NewUser":{

                        Log.w(TAG, "Number Sent For Verification");

                        if(jsonObject.has("verificationCode"))
                        {


                        }

                    }break;

                    case "UserVerify":{


                        context.sendBroadcast(new Intent("Verification_Over"));

                        if(jsonObject.has("accessCode"))
                        {
                            SharedPrefrenceStorage.StoreAuthToken(context, jsonObject.getString("accessCode"));
                            Log.w("AccessTokennnn", SharedPrefrenceStorage.GetAuthToken(context));

                            new AppIdGetterId(context).execute(); // Send GCM_Id

                            Log.w(TAG, "Number is verified, and access token is stored");
                        }

                    }break;

                    case "GcmId":{


                        Log.w(TAG, "GCM Id sent to the server");

/*

                        SharedPrefrenceStorage.StoreName(context, "diva");
                        SharedPrefrenceStorage.StoreEmail(context, "diva.feng@gmail.com");
                        SharedPrefrenceStorage.StoreGender(context, "F");
                        StaticMaster.sendCommonData(context, "Add_Profile", "http://54.169.182.72/api/v0/user", "PUT");
*/


                    }break;

                    case "Add_Profile":{


                        Log.d(StaticMaster.log, "Profile is successfully stored on the server");

/*
                        StaticMaster.mobile = "9988850989";
                        StaticMaster.sendCommonData(context, "AddGuardians", "http://54.169.182.72/api/v0/guardians", "POST");
*/


                    }break;

                    case "AddGuardians":{


                        Log.w(TAG, "Guardians is added");

                        JSONArray array = jsonObject.getJSONArray("result");

                        Log.w("GuardianDetails", "Length "+array.length());

                        for(int i=0; i<array.length(); i++)
                        {
                            JSONObject ob = array.getJSONObject(i);

                            if(ob.has("guardian_id"))
                            {

                                String guardianId = ob.getString("guardian_id");

                                Log.w("GuardianDetails", "Id found "+guardianId);

                                String name = "Sammy";

                                database.insertGuardian(guardianId, name, StaticMaster.mobile, null, "pending", "no");

                                GuardianDetails guardianDetails = database.getGuardianFromId(guardianId);

                                Log.d(StaticMaster.log, "Guardians has been inserted into the database");

                                Log.d(StaticMaster.log, "Guardian Inserted :- "+guardianDetails.getName()+" "+guardianDetails.getMobile()+" "+guardianDetails.getStatus());

                            }
                        }

                    }break;

                    case "Send_SOS":{


                        if(jsonObject.has("id"))
                        {
                            StaticMaster.ActivityId = jsonObject.getString("id");
                            SharedPrefrenceStorage.StoreActivityid(context, StaticMaster.ActivityId);
                            Log.w("SOSId", StaticMaster.ActivityId);
                        }

                    }break;

                    case "Start_Safewalk":{

                         if(jsonObject.has("id"))
                        {
                          StaticMaster.ActivityId = jsonObject.getString("id");
                          SharedPrefrenceStorage.StoreActivityid(context, StaticMaster.ActivityId);
                          Log.w("SafewalkId", StaticMaster.ActivityId);
                        }

                    }break;

                    case "Get_Location_ActivityId":{

                         if(jsonObject.has("location"))
                        {
                            JSONObject object = jsonObject.getJSONObject("location");
                            SharedPrefrenceStorage.StoreVictimCoordinates(context, object.getString("lat")+"@"+object.getString("lng"));

                            context.sendBroadcast(new Intent("SafeWalk_Update"));
                        }

                    }break;

                    case "Get_SOS_Data_ActivityId":{

                         if(jsonObject.has("result"))
                        {
                            JSONObject master = jsonObject.getJSONObject("result");

                            JSONArray array = master.getJSONArray("guardians");

                              for(int i=0; i<array.length(); i++)
                            {
                               JSONObject temp = array.getJSONObject(i);

                                String id = temp.getString("user_id");
                                String name = temp.getString("name");
                                String mobile = temp.getString("primaryContact");


                                Log.w(TAG, "Guardian "+id+"_"+name+"_"+mobile);

                               database.insertTemp(id, name, mobile, null, null);
                            }

                            JSONObject victim = master.getJSONObject("victim");

                            String id = victim.getString("user_id");
                            String name = victim.getString("name");
                            String mobile = victim.getString("primaryContact");
                            String lat = victim.getString("lat");
                            String lng = victim.getString("lng");

                            SharedPrefrenceStorage.StoreVictimCoordinates(context, lat+"@"+lng);

                            Log.w(TAG, "Victim "+id+"_"+name+"_"+mobile+"_"+lat+"_"+lng);
                        }
                    }break;

                    case "Register_Device" :{

                         if(jsonObject.has("reason"))
                        {
                            String result = jsonObject.getString("reason");

                              if(result.equals("deviceAlreadyRegistered"))
                            {


                            }

                        }else { // this is a new device, register it

                             SharedPrefrenceStorage.StoreMacAdd(context, StaticMaster.tempData);
                         }

                    }break;

                    case "Forget_Device": {

                        SharedPrefrenceStorage.StoreMacAdd(context, null);

                    }break;
                }

                if(jsonObject.has("user"))
                {
                    JSONObject userObject = jsonObject.getJSONObject("user");

                    SharedPrefrenceStorage.StoreUserId(context, userObject.getString("_id"));
                    Log.w("User_Id", SharedPrefrenceStorage.GetUserId(context));
                }

            }



        } catch(IOException io){}
        catch (JSONException e) {e.printStackTrace();}

        return null;
    }

}
