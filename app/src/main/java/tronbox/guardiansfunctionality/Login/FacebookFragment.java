package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class FacebookFragment extends Fragment {

    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;


    private OnFragmentInteractionListener mListener;
    private EditText firstName, email;
    private Button signUp;
    private Spinner gender;
    private ImageView profilePic;
    private int REQUEST_IMAGE_CAPTURE = 101;

    private ArrayList<String> genders = new ArrayList<String>();

    private static final String TAG = "MainFragment";
    private UiLifecycleHelper uiHelper;

    UserRegisterActivity context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.user_profile, container, false);

        LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
        authButton.setFragment(this);
        authButton.setReadPermissions(Arrays.asList("email"));



        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            mListener = (OnFragmentInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {

            downloadPic(session);

            Log.i(TAG, "Logged in...");
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed()) ) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    public void downloadPic(Session session)
    {
        Bundle params = new Bundle();
        params.putBoolean("redirect", false);
        params.putString("height", "200");
        params.putString("type", "normal");
        params.putString("width", "200");

       Request profileRequest = new Request(
               session,
               "/me",
               null,
               HttpMethod.GET,
               new Request.Callback() {
                   public void onCompleted(Response response) {

                         if(response.getRawResponse() != null)
                       {
                           try {

                               JSONObject object = new JSONObject(response.getRawResponse());

                               if(object.has("email"))
                               {

                                   String email = object.getString("email");

                                   SharedPrefrenceStorage.StoreEmail(getActivity(), email);

                                   Log.w("FacebookData", SharedPrefrenceStorage.GetEmail(getActivity()));

                               }



                               if(object.has("first_name"))
                               {

                                   String name = object.getString("first_name");

                                   if(object.has("last_name"))
                                   {
                                       name = name.concat(" "+object.getString("last_name"));

                                   }

                                   SharedPrefrenceStorage.StoreName(getActivity(), name);

                                   Log.w("FacebookData", SharedPrefrenceStorage.GetName(getActivity()));
                               }


                               if(object.has("gender"))
                               {

                                   String gender = object.getString("gender");

                                   SharedPrefrenceStorage.StoreGender(getActivity(), gender);


                                   Log.w("FacebookData", SharedPrefrenceStorage.GetGender(getActivity()));
                               }

                           } catch (JSONException e) {
                               e.printStackTrace();
                           }
                       }

                   }
               }
       );

       Request pictureRequest = new Request(

                session,
                "/me/picture",
                params,
                HttpMethod.GET,
                new Request.Callback() {
                    public void onCompleted(Response response) {

                        if(response.getRawResponse() != null){

                            try {

                                String profilePicUrl = new JSONObject(response.getRawResponse()).getJSONObject("data").getString("url");
                                SharedPrefrenceStorage.StoreProfilePic(getActivity(), profilePicUrl);


                                downloadImage(profilePicUrl);

                                Log.w("BatchRequestPicture", SharedPrefrenceStorage.GetProfilePic(getActivity()));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }
                }
        );


        RequestBatch batch = new RequestBatch();
        batch.add(profileRequest);
        batch.add(pictureRequest);
        batch.addCallback(new RequestBatch.Callback() {
            @Override
            public void onBatchCompleted(RequestBatch batch) {

                Log.w("BatchRequestP", "Complete");
            }
        });
        batch.executeAsync();
    }


    public void downloadImage(String url){

        HandlerThread thread = new HandlerThread("Images");
        thread.start();
        new DownloadImageHandler(thread.getLooper(), getActivity().getApplicationContext(), url);
    }

}
