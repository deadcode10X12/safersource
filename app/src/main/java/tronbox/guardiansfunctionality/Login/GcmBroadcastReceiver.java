package tronbox.guardiansfunctionality.Login;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.HandleGuardians.InvitePendingGuardians;
import tronbox.guardiansfunctionality.Maps.SaferWalkLiveFeed;
import tronbox.guardiansfunctionality.Maps.ShareLocationActivity;
import tronbox.guardiansfunctionality.Home.ActivityLifeCycleHandler;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.Maps.MapDrawDecoder;
import tronbox.guardiansfunctionality.Maps.SOS;
import tronbox.guardiansfunctionality.Maps.SafeWalk;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;


public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(final Context context, Intent intent) {


        Bundle bundle = intent.getExtras();

        MobileDatabase database = new MobileDatabase(context, "GUARDIAN", null, 1);

        printBundleContent(bundle);

         if(bundle.containsKey("id") || bundle.containsKey("event"))
        {
            String userId = bundle.getString("id");

            if(bundle.containsKey("event"))
            {
                userId = "120";

            }

            String Message = bundle.getString("message");

            final NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher))
                            .setContentTitle("Safer")
                            .setContentText(Message);


            mBuilder.setSound(Uri.parse("android.resource://"
                    + context.getPackageName() + "/" + R.raw.notification));

//            mBuilder.setSound(new RingtoneManager(context).getActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE));

            Intent resultIntent = null;


            switch (userId)
            {
                case "101":{

                    resultIntent = new Intent(context, InvitePendingGuardians.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    resultIntent.putExtra("type", "ward");

                    final String id = bundle.getString("ward_id");

                    Log.w(StaticMaster.log, "GCM - Message = "+Message+"\n Id = "+userId+"\n Ward_Id = "+id);

                    Volley.newRequestQueue(context).add(new JsonObjectRequest(Request.Method.GET,"http://54.169.182.72/api/v0/ward/"+id, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject jsonObject) {

                            try {

                                if(jsonObject.has("ward"))
                                {
                                    JSONObject master = jsonObject.getJSONObject("ward");

                                    String name = master.getString("name");
                                    String mobile = master.getString("primaryContact");
                                    String photo = master.getString("photo");

                                    StaticMaster.aliasManager(id, name, mobile, photo, "true", "ward", context);

                                    Networking.DownloadAndStoreImage(context, photo, id);

                                    // Remove the notification request

                                }

                            }catch (JSONException e){}


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {

                        }
                    }){
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {


                            HashMap<String, String> params = new HashMap<String, String>();

                            params.put("Content-Type", "application/json");

                            if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                            {
                                String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                                Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                                params.put("authorization", "bearer " + accessToken);
                            }



                            return params;
                        }

                    });


//                    StaticMaster.sendCommonData(context, "Download_Ward_Profile", "http://54.169.182.72/api/v0/ward/"+StaticMaster.tempData, Request.Method.GET);

                }break;

                case "102":{ // Guardian has accepted your gardian_request let's download his profile


                    resultIntent = new Intent(context, HomeActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    String guardianId = bundle.getString("guardian_id");
                    resultIntent.putExtra("guardian_id", guardianId);

                    Log.d(StaticMaster.log, "GCM - Message = "+Message+"\n Id = "+userId+"\n Ward_Id = "+guardianId);

                    database.updateGuardianStatus(guardianId, "true", 4);

                    GuardianDetails guardianDetails = database.getGuardianFromId(guardianId);
                    Log.d(StaticMaster.log, guardianDetails.getName()+" is added as your guardian his status is "+guardianDetails.getStatus());

                    context.sendBroadcast(new Intent("Refresh_Home"));

                }break;

                case "109":{

                    resultIntent = new Intent(context, HomeActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    Intent intent1 = new Intent(context, HomeActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent1);


                }break;

                case "104":{ // Rcvd SOS Request


                    resultIntent = new Intent(context, SOS.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    String lat = bundle.getString("lat");
                    String lng = bundle.getString("lng");
                    String activityId = bundle.getString("activityID");
                    String victimId = bundle.getString("user_id");


                    SharedPrefrenceStorage.StoreActivityid(context, activityId);
                    SharedPrefrenceStorage.StoreActivityUserid(context, victimId);

                    SharedPrefrenceStorage.StoreVictimCoordinates(context, lat+"@"+lng);

                    new MapDrawDecoder(context, SharedPrefrenceStorage.GetUserCoordinates(context), SharedPrefrenceStorage.GetVictimCoordinates(context));

                      if(SharedPrefrenceStorage.GetRunningActivity(context) != null && SharedPrefrenceStorage.GetRunningActivity(context).equals("SafeWalk"))
                    {
                        context.sendBroadcast(new Intent("StopSaferWalk"));
                    }

                }break;


                case "105":{ // Rcvd safewalk request

                    resultIntent = new Intent(context, SafeWalk.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    String activityId = bundle.getString("safewalkID");
                    String src[] = bundle.getString("src").split(",");
                    String dst[] = bundle.getString("dest").split(",");
                    String victimId = bundle.getString("user_id");

                    String Source = src[0].replace("[\"", "").replace("\"", "")+"@"+src[1].replace("\"]", "").replace("\"", "");
                    String Destination = dst[0].replace("[\"", "").replace("\"", "")+"@"+dst[1].replace("\"]", "").replace("\"", "");

                    // Store the safer walk case id
                    SharedPrefrenceStorage.StoreSaferWalkCaseid(context, activityId);

                    // Store the safer walk source
                    SharedPrefrenceStorage.StoreSaferWalkSource(context, Source);

                    // Store the safer walk destination
                    SharedPrefrenceStorage.StoreSaferWalkDestination(context, Destination);

                    // Store the safer walk initiator
                    SharedPrefrenceStorage.StoreSaferWalkInitiator(context, victimId);

                    // Store the safer walk listener
                    SharedPrefrenceStorage.StoreSaferWalkListener(context, "Me");



                    SharedPrefrenceStorage.StoreActivityid(context, activityId);
                    SharedPrefrenceStorage.StoreActivityUserid(context, victimId);

                    SharedPrefrenceStorage.StoreVictimCoordinates(context, Source);


                    NotificationCompat.InboxStyle inboxStyle =  new NotificationCompat.InboxStyle();
                    inboxStyle.setBigContentTitle("Safer");

                    String[] arr = Message.split(" ");

                    if(arr.length > 7) // we need to split
                    {
                        inboxStyle.addLine(Message.substring(0, Message.indexOf(arr[7])));
                        inboxStyle.addLine(Message.substring(Message.indexOf(arr[7]), Message.length()));

                    }else {

                        inboxStyle.addLine(Message);
                    }

                    mBuilder.setStyle(inboxStyle);

                    resultIntent.putExtra("Source", Source);
                    resultIntent.putExtra("Destination", Destination);
                    resultIntent.putExtra("user", false);
                    resultIntent.putExtra("user_id", victimId);

                    Log.w("GCM_CODE_TRACKING", "ActivityId = "+activityId+"\n Src = "+Source+"\n User_Id = "+victimId+"\n Dst = "+Destination);

                }break;

                case "120": {

                    String mobile = bundle.getString("from");


                    ArrayList<GuardianDetails> guardianDetails = new ArrayList<>();

                    for(GuardianDetails details : database.getAllGuardians())
                    {
                        guardianDetails.add(details);
                    }

                    for(GuardianDetails details : database.getAllWard())
                    {
                        guardianDetails.add(details);
                    }

                    Log.w("Notification_Data", mobile);

                    for(GuardianDetails details : guardianDetails)
                    {

                        Log.w("Notification_Data", details.getMobile());

                        if(details.getMobile().equals(mobile))
                        {
                            StaticMaster.tempGuardianDetails = details;
                            Log.w("Notification_Data", details.getName());
                        }

                    }

                      if(StaticMaster.tempGuardianDetails == null)
                    {

                        Log.w("Notification_Data", "No guardian found");
                    }

                    NotificationCompat.InboxStyle inboxStyle =  new NotificationCompat.InboxStyle();
                    inboxStyle.setBigContentTitle("Safer");

                    String[] arr = Message.split(" ");

                      if(arr.length > 5) // we need to split
                    {
                        inboxStyle.addLine(Message.substring(0, Message.indexOf(arr[5])));
                        inboxStyle.addLine(Message.substring(Message.indexOf(arr[5]), Message.length()));

                    }else {

                          inboxStyle.addLine(Message);
                      }

                    inboxStyle.addLine("Do you want to share ?");


                    mBuilder.setStyle(inboxStyle);

                    resultIntent = new Intent(context, HomeActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    Intent yesIntent = new Intent(context, HomeActivity.class);
                    yesIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    yesIntent.setAction("Yes");
                    yesIntent.putExtra("data", "Notification_Data");

                    PendingIntent yesPendingIntent = PendingIntent.getActivity(context,0,yesIntent,PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.addAction(R.drawable.ic_action_accept, "Yes", yesPendingIntent);


                    Intent noIntent = new Intent(context, HomeActivity.class);
                    noIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    noIntent.setAction("No");
                    noIntent.putExtra("data", "Notification_Data");

                    PendingIntent noPendingIntent = PendingIntent.getActivity(context,0,noIntent,PendingIntent.FLAG_UPDATE_CURRENT);
                    mBuilder.addAction(R.drawable.ic_action_cancel, "No", noPendingIntent);



                }break;

                case "108" :{


                    String wardId = bundle.getString("ward_id");
                    StaticMaster.database(context).deleteWard(wardId);

                    resultIntent = new Intent(context, HomeActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                     if(!ActivityLifeCycleHandler.isApplicationVisible())
                    {

                        Intent noIntent = new Intent(context, HomeActivity.class);
                        noIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(noIntent);

                        return;
                    }

                }break;


                case "114" :{


                    String guardianId = bundle.getString("guardian_id");
                    StaticMaster.database(context).deleteGuardian(guardianId);

                    resultIntent = new Intent(context, HomeActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    if(!ActivityLifeCycleHandler.isApplicationVisible())
                    {

                        Intent noIntent = new Intent(context, HomeActivity.class);
                        noIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(noIntent);

                        return;
                    }

                }break;

                case "113":{

                    String id = bundle.getString("sender");

                    String lat = bundle.getString("lat");
                    String lng = bundle.getString("lng");


                    resultIntent = new Intent(context, ShareLocationActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    resultIntent.putExtra("lat", lat);
                    resultIntent.putExtra("lng", lng);
                    resultIntent.putExtra("id", id);

                }break;


                case "110":{ // Stop the SaferWalk

                    String id = bundle.getString("safewalkID");


                    NotificationManager mNotifyMgr = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotifyMgr.cancelAll();

                     if(id.equals(SharedPrefrenceStorage.GetActivityId(context)))
                    {
                        context.sendBroadcast(new Intent("StopSaferWalk"));

                        return;
                    }

                }break;


            }

            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            context,
                            Integer.parseInt(userId),
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            mBuilder.setContentIntent(resultPendingIntent);

            NotificationManager mNotifyMgr =(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);


            Notification notification = mBuilder.build();

             if(userId.equals("105")) // Rcvd SaferWalk Request
            {

                notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

            }

            mNotifyMgr.notify(Integer.parseInt(userId), notification);

        }

    }

     public void printBundleContent(Bundle bundle)
    {
         for(String key : bundle.keySet())
        {
            Log.w("GCM_CODE_TRACKING", key+" - "+bundle.getString(key));
        }
    }

}