package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class SignUpFraggy extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;


    private OnFragmentInteractionListener mListener;
    private EditText firstName, email;
    private Button signUp;
    private Spinner gender;
    private SpinnerAdapter genderAdapter;
    private ImageView profilePic;
    private int REQUEST_IMAGE_CAPTURE = 101;

    private ArrayList<String> genders = new ArrayList<String>();

    private static final String TAG = "MainFragment";
    private UiLifecycleHelper uiHelper;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);


        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN) // add permissions here
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .build();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.user_profile, container, false);

        LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
        authButton.setFragment(this);
        authButton.setReadPermissions(Arrays.asList("email"));


        view.findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!mGoogleApiClient.isConnecting()) {
                    mSignInClicked = true;
                    //resolveSignInError();

                    mGoogleApiClient.connect();
                }
            }
        });


        profilePic = (ImageView)view.findViewById(R.id.agent_pic);
        profilePic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "Door Pic" + String.valueOf(new Random().nextInt()));
                values.put(MediaStore.Images.Media.DESCRIPTION, "Door Wizard");
                StaticMaster.imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                    final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, StaticMaster.imageUri);
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);

            }
        });

        firstName = (EditText)view.findViewById(R.id.agent_first_name);
        email = (EditText)view.findViewById(R.id.agent_email);


        signUp = (Button)view.findViewById(R.id.sign_up);

        gender = (Spinner)view.findViewById(R.id.agent_gender);

        genders.add("Male");
        genders.add("Female");

        genderAdapter = new SpinnerAdapter(getActivity(), R.layout.gender_spinner_text, genders, "Enter Your Gender");
        gender.setAdapter(genderAdapter);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new AppIdGetterId(getActivity()).execute();

            }
        });


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            mListener = (OnFragmentInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    class SpinnerAdapter extends ArrayAdapter<String> {

        Context context;
        int resource;
        ArrayList<String> list;
        String text;

        public SpinnerAdapter(Context context, int resource, ArrayList<String> list, String text){
            super(context, resource, list);

            this.context = context;
            this.resource = resource;
            this.list = list;
            this.text = text;

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            TextView view = null;

            LayoutInflater inflater = LayoutInflater.from(context);
            view = (TextView)inflater.inflate(resource,parent,false);
            view.setBackgroundResource(R.drawable.dropdown);

            if(position == 0){

                if(list.get(0).equals("Male")){

                    view.setText("Male");

                }else {

                    view.setText("India");
                }


            }else {

                view.setText(list.get(position));
            }

            return view;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_IMAGE_CAPTURE){

            Bitmap bitmap = decodeFile(getRealPathFromURI(getActivity(), StaticMaster.imageUri));

            if(bitmap != null){

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);

                profilePic.setImageBitmap(bitmap);

            }else {

                Toast.makeText(getActivity(), "Out Of Memory, Kindly clear some memory", Toast.LENGTH_LONG).show();

            }

        }else if (requestCode == RC_SIGN_IN) {
            if (resultCode != getActivity().RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }

        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    public Bitmap decodeFile(String path) {

        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            // The new size we want to scale to
            final int REQUIRED_SIZE_WIDTH = 200; //(int)Master.canvasWidth;
            final int REQUIRED_SIZE_HEIGHT = 200; //(int)Master.canvasHeight;


            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE_WIDTH && o.outHeight / scale / 2 >= REQUIRED_SIZE_HEIGHT)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                getActivity().startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    public void onConnectionFailed(ConnectionResult result) {

        if (!mIntentInProgress) {
            // Store the ConnectionResult so that we can use it later when the user clicks
            // 'sign-in'.
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }

    }


    @Override
    public void onConnected(Bundle bundle) {

        mSignInClicked = false;
        Toast.makeText(getActivity(), "User is connected!", Toast.LENGTH_LONG).show();

        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

            String personName = currentPerson.getDisplayName();
            String personPhoto = currentPerson.getImage().toString().replace("\\","").replace("sz=50", "sz=100");
            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

            String gender = null;

            if(currentPerson.getGender() == 0){
                gender = "Male";

            }else if(currentPerson.getGender() == 1){
                gender = "Female";

            }else if(currentPerson.getGender() == 2){

                gender = "Other";
            }

            downloadImage(personPhoto);

            SharedPrefrenceStorage.StoreEmail(getActivity(), email);
            Log.w("BatchRequestProfile", SharedPrefrenceStorage.GetEmail(getActivity()));

            SharedPrefrenceStorage.StoreName(getActivity(), personName);
            Log.w("BatchRequestProfile", SharedPrefrenceStorage.GetName(getActivity()));

            SharedPrefrenceStorage.StoreProfilePic(getActivity(), personPhoto);
            Log.w("BatchRequestProfile", SharedPrefrenceStorage.GetProfilePic(getActivity()));

            SharedPrefrenceStorage.StoreGender(getActivity(), gender);
            Log.w("BatchRequestProfile", SharedPrefrenceStorage.GetGender(getActivity()));

        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {

            downloadPic(session);

            Log.i(TAG, "Logged in...");
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed()) ) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    public void downloadPic(Session session)
    {
        Bundle params = new Bundle();
        params.putBoolean("redirect", false);
        params.putString("height", "200");
        params.putString("type", "normal");
        params.putString("width", "200");

       Request profileRequest = new Request(
               session,
               "/me",
               null,
               HttpMethod.GET,
               new Request.Callback() {
                   public void onCompleted(Response response) {

                         if(response.getRawResponse() != null)
                       {
                           try {

                               JSONObject object = new JSONObject(response.getRawResponse());

                               if(object.has("email"))
                               {

                                   String email = object.getString("email");

                                   SharedPrefrenceStorage.StoreEmail(getActivity(), email);

                                   Log.w("BatchRequestProfile", SharedPrefrenceStorage.GetEmail(getActivity()));

                               }



                               if(object.has("first_name"))
                               {

                                   String name = object.getString("first_name");

                                   if(object.has("last_name"))
                                   {
                                       name = name.concat(" "+object.getString("last_name"));

                                   }

                                   SharedPrefrenceStorage.StoreName(getActivity(), name);

                                   Log.w("BatchRequestProfile", SharedPrefrenceStorage.GetName(getActivity()));
                               }


                               if(object.has("gender"))
                               {

                                   String gender = object.getString("gender");

                                   SharedPrefrenceStorage.StoreGender(getActivity(), gender);


                                   Log.w("BatchRequestProfile", SharedPrefrenceStorage.GetGender(getActivity()));
                               }

                           } catch (JSONException e) {
                               e.printStackTrace();
                           }
                       }

                   }
               }
       );

       Request pictureRequest = new Request(

                session,
                "/me/picture",
                params,
                HttpMethod.GET,
                new Request.Callback() {
                    public void onCompleted(Response response) {

                        if(response.getRawResponse() != null){

                            try {

                                String profilePicUrl = new JSONObject(response.getRawResponse()).getJSONObject("data").getString("url");
                                SharedPrefrenceStorage.StoreProfilePic(getActivity(), profilePicUrl);


                                downloadImage(profilePicUrl);

                                Log.w("BatchRequestPicture", SharedPrefrenceStorage.GetProfilePic(getActivity()));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }
                }
        );


        RequestBatch batch = new RequestBatch();
        batch.add(profileRequest);
        batch.add(pictureRequest);
        batch.addCallback(new RequestBatch.Callback() {
            @Override
            public void onBatchCompleted(RequestBatch batch) {

                Log.w("BatchRequestP", "Complete");
            }
        });
        batch.executeAsync();
    }

      public void sendProfileToServer(String name, String email, String gender)
    {
        JSONObject post_data = new JSONObject();

        try {

            post_data.put("name", name);
            post_data.put("email", email);
            post_data.put("gender", gender);

            Log.w("JsonData", String.valueOf(post_data));

            new SendCommonDataServer(getActivity()).execute("http://54.169.182.72/api/v0/user", String.valueOf(post_data));

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    public void downloadImage(String url){

        HandlerThread thread = new HandlerThread("Images");
        thread.start();
        new DownloadImageHandler(thread.getLooper(), getActivity().getApplicationContext(), url);
    }

}
