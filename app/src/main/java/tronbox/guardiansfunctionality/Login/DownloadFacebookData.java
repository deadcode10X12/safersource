package tronbox.guardiansfunctionality.Login;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DownloadFacebookData extends AsyncTask<String, Void, String> {

    private Context context;
    private HandlerThread thread;
    private String id = "null", name = "null", pic = "null", location = "null", birthday = "null", gender = "null";
    private HashMap<String,String> mapEntries;
    public DownloadFacebookData(Context context){
        this.context = context;
    }

    private StringBuilder builder = new StringBuilder();



    @Override
    protected String doInBackground(String... params) {

        thread = new HandlerThread("Images");
        thread.start();

        mapEntries = new HashMap<String,String>();

        String myUrl = "https://graph.facebook.com/me?fields=id,name,birthday,location,gender,picture,email.limit(500).fields(id,name,picture)&format=json&access_token="+params[0];

        try {

            HttpURLConnection httpConnection = (HttpURLConnection)new URL(myUrl).openConnection();
            httpConnection.connect();

            InputStream inputStream = httpConnection.getInputStream();
            Reader reader = new InputStreamReader(inputStream);
            BufferedReader bufferReader = new BufferedReader(reader);

            StringBuilder build = new StringBuilder();
            String temp;

            while((temp = bufferReader.readLine()) != null){
                build.append(temp);
            }


            Log.w("NewGesponseeeeee", build.toString());

            JSONObject master = new JSONObject(build.toString());

            if(master.has("birthday")){

                birthday = master.getString("birthday");

                Log.w("ProfileFacebookBirthDay", birthday);

            }

            if(master.has("name")){

                name = master.getString("name");
                Log.w("ProfileFacebookName", name);


            }

            if(master.has("gender"));
            {
                gender = master.getString("gender");

                Log.w("ProfileFacebookGender", gender);


            }

            if(master.has("id")){

                id = master.getString("id");

                Log.w("ProfileFacebookid", id);


            }

            if(master.has("picture")){


                pic = master.getJSONObject("picture").getJSONObject("data").getString("url");




                mapEntries.put(id+".png", pic);

                Log.w("ProfileFacebookpic", pic);


            }

            if(master.has("location")){


                location = master.getJSONObject("location").getString("name");

                Log.w("ProfileFacebookLocation", location);


            }


            if(builder.length() > 3){
                Log.w("DANGER", builder.toString());
            }

        }

        catch (MalformedURLException e){ Log.e("Facebook_Connect_Error", "Url is malformed " + e.getLocalizedMessage());}
        catch (IOException e) {
            Log.e("Facebook_Connect_Error", "IO Error " + e.getLocalizedMessage());}
        catch (JSONException e) {
            Log.e("Facebook_Connect_Error", "Jason Error " + e.getLocalizedMessage());}

        return null;
    }



}

