/*
   This is the splash screen it will appear for 3 seconds then it will forward you to your last screen,
   it will appear in a full screen mode
*/


package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;

import tronbox.guardiansfunctionality.GuardianBrain;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;

  public class SplashActivity extends Activity
{

    Context myContext=SplashActivity.this;
    int IntervalTime=0;
    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState)

    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        startService(new Intent(getApplicationContext(), GuardianBrain.class));

    }

      Runnable run = new Runnable()
    {
        @Override
        public void run()
        {
              try
            {
                // jump to the next screen depending upon last position

                   switch (SharedPrefrenceStorage.GetPosition(getApplicationContext()))
                {
                    case "SplashActivity":{

                        SharedPrefrenceStorage.StorePosition(myContext, "MobileVerificationActivity");

                        Intent it = new Intent(myContext, MobileVerificationActivity.class);
                        startActivity(it);

                    }break;

                    case "MobileVerificationActivity": {

                        Intent it = new Intent(myContext, MobileVerificationActivity.class);
                        startActivity(it);

                    }break;

                    case "MobileVerificationCodeActivity":{

                        Intent it = new Intent(myContext, MobileVerificationCodeActivity.class);
                        startActivity(it);


                    }break;

                    case "UserRegisterActivity":{

                        Intent it = new Intent(myContext, UserRegisterActivity.class);
                        startActivity(it);

                    }break;

                    case "DeviceSyncActivity":{


                        Intent it = new Intent(myContext, DeviceSyncActivity.class);
                        startActivity(it);

                    }break;

                    case "ContactListActivity":{

                        Intent it = new Intent(myContext, ContactListActivity.class);
                        startActivity(it);

                    }

                    case "HomeActivity":{

                        Intent it = new Intent(myContext, HomeActivity.class);
                        startActivity(it);

                    }

                     default:{

                         Intent it = new Intent(myContext, HomeActivity.class);
                         startActivity(it);

                     }

                }


            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }finally {
                  finish();
              }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        IntervalTime=3;
        handler.postDelayed(run, IntervalTime*1000);

    }

    @Override
    public void onBackPressed() {

        finish();

        super.onBackPressed();
    }

    @Override
    public void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);

        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

    }
}
