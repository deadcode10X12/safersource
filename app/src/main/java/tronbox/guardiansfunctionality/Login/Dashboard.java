/*
package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.Maps.MapDrawDecoder;
import tronbox.guardiansfunctionality.Maps.MyCurrentLocation;
import tronbox.guardiansfunctionality.Maps.SOS;
import tronbox.guardiansfunctionality.Maps.SafeWalkSetup;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;


public class Dashboard extends Activity {

    Button connect;
    Intent serviceIntent;
    CommonFunctions comFunc=new CommonFunctions();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        connect = (Button)findViewById(R.id.connect);


        //serviceIntent = new Intent(getApplicationContext(), GuardianBrain.class);
        startService(serviceIntent);

        registerReceiver(receiver, new IntentFilter("connect"));
        registerReceiver(receiver2, new IntentFilter("send_sos"));

        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()){

            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 101);

        }

//        new MapDrawDecoder(getApplicationContext(), SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()), SharedPrefrenceStorage.GetVictimCoordinates(getApplicationContext()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK)
        {
            if(requestCode == 101)
            {

                sendBroadcast(new Intent("StartScan"));

            }
        }

    }

    @Override
    public void finish() {
        super.finish();

        unregisterReceiver(receiver);
        unregisterReceiver(receiver2);

    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

             String data = intent.getExtras().getString("data");

            if(data.equals("connected")){

                connect.setText("Connected");

                //StaticMaster.sendCommonData(getApplicationContext(), "Send_SOS", "http://54.169.182.72/api/v0/sos", "POST");


            }else if(data.equals("disconnected")) {

//                stopService(serviceIntent);

                connect.setText("Disconnected");


            }

        }
    };


    BroadcastReceiver receiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Toast.makeText(getApplicationContext(), "SOS_Send", Toast.LENGTH_SHORT).show();

            Intent intent1 = new Intent(getApplicationContext(), MyCurrentLocation.class);
            startActivity(intent1);

*/
/*
            StaticMaster.sendCommonData(getApplicationContext(), "Send_SOS", "http://54.169.182.72/api/v0/sos", "POST");*//*



        }
    };


}


 // Commands Control Center

1) Add New User

    StaticMaster.mobile = "9966650989"; // guy-1 = "9922250989", guy-2 = "9933350989"
    StaticMaster.sendCommonData(getApplicationContext(), "NewUser", "http://54.169.182.72/api/v0/user", "POST");

2) Add Fake Profile


        SharedPrefrenceStorage.StoreName(getApplicationContext(), "Riya");
        SharedPrefrenceStorage.StoreEmail(getApplicationContext(), "patilnikhil99@gmail.com");
        SharedPrefrenceStorage.StoreGender(getApplicationContext(), "F");

        StaticMaster.sendCommonData(getApplicationContext(), "Add_Profile", "http://54.169.182.72/api/v0/user", "PUT");

4) Send a GCM ID

        StaticMaster.sendCommonData(context, "GcmId", "http://54.169.182.72/api/v0/subscribe", "POST");


3) Add a Guardian

        StaticMaster.mobile = "9944450989";
        StaticMaster.sendCommonData(getApplicationContext(), "AddGuardians", "http://54.169.182.72/api/v0/guardians", "POST");

4) Send SafeWalk Request


        StaticMaster.contactsList.clear();
        StaticMaster.contactsList.add("9977750989");

        Intent intent = new Intent(getApplicationContext(), SafeWalkSetup.class);
        startActivity(intent);

on Start Button Pressed -> StaticMaster.sendCommonData(getApplicationContext(), "Start_Safewalk", "http://54.169.182.72/api/v0/safewalk", "POST");

5) Update Location with Safewalk_Id


      StaticMaster.sendCommonData(getApplicationContext(), "Update_Location_ActivityId", "http://54.169.182.72/api/v0/safewalk/"+ SharedPrefrenceStorage.GetActivityId(getApplicationContext())+"/location", "PUT");

6) Get Updated Location With Safewalk_Id


      StaticMaster.sendCommonData(getApplicationContext(), "Get_Location_ActivityId", "http://54.169.182.72/api/v0/safewalk/"+ SharedPrefrenceStorage.GetActivityId(getApplicationContext())+"/location", "GET");

7) Send SOS Request

 StaticMaster.sendCommonData(getApplicationContext(), "Send_SOS", "http://54.169.182.72/api/v0/sos", "POST");


8) Update Location with SOS_Id

      StaticMaster.sendCommonData(getApplicationContext(), "Update_Location_ActivityId", "http://54.169.182.72/api/v0/activity/"+ SharedPrefrenceStorage.GetActivityId(getApplicationContext())+"/location", "PUT");

9) Get Updated Location With SOS_Id

      StaticMaster.sendCommonData(getApplicationContext(), "Get_SOS_Data_ActivityId", "http://54.169.182.72/api/v0/activity/"+ SharedPrefrenceStorage.GetActivityId(getApplicationContext())+"/locations?profile=1&volunteer=0", "GET");

10) Store Mac Address

     StaticMaster.sendCommonData(getApplicationContext(), "Register_Device", "http://54.169.182.72/api/v0/user/device", "PUT");

11) Forget The Device

  StaticMaster.sendCommonData(getApplicationContext(), "Forget_Device", "http://54.169.182.72/api/v0/user/device", "DELETE");

12) Send Feedback

     StaticMaster.sendCommonData(getApplicationContext(), "Send_Feedback", "http://54.169.182.72/api/v0/user/feedback", "POST");


*/
