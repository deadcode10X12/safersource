package tronbox.guardiansfunctionality.Login;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.Device.BlueGigBrain;
import tronbox.guardiansfunctionality.Device.SelectNearDevice;
import tronbox.guardiansfunctionality.GuardianBrain;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 3/18/2015.
 */
public class DeviceSyncActivity extends FragmentActivity
{
    Context myContext=DeviceSyncActivity.this;
    private ProgressBar prgSpinner;
    Boolean bProgressTrue=false;
    LinearLayout llSkipNext;

    TextView txtSyncHeading1,txtSyncHeading2,txtSyncHeading3,txtSyncHeadingTop,txtSkipDS;

    Typeface robotoFont;

    private int Request_Id = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        robotoFont = StaticMaster.RobotoFont(getApplicationContext());

        StaticMaster.CurrentState = StaticMaster.States.Normal;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_device);


        UIDeclaration();
        prgSpinner.setVisibility(View.GONE);
        LinearLayout llSyncBtn=(LinearLayout)findViewById(R.id.llSyncBtn);
        llSyncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bProgressTrue == false) {

                    if (!BluetoothAdapter.getDefaultAdapter().isEnabled()){

                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, Request_Id);

                    }
                    else {

                        prgSpinner.setVisibility(View.VISIBLE);
                        bProgressTrue = true;

                        SelectNearDevice selectNearDevice = new SelectNearDevice();
                        selectNearDevice.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar);
                        selectNearDevice.show(getSupportFragmentManager().beginTransaction(), "dialog");


                        Intent intent1 = new Intent("StartScan");
                        intent1.putExtra("data", "scan");
                        sendBroadcast(intent1);

                    }

                } else {

                    prgSpinner.setVisibility(View.GONE);
                    bProgressTrue = false;
                }
            }
        });

        llSkipNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                nextScreen();
            }
        });

    }

    //method for declare all UI controls
    protected void UIDeclaration()
    {

        prgSpinner=(ProgressBar)findViewById(R.id.prgSpinner);

        txtSkipDS = (TextView)findViewById(R.id.txtSkipDS);
        txtSkipDS.setTypeface(robotoFont);
        txtSkipDS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextScreen();
            }
        });

        txtSyncHeading1=(TextView)findViewById(R.id.txtSyncHeading1);
        txtSyncHeading1.setTypeface(robotoFont);

        txtSyncHeading2=(TextView)findViewById(R.id.txtSyncHeading2);
        txtSyncHeading2.setTypeface(robotoFont);

        txtSyncHeading3=(TextView)findViewById(R.id.txtSyncHeading3);
        txtSyncHeading3.setTypeface(robotoFont);

        txtSyncHeadingTop = (TextView)findViewById(R.id.txtSyncHeadingTop);
        txtSyncHeadingTop.setTypeface(robotoFont);

        llSkipNext=(LinearLayout)findViewById(R.id.llSkipNext);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

         if(resultCode == RESULT_OK)
        {
              if(requestCode == Request_Id)
            {
                prgSpinner.setVisibility(View.VISIBLE);
                bProgressTrue = true;

                SelectNearDevice selectNearDevice = new SelectNearDevice();
                selectNearDevice.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar);
                selectNearDevice.show(getSupportFragmentManager().beginTransaction(), "dialog");

                Intent intent1 = new Intent("StartScan");
                intent1.putExtra("data", "scan");
                sendBroadcast(intent1);

            }
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();

            if(bundle != null && bundle.containsKey("data"))
            {
                String data = bundle.getString("data");

                  if(data.equals("connected"))
                {
                    prgSpinner.setVisibility(View.GONE);
                    bProgressTrue = false;

                    showDialog();
                }
            }

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(broadcastReceiver, new IntentFilter("connect"));
    }

    @Override
    protected void onPause() {
        super.onPause();

        try{

            unregisterReceiver(broadcastReceiver);

        }catch (IllegalArgumentException e){

        }
    }

    void showDialog() {


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        final DialogFragment newFragment = StaticMaster.MyDialogFragment.newInstance(4);
        newFragment.show(ft, "dialog");

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        nextScreen();

                    }
                });
            }
        }, 3000);

    }

       public void nextScreen()
    {

        int size = StaticMaster.database(getApplicationContext()).getAllGuardians().size();

          if(size < 1)
        {

            SharedPrefrenceStorage.StorePosition(myContext, "ContactListActivity");

            Intent intent = new Intent(myContext, ContactListActivity.class);
            intent.putExtra("data","AddGuardians");
            startActivity(intent);

        }
           else {

              SharedPrefrenceStorage.StorePosition(myContext, "HomeActivity");
              Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
              startActivity(intent);

          }

    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

}
