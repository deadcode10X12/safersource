package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class DummyNotificationHandler extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy_notification_handler);

        Bundle bundle = getIntent().getExtras();
        String data = bundle.getString("id");

          switch (data)
        {
            case "101":{


                String wardId = bundle.getString("ward_id");

                StaticMaster.sendCommonData(getApplicationContext(), "Accept_Guardian_Request", "http://54.169.182.72/api/v0/guardian/" + wardId + "/accept", Request.Method.PUT);

            }break;

            case "102":{

                StaticMaster.sendCommonData(getApplicationContext(), "Get_All_Guardians", "http://54.169.182.72/api/v0/guardians", Request.Method.GET);

            }break;

            case "104":{



            }break;


        }

    }

}
