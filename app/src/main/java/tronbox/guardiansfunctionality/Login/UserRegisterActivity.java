package tronbox.guardiansfunctionality.Login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import tronbox.guardiansfunctionality.Home.CircleImagePhoto;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 3/9/2015.
 * Display the user registration details
 */

  public class UserRegisterActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
    Context myContext=UserRegisterActivity.this;
    CommonFunctions comFunc=new CommonFunctions();
    String[] strings = {"Gender","Female","Male","Other"};
    ImageView imgNextUR;
    Spinner mySpinner;

    LinearLayout vwRounded;
    TextView txtlstGender;
    CircleImagePhoto myImageViewS;


    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;


    private OnFragmentInteractionListener mListener;
    private EditText fullName, email;
    private Button signUp;
    private Spinner gender;
    private ImageView profilePic;
    private int REQUEST_IMAGE_CAPTURE = 101;

    private ArrayList<String> genders = new ArrayList<String>();

    private static final String TAG = "MainFragment";
    private UiLifecycleHelper uiHelper;

    private boolean picOk = false;

    private Bitmap finalBitmap;

    private SignInButton googleSignIn;

    private boolean socialLogin = false;

    private String socialProfilePic = null;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        UIDeclaration();

        email = (EditText)findViewById(R.id.etxtREmail);
        fullName = (EditText)findViewById(R.id.etxtRFullName);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN) // add permissions here
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .build();

        googleSignIn = (SignInButton)findViewById(R.id.sign_in_button);
        googleSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!mGoogleApiClient.isConnecting()) {
                    mSignInClicked = true;
                    mGoogleApiClient.connect();
                    Log.d(StaticMaster.log, "Connecting to Google+");

                }

            }
        });


        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        LoginButton authButton = (LoginButton)findViewById(R.id.authButton);
        authButton.setReadPermissions(Arrays.asList("email"));



        vwRounded.setOnClickListener(new View.OnClickListener()
         {
         @Override
         public void onClick(View v)
         {
             SelectNewProfilePhoto();
         }
         });

        imgNextUR.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                boolean ok = true;

                StringBuffer errorMessage = new StringBuffer();
                errorMessage.append("Please fill the column ");

                 if(fullName.getText().length() < 1)
                {
                    errorMessage.append("Full Name");
                    ok = false;
                }

                if(email.getText().length() < 1)
                {
                       ok = false;

                      if(errorMessage.toString().contains("Full Name"))
                    {
                      errorMessage.append(", ");
                    }

                      errorMessage.append("Email");

                }

                  if(txtlstGender.getText().length() < 1)
                {
                    ok = false;

                    if(errorMessage.toString().contains("Full Name") || errorMessage.toString().contains("Email"))
                    {
                        errorMessage.append(", ");
                    }

                        errorMessage.append("Gender");

                }

                  if(picOk == false)
                {

                    if(errorMessage.toString().contains("Full Name") || errorMessage.toString().contains("Email") || errorMessage.toString().contains("Gender"))
                    {
                        errorMessage.append(" and ");
                    }

                    errorMessage.append("Profile Pic");
                }

                  if((ok == true && picOk == true) || socialLogin == true)
                {
                    progressDialog = ProgressDialog.show(UserRegisterActivity.this, "Creating Profile", fullName.getText().toString()+" please wait....",true);
                    uploadAndSavePic(); // upload the pic to the server


                    // Storing user details locally......

                    // First Name should be in camel case
                    String editName = fullName.getText().toString();

                     if(!editName.contains(" "))
                    {
                      editName = editName.substring(0,1).toUpperCase()+editName.substring(1,editName.length());
                    }
                      else {

                         String[] arr = editName.split(" ");

                         String temp1 = arr[0].substring(0, 1).toUpperCase()+arr[0].substring(1, arr[0].length());
                         String temp2 = arr[1].substring(0, 1).toUpperCase()+arr[1].substring(1, arr[1].length());

                         editName = temp1+" "+temp2;

                     }

                    SharedPrefrenceStorage.StoreName(getApplicationContext(), editName);
                    SharedPrefrenceStorage.StoreEmail(getApplicationContext(), email.getText().toString());
                    SharedPrefrenceStorage.StoreGender(getApplicationContext(), txtlstGender.getText().toString());

                    Log.d(StaticMaster.log, "Sending CMD to upload the profile info, check next log to confirm");
                    StaticMaster.sendCommonData(getApplicationContext(), "Add_Profile", Networking.CommonUrl+"user", com.android.volley.Request.Method.PUT);

                    progressDialog.dismiss();
                }
                   else
                 {
                     Toast.makeText(getApplicationContext(), errorMessage.toString(), Toast.LENGTH_SHORT).show();

                      if(ok == false)
                     {
                         ok = true;
                     }

                 }


            }
        });
        //txtlstGender.setKeyListener(null);
        txtlstGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ClickOnView", "Click on Text View");
                final Dialog dialog=new Dialog(myContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.gender_list);
                final TextView txtlstFemale=(TextView)dialog.findViewById(R.id.txtlstFemale);
                dialog.show();
                txtlstFemale.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtlstGender.setText("F");
                        dialog.dismiss();
                    }
                });
                final TextView txtlstMale=(TextView)dialog.findViewById(R.id.txtlstMale);
                txtlstMale.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtlstGender.setText("M");
                        dialog.dismiss();
                    }
                });
                final TextView txtlstOther=(TextView)dialog.findViewById(R.id.txtlstOther);
                txtlstOther.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        txtlstGender.setText("Other");
                        dialog.dismiss();
                    }
                });

            }
        });

        StaticMaster.registerBroadcastReceiver(this, "UserRegisterActivity");
    }



    //method for declare all UI controls
    protected void UIDeclaration()
    {
        txtlstGender=(TextView)findViewById(R.id.txtlstGender);
        vwRounded=(LinearLayout)findViewById(R.id.vwRounded);
        imgNextUR=(ImageView)findViewById(R.id.imgNextUR);
        //llPlusRounded=(LinearLayout)findViewById(R.id.llPlusRounded);
        myImageViewS=(CircleImagePhoto)findViewById(R.id.imgRegisterPhoto);
    }
    //method for select or take photo
    private void SelectNewProfilePhoto()
    {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(myContext);
        builder.setTitle("Add Your Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialog, int item)
            {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.png");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                    //llPlusRounded.setVisibility(View.INVISIBLE);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                    //llPlusRounded.setVisibility(View.INVISIBLE);
                }
                else if (options[item].equals("Cancel"))
                {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                this.startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(),
                        RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    public void onConnectionFailed(ConnectionResult result) {

        if (!mIntentInProgress) {
            // Store the ConnectionResult so that we can use it later when the user clicks
            // 'sign-in'.
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }

    }


    @Override
    public void onConnected(Bundle bundle) {

        mSignInClicked = false;
        Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();

        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

            Log.d(StaticMaster.LoginScreenLogs, "Connected to Google+ Fetching User Profile Info");

            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

            String personName = currentPerson.getDisplayName();
            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

            String gender = null;

            if(currentPerson.getGender() == 0){
                gender = "M";

            }else if(currentPerson.getGender() == 1){
                gender = "F";

            }else if(currentPerson.getGender() == 2){

                gender = "Other";
            }

            try {
                SharedPrefrenceStorage.StoreProfilePic(getApplicationContext(),new JSONObject(currentPerson.getImage().toString().replace("\\","").replace("sz=50", "sz=100")).getString("url"));
                downloadImage();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            socialLogin = true;

            updateProfileData(personName, email, gender);

        }
    }


    public void downloadImage(){

        Log.d(StaticMaster.LoginScreenLogs, "Sending CMD to downlaod the profile pic from an url ");

//        llPlusRounded.setVisibility(View.GONE);

        ImageRequest request = new ImageRequest(SharedPrefrenceStorage.GetProfilePic(getApplicationContext()),
                new com.android.volley.Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {

                        finalBitmap = bitmap;
                        myImageViewS.setImageBitmap(comFunc.getRoundedShape(bitmap, 400));

                    }
                }, 0, 0, null,
                new com.android.volley.Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                        Log.d(StaticMaster.LoginScreenLogs, "Image Download Error :- "+error.getMessage());

                    }
                });

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);


/*
        HandlerThread thread = new HandlerThread("Images");
        thread.start();
        new DownloadImageHandler(thread.getLooper(), UserRegisterActivity.this, url);
*/

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {

                if (requestCode == 1) {
                    File f = new File(Environment.getExternalStorageDirectory().toString());
                    for (File temp : f.listFiles()) {
                        if (temp.getName().equals("temp.png")) {
                            f = temp;
                            break;
                        }
                    }
                    try {
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        finalBitmap = Networking.getCompressBit(BitmapFactory.decodeFile(f.getAbsolutePath(), bitmapOptions));
                        updateProfileImage(finalBitmap);
                        picOk = true;
                        Log.w(StaticMaster.LoginScreenLogs, "Profile Pic Source - Camera");

                        String path = Environment
                                .getExternalStorageDirectory()
                                + File.separator
                                + "Phoenix" + File.separator + "default";
                        f.delete();
                        OutputStream outFile = null;
                        File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".png");
                        try {
                            outFile = new FileOutputStream(file);
                            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, outFile);
                            outFile.flush();
                            outFile.close();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (requestCode == 2) {
                    if (data!=null) {

                        Uri selectedImage = data.getData();
                        String[] filePath = {MediaStore.Images.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        String picturePath = c.getString(columnIndex);
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        finalBitmap = Networking.getCompressBit(BitmapFactory.decodeFile(picturePath,bitmapOptions));
                        updateProfileImage(finalBitmap);
                        c.close();

                        picOk = true;
                        Log.w(StaticMaster.LoginScreenLogs, "Profile Pic Source - Memory");

                    } else if (requestCode == RC_SIGN_IN) {

                        if (resultCode != RESULT_OK) {
                            mSignInClicked = false;
                        }

                        mIntentInProgress = false;

                        if (!mGoogleApiClient.isConnecting()) {
                            mGoogleApiClient.connect();
                        }
                    }
                }else if (requestCode == RC_SIGN_IN) {

                    if (resultCode != RESULT_OK) {
                        mSignInClicked = false;
                    }

                    mIntentInProgress = false;

                    if (!mGoogleApiClient.isConnecting()) {
                        mGoogleApiClient.connect();
                    }
                }


                uiHelper.onActivityResult(requestCode, resultCode, data);
            }
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }


    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {

            downloadPic(session);

            Log.i(TAG, "Logged in...");
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };


    @Override
    public void onResume() {
        super.onResume();
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed()) ) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    public void downloadPic(Session session)
    {

        Log.d(StaticMaster.LoginScreenLogs, "Connected to facebook fetching User Profile_Info");

        Bundle params = new Bundle();
        params.putBoolean("redirect", false);
        params.putString("height", "200");
        params.putString("type", "normal");
        params.putString("width", "200");

        Request profileRequest = new Request(
                session,
                "/me",
                null,
                HttpMethod.GET,
                new Request.Callback() {
                    public void onCompleted(Response response) {

                          if(response.getRawResponse() != null)
                        {
                            try {

                                String name = null, email = null, gender = null;

                                JSONObject object = new JSONObject(response.getRawResponse());

                                 if(object.has("email"))
                                {
                                    email = object.getString("email");
                                }

                                 if(object.has("first_name"))
                                {

                                     name = object.getString("first_name");

                                    if(object.has("last_name"))
                                    {
                                        name = name.concat(" "+object.getString("last_name"));

                                    }
                                }


                                  if(object.has("gender"))
                                {
                                    gender = object.getString("gender");

                                     if(gender.equalsIgnoreCase("male"))
                                    {
                                        gender = "M";

                                    }else if(gender.equalsIgnoreCase("female"))
                                     {
                                         gender = "F";
                                     }
                                }

                                updateProfileData(name,email,gender);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
        );

        Request pictureRequest = new Request(

                session,
                "/me/picture",
                params,
                HttpMethod.GET,
                new Request.Callback() {
                    public void onCompleted(Response response) {

                        if(response.getRawResponse() != null){

                            try {

                                SharedPrefrenceStorage.StoreProfilePic(getApplicationContext(), new JSONObject(response.getRawResponse()).getJSONObject("data").getString("url"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }
                }
        );


        RequestBatch batch = new RequestBatch();
        batch.add(profileRequest);
        batch.add(pictureRequest);
        batch.addCallback(new RequestBatch.Callback() {
            @Override
            public void onBatchCompleted(RequestBatch batch) {

                socialLogin = true;
                downloadImage();

                Log.d(StaticMaster.LoginScreenLogs, "Facebook batch request is complete, data is retrieved");
            }
        });
        batch.executeAsync();
    }

      public void uploadAndSavePic()
    {
          if(finalBitmap != null)
        {

            try {

                Log.d(StaticMaster.LoginScreenLogs, "User is using the local signup, saving his profile_pic interanlly");

                FileOutputStream out1  = openFileOutput(StaticMaster.UserProfilePic, Context.MODE_PRIVATE);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                byte[] buffer = bos.toByteArray();
                out1.write(buffer, 0, buffer.length);
                out1.close();

            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            } catch (Exception e){
            }



             if(socialLogin == false)
            {
                Log.d(StaticMaster.LoginScreenLogs, "Sending cmd to upload his profile_pic on server");
                new SendProfileImage(getApplicationContext()).execute(finalBitmap);
            }

        }

    }

      public void updateProfileData(String name, String email, String gender)
    {
        fullName.setText(name);
        this.email.setText(email);
        txtlstGender.setText(gender);

    }

      public void updateProfileImage(Bitmap bitmap)
    {
        myImageViewS.setImageBitmap(comFunc.getRoundedShape(bitmap, 400));
        picOk = true;


    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

}

/*
try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "tronbox.guardiansfunctionality",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHashhhhhhhhh:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
 */

