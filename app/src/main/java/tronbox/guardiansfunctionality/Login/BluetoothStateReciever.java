package tronbox.guardiansfunctionality.Login;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

public class BluetoothStateReciever extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")){

            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter != null) {
                if (!mBluetoothAdapter.isEnabled()) {

                    Log.w("BluetoothState", "State = OFF");

                }else {

                    Log.w("BluetoothState", "State = ON");

                }
            }

        }else if(intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")){

          ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo mobileNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo wifiNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);


             if(mobileNetworkInfo.isAvailable())
            {

                Log.w("Mobile_Connection", "Mobile Network Is Available");

                if(mobileNetworkInfo.isConnected()){

                    Log.w("Mobile_Connection", "Internet is present via mobile");

                }else {

                    Log.w("Mobile_Connection", "Internet is not present via mobile");

                }

            }else {

                 Log.w("Mobile_Connection", "Mobile Network Is Not Available");

            }


            if(wifiNetworkInfo.isConnected()){

                Log.w("Mobile_Connection", "Internet is present via wifi");

            }else {

                Log.w("Mobile_Connection", "Internet is not present via wifi");

            }


        }else if(intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED")){

            WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);

              if(!wifiManager.isWifiEnabled()) // wifi is off
            {

                Log.w("WifiState", "State = ON");

            }


        }else if(intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)){

            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            float batteryPct = level / (float)scale;

            Toast.makeText(context, "Battery PCT "+batteryPct, Toast.LENGTH_SHORT).show();
        }

    }
}
