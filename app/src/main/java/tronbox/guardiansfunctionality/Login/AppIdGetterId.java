package tronbox.guardiansfunctionality.Login;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.Request;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class AppIdGetterId extends AsyncTask<Void, Void, String> {

	private Context context;

	public AppIdGetterId(Context context) {
		this.context = context; 
	}
	@Override
	protected String doInBackground(Void... arg0) {
		Log.i("DEMO", "AppIdGetter started...");
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
		
		String id = null;

		try {

            id = gcm.register("989005861527");

		} catch (IOException e) {
			cancel(true);
		}
		catch (Exception e) {
			cancel(true);
		}

		return id;
	}
	@Override
	protected void onPostExecute(String result) {

        SharedPrefrenceStorage.StoreRegId(context,result);
        StaticMaster.sendCommonData(context, "GcmId", "http://54.169.182.72/api/v0/subscribe", Request.Method.POST);

        Log.w("GEMO", "AppIdGetter Id = " + result);

	}
	@Override
	protected void onCancelled(String result) {
		//do something when error occured
		Log.e("DEMO", "AppIdGetter Cancelled");
	}
}
