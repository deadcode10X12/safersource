package tronbox.guardiansfunctionality.Login;

public interface OnFragmentInteractionListener {

    public void onFragmentInteraction(String id, String data);
}