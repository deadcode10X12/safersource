package tronbox.guardiansfunctionality.Login;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;

import tronbox.guardiansfunctionality.R;

public class CommonFunctions
{
    private Context myContext;

    public static Bitmap roundMyPhoto(Bitmap bPhoto)
    {
        Bitmap bCirclePhoto=Bitmap.createBitmap(bPhoto.getWidth(),bPhoto.getHeight(),Bitmap.Config.ARGB_8888);
        BitmapShader shader=new BitmapShader(bPhoto,TileMode.CLAMP,TileMode.CLAMP);
        Paint paint=new Paint();
        paint.setShader(shader);
        paint.setAntiAlias(true);
        Canvas c=new Canvas(bCirclePhoto);
        c.drawCircle(bPhoto.getWidth()/2,bPhoto.getHeight()/2,bPhoto.getWidth()/2, paint);
        return bCirclePhoto;
    }


    public Bitmap decodeFile(Context context,String file)
    {
        try
        {
            myContext=context;
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file,o);
            //BitmapFactory.decodeResource(myContext.getResources(), resId, o);
            final int REQUIRED_SIZE = 400;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true)
            {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale++;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(file, o2);

        } catch (Exception e) {
        }
        return null;
    }
    public Bitmap getRoundedShape(Bitmap scaleBitmapImage,int width)
    {
        try {
            if (scaleBitmapImage == null) {
                scaleBitmapImage = BitmapFactory.decodeResource(myContext.getResources(), R.drawable.face);
            }
            int targetWidth = width;
            int targetHeight = width;
            Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                    targetHeight, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(targetBitmap);
            Path path = new Path();
            path.addCircle(((float) targetWidth - 1) / 2,
                    ((float) targetHeight - 1) / 2,
                    (Math.min(((float) targetWidth),
                            ((float) targetHeight)) / 2),
                    Path.Direction.CCW);
            canvas.clipPath(path);
            Bitmap sourceBitmap = scaleBitmapImage;
            canvas.drawBitmap(sourceBitmap,
                    new Rect(0, 0, sourceBitmap.getWidth(),
                            sourceBitmap.getHeight()),
                    new Rect(0, 0, targetWidth,
                            targetHeight), null);
            return targetBitmap;
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }
    public Bitmap imageCirclexClip(Context context,Bitmap sourceBitmap){
        if(sourceBitmap == null){
            sourceBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.dummy_image);
        }
        int targetWidth = 100;
        int targetheight = 100;

        Bitmap outputBitmap = Bitmap.createBitmap(targetWidth, targetheight, Bitmap.Config.ARGB_8888);
        Path path = new Path();
        path.addCircle(targetWidth/2, targetheight/2, targetWidth/2, Path.Direction.CCW);
        Canvas canvas = new Canvas(outputBitmap);
        canvas.clipPath(path);

        Rect src = new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight());
        Rect out = new Rect(0, 0, targetWidth, targetheight);

        Bitmap source = sourceBitmap;
        canvas.drawBitmap(source, src, out, null);

        return outputBitmap;
    }
    public static Bitmap imageCirclexClipBackground(Context context,Bitmap sourceBitmap, String color){
        if(sourceBitmap == null){
            sourceBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.dummy_image);
        }
        int targetWidth = 100;
        int targetheight = 100;

        Bitmap outputBitmap = Bitmap.createBitmap(targetWidth, targetheight, Bitmap.Config.ARGB_8888);
        Path path = new Path();
        path.addCircle(targetWidth/2, targetheight/2, targetWidth/2, Path.Direction.CCW);
        Canvas canvas = new Canvas(outputBitmap);
        canvas.clipPath(path);

        Rect src = new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight());
        Rect out = new Rect(0, 0, targetWidth, targetheight);

        Bitmap source = sourceBitmap;
        canvas.drawColor(Color.parseColor(color));
        canvas.drawBitmap(source, src, out, null);

        return outputBitmap;
    }
}
