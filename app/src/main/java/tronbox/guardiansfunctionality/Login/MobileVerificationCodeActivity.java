package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.telephony.SmsMessage;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 3/9/2015.
 * Display Mobile Send Verification Code
 */
public class MobileVerificationCodeActivity extends Activity
{
    Context myContext = MobileVerificationCodeActivity.this;
    Button btnVCVerify;
    TextView txtVCResend, timerText, txtVCEditMobile;

    int minute = 2, second = 0;
    String minuteText = "", secondText = "";

    Timer timer;
    EditText verficationEditText;

    String TAG = "GuardianProgress";

    ProgressDialog progressDialog;


    CustomTimer customTimer = new CustomTimer(122000,1000);

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);


        txtVCEditMobile = (TextView)findViewById(R.id.txtVCEditMobile);
        txtVCEditMobile.setText(Html.fromHtml("<b><u>Edit Mobile</u></b>"));
        txtVCEditMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(myContext, MobileVerificationActivity.class);
                startActivity(intent);
            }
        });

        verficationEditText = (EditText)findViewById(R.id.etxtVCode);


        timerText = (TextView)findViewById(R.id.timer_text);

        btnVCVerify = (Button) findViewById(R.id.btnVCVerify);
        txtVCResend=(TextView)findViewById(R.id.txtVCResend);
        txtVCResend.setText(Html.fromHtml("<b><u>Resend Verification Code</u></b>"));
        txtVCResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  if(minute < 1)
                {

                    customTimer.cancel();

                    customTimer.reset();

                    customTimer.start();

                    SharedPrefrenceStorage.StoreVerificationCode(myContext, null);
                    StaticMaster.sendCommonData(getApplicationContext(), "NewUser", Networking.CommonUrl+"user", Request.Method.POST);

                }
                  else {
                     Toast.makeText(getApplicationContext(), "Still Processing...", Toast.LENGTH_SHORT).show();
                 }

            }
        });

        btnVCVerify.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                if (!verficationEditText.getText().toString().isEmpty()) {

                      if(SharedPrefrenceStorage.GetVerificationCode(myContext) != null)
                    {

                    }
                       else {

                          SharedPrefrenceStorage.StoreVerificationCode(myContext, verficationEditText.getText().toString());
                          StaticMaster.sendCommonData(getApplicationContext(), "UserVerify", Networking.CommonUrl+"user/verify", Request.Method.POST);

                      }

                      if(SharedPrefrenceStorage.GetName(myContext) != null)
                    {
                        SharedPrefrenceStorage.StorePosition(myContext, "DeviceSyncActivity");

                        Intent i = new Intent(myContext, DeviceSyncActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }else {

                        SharedPrefrenceStorage.StorePosition(myContext, "UserRegisterActivity");

                        Intent i = new Intent(myContext, UserRegisterActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();


                    }

/*
                    SharedPrefrenceStorage.StoreVerificationCode(getApplicationContext(), verficationEditText.getText().toString());
                    StaticMaster.sendCommonData(getApplicationContext(), "UserVerify", "http://54.169.182.72/api/v0/user/verify", Request.Method.POST);
*/


/*
                    if(SharedPrefrenceStorage.GetName(getApplicationContext()) != null)
                    {

                        SharedPrefrenceStorage.StorePosition(getApplicationContext(), "DeviceSyncActivity");

                        Intent i = new Intent(getApplicationContext(), DeviceSyncActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);

                    }else {

                        SharedPrefrenceStorage.StorePosition(getApplicationContext(), "UserRegisterActivity");

                        Intent i = new Intent(getApplicationContext(), UserRegisterActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);


                    }
*/

                }
                else
                {
                    Toast.makeText(myContext,"Verification code should not blank!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        timer = new Timer();
        //timer.schedule(timerTask, 1000, 1000);


        customTimer.start();

        //progressDialog = ProgressDialog.show(myContext, "Safer Verification Code", "Checking Verification Code, Please wait....", true);

    }


  TimerTask timerTask =  new TimerTask() {

    @Override
    public void run() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                second = second - 1;

                if(second == -1)
                {
                    second = 59;
                    minute = minute - 1;
                }


                if(second/10 == 0)
                {
                    secondText = "0"+String.valueOf(second);
                }else {
                    secondText = String.valueOf(second);
                }

                if(minute/10 == 0)
                {
                    minuteText = "0"+String.valueOf(minute);
                }else {
                    minuteText = String.valueOf(minute);
                }

                if(minute == -1)
                {
                    cancel();

                }else {

                    timerText.setText(minuteText+":"+secondText);

                }

            }
        });

    }

};

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(broadcastReceiver, new IntentFilter("MobileVerification"));
        registerReceiver(smsReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(broadcastReceiver);
        unregisterReceiver(smsReceiver);
    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


             if(SharedPrefrenceStorage.GetName(context) != null)
            {

                SharedPrefrenceStorage.StorePosition(context, "DeviceSyncActivity");

                Intent i = new Intent(context, DeviceSyncActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                finish();
            }else {

                 SharedPrefrenceStorage.StorePosition(context, "UserRegisterActivity");

                 Intent i = new Intent(context, UserRegisterActivity.class);
                 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                 context.startActivity(i);
                 finish();

             }

        }
    };

     public void startTimer()
    {
        minute = 2;
        second = 0;

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        second = second - 1;

                        if(second == -1)
                        {
                            second = 59;
                            minute = minute - 1;
                        }


                        if(second/10 == 0)
                        {
                            secondText = "0"+String.valueOf(second);
                        }else {
                            secondText = String.valueOf(second);
                        }

                        if(minute/10 == 0)
                        {
                            minuteText = "0"+String.valueOf(minute);
                        }else {
                            minuteText = String.valueOf(minute);
                        }

                        if(minute == -1)
                        {
                            cancel();

                        }else {

                            timerText.setText(minuteText+":"+secondText);

                        }

                    }
                });

            }

        },1000,1000);

    }

    BroadcastReceiver smsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

               if(SharedPrefrenceStorage.GetVerificationCode(getApplicationContext()) == null)
            {
                Bundle bundle = intent.getExtras();
                try {

                    if (bundle != null) {

                        final Object[] pdusObj = (Object[]) bundle.get("pdus");

                        for (int i = 0; i < pdusObj.length; i++) {

                            SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                            String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                            String senderNum = phoneNumber;
                            String message = currentMessage.getDisplayMessageBody();
                            String verificationCode = message.substring(message.length() - 5, message.length());

                            verficationEditText.setText(verificationCode);

                            SharedPrefrenceStorage.StoreVerificationCode(context, verificationCode);
                            StaticMaster.sendCommonData(getApplicationContext(), "UserVerify", Networking.CommonUrl+"user/verify", Request.Method.POST);


                              if(progressDialog != null)
                            {
                                //  progressDialog.dismiss();
                            }

                            Log.w("SmsReceiver", "senderNum: " + senderNum + "; message:" + verificationCode);

                        } // end for loop
                    } // bundle is null

                } catch (Exception e) {
                    Log.e("SmsReceiver", "Exception smsReceiver" + e);

                }
            }

        }
    };

    class CustomTimer extends CountDownTimer {

        public CustomTimer(long millisInFuture, long countDownInterval)
        {
            super(millisInFuture, countDownInterval);

        }

          public void reset()
        {
            minute = 2;
            second = 0;
        }

        @Override
        public void onTick(long millisUntilFinished) {

            second = second - 1;

            if(second == -1)
            {
                second = 59;
                minute = minute - 1;
            }


            if(second/10 == 0)
            {
                secondText = "0"+String.valueOf(second);
            }else {
                secondText = String.valueOf(second);
            }

            if(minute/10 == 0)
            {
                minuteText = "0"+String.valueOf(minute);
            }else {
                minuteText = String.valueOf(minute);
            }

            if(minute == -1)
            {
                cancel();

            }else {

                timerText.setText(minuteText+":"+secondText);

            }
        }

        @Override
        public void onFinish() {

        }
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}