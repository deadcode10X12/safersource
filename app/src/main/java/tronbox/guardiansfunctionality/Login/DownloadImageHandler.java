package tronbox.guardiansfunctionality.Login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import tronbox.guardiansfunctionality.StaticMaster;

public class DownloadImageHandler extends Handler {

    private Context context;
    private String link = null, name = null;
    private UserRegisterActivity userRegisterActivity;

    public DownloadImageHandler(Looper looper, Context context, String link){
        super(looper);

        this.context = context;
        this.link = link;
    }

    public DownloadImageHandler(Looper looper, UserRegisterActivity userRegisterActivity, String link){
        super(looper);

        this.userRegisterActivity = userRegisterActivity;
        this.link = link;
    }

    @Override
    public void handleMessage(Message msg) {

             try {

                 java.net.URL url = new java.net.URL(link);
                 HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                 connection.setDoInput(true);

                 connection.connect();
                 InputStream input = connection.getInputStream();
                 Bitmap myBitmap = BitmapFactory.decodeStream(input);
                 SaveImage(myBitmap, StaticMaster.UserProfilePic);

                 userRegisterActivity.updateProfileImage(myBitmap);

                 Log.d(StaticMaster.log, "Profile pic is donwloaded and saved");

             } catch (IOException e) {
                 e.printStackTrace();
             }


    }

    public void SaveImage(Bitmap finalBitmap, String name) {
        try {
            FileOutputStream out1  = context.openFileOutput(name, Context.MODE_PRIVATE);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            byte[] buffer = bos.toByteArray();
            out1.write(buffer, 0, buffer.length);
            out1.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } catch (Exception e){
        }
    }

}
