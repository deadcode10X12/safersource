package tronbox.guardiansfunctionality.Login;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by nikhil on 2/7/15.
 */
public class SendProfileImage extends AsyncTask<Bitmap, Void, Void>{


    File beforeFile, afterFile;
    Context context;
    String attachmentName = "bitmap";
    String attachmentFileName = "bitmap.bmp";
    String crlf = "\r\n";
    String twoHyphens = "--";
    String boundary =  "*****";


    public SendProfileImage(Context context){
        this.context = context;
    }


    @Override
    protected Void doInBackground(Bitmap... params) {


        try {

        HttpURLConnection httpUrlConnection = null;
        URL url = new URL("http://54.169.182.72/api/v0/user/pic");
        httpUrlConnection = (HttpURLConnection) url.openConnection();
        httpUrlConnection.setUseCaches(false);
        httpUrlConnection.setDoOutput(true);

        httpUrlConnection.setRequestMethod("POST");
        httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
        httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
        httpUrlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + this.boundary);
        httpUrlConnection.addRequestProperty("authorization","bearer "+ SharedPrefrenceStorage.GetAuthToken(context));

            DataOutputStream request = new DataOutputStream(httpUrlConnection.getOutputStream());

           request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
           request.writeBytes("Content-Disposition: form-data; name=\"" + this.attachmentName + "\";filename=\"" + this.attachmentFileName + "\"" + this.crlf);
           request.writeBytes(this.crlf);


            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            params[0].compress(Bitmap.CompressFormat.PNG, 75, bos);
            byte[] data = bos.toByteArray();

            request.write(data);

            request.writeBytes(this.crlf);
            request.writeBytes(this.twoHyphens + this.boundary + this.twoHyphens + this.crlf);

            request.flush();
            request.close();

            InputStream responseStream = new BufferedInputStream(httpUrlConnection.getInputStream());

            BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
            String line = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = responseStreamReader.readLine()) != null)
            {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();

            String response = stringBuilder.toString();

            Log.d(StaticMaster.log, "Image is uploaded response = "+response);


        } catch (IOException ex) {

            Log.w("ServerResponseFromServerException", ex.toString());

        }


        return null;
    }


    public void storeImages()
    {

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);


        File myDir =  getAlbumStorageDir("cloplay");
        myDir.mkdirs();

        String beforeImage = "before.jpg";

        beforeFile = new File (myDir, beforeImage);


        if (beforeFile.exists ()) beforeFile.delete ();

        try {
            FileOutputStream out = new FileOutputStream(beforeFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

            /*Master.beforeImageUri = Uri.fromFile(beforeFile);
            Master.afterImageUri = Uri.fromFile(afterFile);*/


        Log.w("StoredAfterFilePath", beforeFile.getPath());

        //}catch (IOException io){}
    }
    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("Errors", "Directory not created");
        }
        return file;
    }

}
