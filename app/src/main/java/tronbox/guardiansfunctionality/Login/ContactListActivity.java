package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import tronbox.guardiansfunctionality.AddGuardians.ActivityCommunicator;
import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.AddGuardians.ContactsAdapter;
import tronbox.guardiansfunctionality.AddGuardians.ContactsList;
import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.Maps.SafeWalk;
import tronbox.guardiansfunctionality.Maps.SafeWalkSetup;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 3/19/2015.
 */
public class ContactListActivity extends FragmentActivity implements ActivityCommunicator
{
    Context myContext=ContactListActivity.this;
    public static ArrayList<String> selectedNames = new ArrayList<>();
    private ProgressDialog progressDialog;

    public static String purpose = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

          if(getIntent().getExtras() != null)
        {
             if(getIntent().getExtras().containsKey("data"))
            {
                purpose = getIntent().getExtras().getString("data");
            }

        }

        Fragment fg=new ContactsListFragment();
        Bundle bd=new Bundle();
        bd.putInt("Screen",1);
        fg.setArguments(bd);
        getSupportFragmentManager().beginTransaction().replace(R.id.list_container, fg).commit();
        ImageView imgSelectCL=(ImageView)findViewById(R.id.imgSelectCL);


        imgSelectCL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(StaticMaster.detailsArrayList != null){

                     if(StaticMaster.detailsArrayList.size() > 0)
                    {
                        if(purpose.equals("AddGuardians"))
                        {

                            SharedPrefrenceStorage.StorePosition(getApplicationContext(), "HomeActivity");

                            progressDialog = ProgressDialog.show(ContactListActivity.this, "Adding Guardians", "Adding Guardians, Please wait....", true);

                            Networking.AddMyGuardian(getApplicationContext());
                        }
                        else if(purpose.equals("SafeWalk"))
                        {
                            finish();
                            Intent intent = new Intent(getApplicationContext(), SafeWalkSetup.class);
                            startActivity(intent);
                        }

                    }

                }

            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter("GuardiansProcessing"));

    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();

    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

              if(progressDialog != null)
            {
                progressDialog.dismiss();
            }

            Intent intent1 = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent1);

        }
    };

    public static class ContactsListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>
    {

        public Context context;
        private ActivityCommunicator activityCommunicator;
        ContactsAdapter adapters;
        ContactsList contactsListAdapter;
        ArrayList<ContactDetails> detailsArrayList;

        ArrayList<ContactDetails> tempArrayList = new ArrayList<ContactDetails>();
        ArrayList<ContactDetails> storeArrayList = new ArrayList<ContactDetails>();




        Bundle data;
        int screenValue;
        HashMap<String, String> sparseArray = new HashMap<String, String>();
        ArrayList<ContactDetails> selectedList;
        MobileDatabase database;
        public ContactsListFragment()
        {

        }
        @Override
        public void onAttach(Activity activity){
            super.onAttach(activity);
            context = getActivity();
            activityCommunicator =(ActivityCommunicator)context;
        }
        @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
        }
        @Override
        public void onSaveInstanceState(Bundle outState){
            super.onSaveInstanceState(outState);
        }
        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.inflate(R.layout.fragment_contact_list, container, false);
            database = new MobileDatabase(getActivity(), "GUARDIAN", null, 1);
            data = getArguments();
            screenValue = data.getInt("Screen");
            detailsArrayList = new ArrayList<ContactDetails>();
            contactsListAdapter = new ContactsList(getActivity(), R.layout.custom_gurdian_list, detailsArrayList);
            adapters = new ContactsAdapter(getActivity());

            final ListView friendsListView = (ListView) view.findViewById(R.id.lstContact);
            friendsListView.setAdapter(contactsListAdapter);

            View header = getActivity().getLayoutInflater().inflate(R.layout.search_header, null);

            ((EditText)header.findViewById(R.id.search_header)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(final CharSequence s, int start, int before, int count) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                                String text = s.toString().toLowerCase();

                                detailsArrayList.clear();
                                contactsListAdapter.removeRowSelection();
                                contactsListAdapter.notifyDataSetChanged();



                                  for(int i=0; i<storeArrayList.size(); i++)
                                {
                                    ContactDetails str = storeArrayList.get(i);

                                    String text2 = "^("+text+")+([\\x20-\\x7E]+)";

                                    Pattern p = Pattern.compile(text2);

                                    if(p.matcher(str.getName().toLowerCase()).matches())
                                    {

                                        //Toast.makeText(getActivity(), str.getName(), Toast.LENGTH_SHORT).show();
                                        Log.w("TestingMatch", str.getName());


                                        detailsArrayList.add(str);
                                        contactsListAdapter.notifyDataSetChanged();

                                         if(ContactListActivity.selectedNames.contains(str.getMobile()))
                                        {
                                            contactsListAdapter.selectRowView(i, true);
                                            contactsListAdapter.notifyDataSetChanged();
                                        }

                                    }
                                }

                            StaticMaster.detailsArrayList = detailsArrayList;
                        }
                    });



                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            friendsListView.addHeaderView(header);

            final int selectCount= friendsListView.getCheckedItemCount();
            Log.e("SelectedCount",String.valueOf(selectCount));
            if(selectCount<=5)
            {
                friendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int pos, long id)
                    {

                        select(pos);

                    }
                });
            }
            else
            {
                Toast.makeText(context, "You can't select more than 5 guardians", Toast.LENGTH_LONG).show();
            }

             if(purpose.equals("AddGuardians"))
            {

                getLoaderManager().initLoader(0, null, this);

            }
               else if(purpose.equals("SafeWalk"))
             {

                 for(GuardianDetails guardianDetails : database.getAllGuardians())
                 {
                     detailsArrayList.add(new ContactDetails(guardianDetails.getId(), guardianDetails.getName(), guardianDetails.getMobile(), guardianDetails.getImage()));
                 }

             }


/*
            Collections.copy(storeArrayList, detailsArrayList);

             for(ContactDetails details : storeArrayList)
            {
                Log.w("CopyTest", details.getName());

            }
*/

            return view;
        }

         public void select(int pos)
        {
            int position = pos-1;

            contactsListAdapter.toggleRowSelection(position);
            SparseBooleanArray selected = contactsListAdapter.getSelectedRowIds();


            selectedList = new ArrayList<ContactDetails>();

            try {

                for (int i = 0; i < selected.size(); i++) {
                    Log.e("Selected", String.valueOf(selected.size()));
                    if (selected.valueAt(i)) {
                        Log.d("LoopSelected", String.valueOf(i) + selected.valueAt(i));
                        if (detailsArrayList.get(selected.keyAt(i)) != null) {
                            ContactDetails selectedrow = detailsArrayList.get(selected.keyAt(i));
                            selectedList.add(selectedrow);

                        }
                    }
                }

                  if (selectedList != null)
                {
                    Log.e("SelectValue", selectedList.toString());
                    activityCommunicator.SelectedContacts(selectedList, position);
                }
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        }

        @Override
        public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1)
        {
            Uri phone = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
            String[] PHONE_PROJECTION = new String[]
                    {
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                            ContactsContract.CommonDataKinds.Phone.DATA1,
                            ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,
                            ContactsContract.CommonDataKinds.Phone._ID,
                    };
            CursorLoader loader = new CursorLoader(getActivity(), phone, PHONE_PROJECTION, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
            return loader;
        }
        @Override
        public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor)
        {
            Log.w("SwapData", "Invoked 1");
            if(cursor.moveToFirst())
            {
                do
                {
                    if(cursor.getString(1).length() >= 10 && cursor.getString(1).length() <= 16)
                    {
                        String name = cursor.getString(0); //name
                        String mobile = cursor.getString(1); // mobile
                        String url = cursor.getString(2); // url
                        if(mobile != null)
                        {
                            sparseArray.put(mobile, name + "@" + mobile + "@" + url);
                        }
                    }
                }while(cursor.moveToNext());
            }
            Set<String> keyset = sparseArray.keySet();
            ArrayList<String> sortList = new ArrayList<String>();
            sortList.clear();
            if(keyset.size() > 0)
            {
                for(String key : keyset)
                {
                    Log.w("KeySettttttttt", key);
                    if(sparseArray.containsKey(key))
                    {
                        sortList.add(sparseArray.get(key));
                        Log.w("KeySet", sparseArray.get(key));
                    }
                }
            }
            else
            {
                Log.w("KeySet", "Empty");
            }
            Collections.sort(sortList, new Comparator<String>() {
                @Override
                public int compare(String s1, String s2) {
                    return s1.compareToIgnoreCase(s2);
                }
            });
            int bTrue=1;
            for(String value : sortList)
            {
                String[] values =  value.split("@");
                ContactDetails details = new ContactDetails();
                details.setName(values[0]);
                details.setMobile(values[1]);
                details.setpicURL(values[2]);
                detailsArrayList.add(details);
                storeArrayList.add(details);
                contactsListAdapter.notifyDataSetChanged();
            }
            adapters.swapCursor(cursor);
        }
        @Override
        public void onLoaderReset(Loader<Cursor> arg0)
        {
            adapters.swapCursor(null);
        }
    }

    @Override
    public void SelectedContacts(List<ContactDetails> contactDetails, int pos) {
        try {

              if (contactDetails != null)
            {
                StaticMaster.detailsArrayList = contactDetails;

                ContactListActivity.selectedNames.clear();

                for(ContactDetails detail : StaticMaster.detailsArrayList)
                {
                    ContactListActivity.selectedNames.add(detail.getMobile());
                    Log.w("SelectedNames", detail.getName());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }



}
