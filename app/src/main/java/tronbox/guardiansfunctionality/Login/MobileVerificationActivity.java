package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 3/9/2015.
 * User Mobile Verification screen
 */
public class MobileVerificationActivity extends Activity
{
    Context myContext=MobileVerificationActivity.this;
    Button btnMVNext;
    TextView txtMVTermCondition;
    EditText etxtMVMobileNo;
    private ProgressDialog progressDialog;
    private String url = "http://www.leafwearables.com/terms-of-use/";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);
        UIDeclaration();

        txtMVTermCondition.setText(Html.fromHtml("<u><b><a>Terms and privacy policy</b></u>"));
        txtMVTermCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        btnMVNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String strMobile= etxtMVMobileNo.getText().toString();
                if (!strMobile.isEmpty()) {

                    if (strMobile.length() == 10) {

                        SharedPrefrenceStorage.StorePosition(myContext, "MobileVerificationCodeActivity");
                        SharedPrefrenceStorage.StoreMobile(getApplicationContext(), strMobile);

                        StaticMaster.sendCommonData(getApplicationContext(), "NewUser", Networking.CommonUrl+"user", Request.Method.POST);


                        Intent intent1 = new Intent(getApplicationContext(), MobileVerificationCodeActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent1);
                        finish();

                        //progressDialog = ProgressDialog.show(MobileVerificationActivity.this, "Safer Verification", "Sending Verification Code, Please wait....",true);

                    } else {
                        Toast.makeText(myContext, "Mobile Number should be 10 digit", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(myContext, "Mobile Number can not blank", Toast.LENGTH_LONG).show();
                }
            }
        });

        registerReceiver(broadcastReceiver, new IntentFilter("MobileVerificationActivity"));

    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            progressDialog.dismiss();

            Intent intent1 = new Intent(getApplicationContext(), MobileVerificationCodeActivity.class);
            startActivity(intent1);
            finish();


        }
    };

    @Override
    public void finish() {
        super.finish();
        unregisterReceiver(broadcastReceiver);
//        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    //method for declare all UI controls
    protected void UIDeclaration()
    {
        etxtMVMobileNo = (EditText)findViewById(R.id.etxtMVMobileNo);
        btnMVNext=(Button)findViewById(R.id.btnMVNext);
        txtMVTermCondition=(TextView)findViewById(R.id.txtMVTermCondition);
    }

}
