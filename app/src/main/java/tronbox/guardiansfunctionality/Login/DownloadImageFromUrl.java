package tronbox.guardiansfunctionality.Login;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;

import org.apache.http.HttpResponse;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class DownloadImageFromUrl extends AsyncTask<String, String, Bitmap>{

    private String link = null;
    private UserRegisterActivity userRegisterActivity;


    public DownloadImageFromUrl(String link){

        this.userRegisterActivity = userRegisterActivity;
        this.link = link;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

        Bitmap myBitmap = null;


        Log.d(StaticMaster.log, "Profile pic url "+link);

        try {

            HttpURLConnection httpUrlConnection = null;
            URL url = new URL(params[0]);
            httpUrlConnection = (HttpURLConnection) url.openConnection();

            httpUrlConnection.setRequestMethod("GET");

            httpUrlConnection.setDoInput(true);

            InputStream responseStream = new BufferedInputStream(httpUrlConnection.getInputStream());

            myBitmap = BitmapFactory.decodeStream(responseStream);

            Log.d(StaticMaster.log, "Profile pic is donwloaded and saved");

        } catch (IOException e) {
            Log.d(StaticMaster.log, "Error Message "+e.getMessage());
        }


        return null;
    }

    @Override
    protected void onPostExecute(Bitmap finalBitmap) {
        super.onPostExecute(finalBitmap);

        try {
            FileOutputStream out1  = userRegisterActivity.openFileOutput(StaticMaster.UserProfilePic, Context.MODE_PRIVATE);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
            byte[] buffer = bos.toByteArray();
            out1.write(buffer, 0, buffer.length);
            out1.close();


            userRegisterActivity.updateProfileImage(finalBitmap);

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } catch (Exception e){
        }

    }


}
