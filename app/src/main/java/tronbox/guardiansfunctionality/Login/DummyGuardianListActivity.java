package tronbox.guardiansfunctionality.Login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import tronbox.guardiansfunctionality.AddGuardians.ActivityCommunicator;
import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.AddGuardians.ContactsAdapter;
import tronbox.guardiansfunctionality.AddGuardians.ContactsList;
import tronbox.guardiansfunctionality.AddGuardians.DummyContactsList;
import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.R;

/**
 * Created by Shrinivas Mishra on 3/19/2015.
 */
public class DummyGuardianListActivity extends FragmentActivity
{
    Context myContext=DummyGuardianListActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        Fragment fg=new ContactsListFragment();
        Bundle bd=new Bundle();
        bd.putInt("Screen",1);
        fg.setArguments(bd);
        getSupportFragmentManager().beginTransaction().replace(R.id.list_container, fg).commit();
        ImageView imgSelectCL=(ImageView)findViewById(R.id.imgSelectCL);

        imgSelectCL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

      public static class ContactsListFragment extends Fragment
    {
        public Context context;
        DummyContactsList contactsListAdapter;
        ArrayList<ContactDetails> detailsArrayList;

        ArrayList<ContactDetails> tempArrayList = new ArrayList<ContactDetails>();
        ArrayList<ContactDetails> storeArrayList = new ArrayList<ContactDetails>();

        Bundle data;
        int screenValue;
        HashMap<String, String> sparseArray = new HashMap<String, String>();
        ArrayList<ContactDetails> selectedList;
        MobileDatabase database;
        public ContactsListFragment()
        {

        }
        @Override
        public void onAttach(Activity activity){
            super.onAttach(activity);
            context = getActivity();

        }
        @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
        }
        @Override
        public void onSaveInstanceState(Bundle outState){
            super.onSaveInstanceState(outState);
        }
        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.inflate(R.layout.fragment_contact_list, container, false);
            database = new MobileDatabase(getActivity(), "GUARDIAN", null, 1);
            data = getArguments();
            screenValue = data.getInt("Screen");
            detailsArrayList = new ArrayList<ContactDetails>();

              for(GuardianDetails guardianDetails : database.getAllGuardians())
            {

                detailsArrayList.add(new ContactDetails(guardianDetails.getId(), guardianDetails.getName(), guardianDetails.getMobile(), null));

            }



            contactsListAdapter = new DummyContactsList(getActivity(), R.layout.custom_gurdian_list, detailsArrayList);


            final ListView friendsListView = (ListView) view.findViewById(R.id.lstContact);
            friendsListView.setAdapter(contactsListAdapter);

            View header = getActivity().getLayoutInflater().inflate(R.layout.search_header, null);

            ((EditText)header.findViewById(R.id.search_header)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(final CharSequence s, int start, int before, int count) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {



                                String text = s.toString().toLowerCase();

                                detailsArrayList.clear();
                                contactsListAdapter.notifyDataSetChanged();

                                for(ContactDetails str : storeArrayList)
                                {

                                    String text2 = "^("+text+")+([\\x20-\\x7E]+)";

                                    Pattern p = Pattern.compile(text2);

                                    if(p.matcher(str.getName().toLowerCase()).matches())
                                    {

                                        //Toast.makeText(getActivity(), str.getName(), Toast.LENGTH_SHORT).show();
                                        Log.w("TestingMatch", str.getName());

                                        detailsArrayList.add(str);
                                        contactsListAdapter.notifyDataSetChanged();
                                    }
                                }



                        }
                    });



                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            friendsListView.addHeaderView(header);

            storeArrayList.addAll(detailsArrayList);

             for(ContactDetails details : storeArrayList)
            {
                Log.w("CopyTest", details.getName());

            }

            //storeArrayList.addAll(detailsArrayList);

            return view;
        }


    }

}
