package tronbox.guardiansfunctionality;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import tronbox.guardiansfunctionality.Login.SendCommonDataServer;

public class UserVerificationTask extends AsyncTask<String, String, String> {

    private Context context;
    private String verificationCode;

    public UserVerificationTask(Context context){

        this.context = context;

        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();

                try {

                    if (bundle != null) {

                        final Object[] pdusObj = (Object[]) bundle.get("pdus");

                        for (int i = 0; i < pdusObj.length; i++) {

                            SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                            String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                            String senderNum = phoneNumber;
                            String message = currentMessage.getDisplayMessageBody();

                            String verificationCode = message.substring(message.length()-5, message.length()-1);

                            /*JSONObject post_data = new JSONObject();
                            try {

                                if(verificationCode != null){

                                    post_data.put("primaryContact", "7838591957");
                                    post_data.put("verificationCode", verificationCode);

                                    Log.w("JsonData", String.valueOf(post_data));

                                    new UserVerificationTask(context).execute("Type_2", "http://54.169.182.72/api/v0/user/verify", String.valueOf(post_data));

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }*/

                            Log.w("SmsReceiver", "senderNum: " + senderNum + "; message:" + verificationCode);

                        } // end for loop
                    } // bundle is null

                } catch (Exception e) {
                    Log.e("SmsReceiver", "Exception smsReceiver" + e);

                }

            }
        }, intentFilter);


    }

    /*
        params[0] = Type_1 = Send_Mobile# OR Type_2 = Send_Mobile#_+_Verification_Code#
        params[1] = url where data needs to be send.
        params[2] = Data to be sent.

     */


    @Override
    protected String doInBackground(String... params) {

        try{

            HttpURLConnection httpUrlConnection = null;
            URL url = new URL(params[1]);
            httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setUseCaches(false);
            httpUrlConnection.setDoOutput(true);

            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
            httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
            httpUrlConnection.addRequestProperty("content-type","application/json");
            httpUrlConnection.setDoOutput(true);

            if (params[2] != null) {
                OutputStreamWriter wr = new OutputStreamWriter(httpUrlConnection.getOutputStream());
                wr.write(params[2]);
                wr.flush();
            }

            InputStream responseStream = new BufferedInputStream(httpUrlConnection.getInputStream());

            BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
            String line = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = responseStreamReader.readLine()) != null)
            {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();

            String response = stringBuilder.toString();
            Log.w("ServerResponseFromServer", response);

            JSONObject jsonObject = new JSONObject(response);
            String status = jsonObject.getString("status");

            if(params[0].equals("Type_1"))
            {

                if(status.equals("success")) // user is new to the server.
                {
                    verificationCode = jsonObject.getString("verificationCode");

                    JSONObject post_data = new JSONObject();

                    try {

                        post_data.put("primaryContact", "9899972269");
                        post_data.put("verificationCode", verificationCode);

                        Log.w("JsonData", String.valueOf(post_data));

                        new UserVerificationTask(context).execute("Type_2", "http://54.169.182.72/api/v0/user/verify", String.valueOf(post_data));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }   else if(params[0].equals("Type_2"))
            {

                if(status.equals("success")) // user is new to the server.
                {
                    SharedPrefrenceStorage.StoreAuthToken(context, jsonObject.getString("accessCode"));

                    JSONObject post_data = new JSONObject();
                    try {

                        post_data.put("name", "Danny");
                        post_data.put("email", "Danny.migi@gmail.com");
                        post_data.put("gender", "M");


                        Log.w("JsonData", String.valueOf(post_data));

                        new SendCommonDataServer(context).execute("http://54.169.182.72/api/v0/user", String.valueOf(post_data));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }

        }catch(IOException io){} catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

}
