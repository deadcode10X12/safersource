package tronbox.guardiansfunctionality.Maps;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by nikhil on 5/29/15.
 */


public class SaferWalkLiveFeed extends IntentService  {

    private Location loc;
    private Geocoder geocoder;

    public SaferWalkLiveFeed(String name) {
        super(name);

    }

    public SaferWalkLiveFeed() {
        super("");
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());



        Volley.newRequestQueue(getApplicationContext()).add(new JsonObjectRequest(Request.Method.GET, Networking.CommonUrl+"safewalk/"+ SharedPrefrenceStorage.GetActivityId(getApplicationContext())+"/location", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {


                Log.w(StaticMaster.SafeWalkLog, "Pulled data from server " + jsonObject.toString());

                try {

                    if (jsonObject.has("location"))
                    {


                        //********** Logging ********//

                        StaticMaster.logsList.add("Ward's current location is successfully pulled from the server@"+StaticMaster.getCurrentTimeStamp());

                        //**************************//

                        loc = new Location("");
                        loc.setLatitude(Double.parseDouble(jsonObject.getJSONObject("location").getString("lat")));
                        loc.setLongitude(Double.parseDouble(jsonObject.getJSONObject("location").getString("lng")));

                        SharedPrefrenceStorage.StoreVictimCoordinates(getApplicationContext(), jsonObject.getJSONObject("location").getString("lat") + "@" + jsonObject.getJSONObject("location").getString("lng"));

                        sendBroadcast(new Intent("UpdateGuardianMap"));

                        if(loc != null)
                        {
                            
                            List<Address> addresses = null;
                            try {

                                addresses = geocoder.getFromLocation(loc.getLatitude(),
                                        loc.getLongitude(), 1);
                            } catch (IOException e1) {


                            } catch (IllegalArgumentException e2) {

                            }

                            if (addresses != null && addresses.size() > 0) {

                                StringBuffer stringBuffer = new StringBuffer();

                                for(Address address : addresses)
                                {
                                    for(int i=0; i<address.getMaxAddressLineIndex(); i++)
                                    {
                                        Log.w("AddressDecipher", address.getAddressLine(i));
                                        stringBuffer.append(address.getAddressLine(i)+" ");
                                    }

                                }

                                //********** Logging ********//

                                StaticMaster.logsList.add("Coordinates is geocoded, building notification@"+StaticMaster.getCurrentTimeStamp());

                                //**************************//

                                String Message = stringBuffer.toString();

                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(getApplicationContext())
                                                .setSmallIcon(R.drawable.ic_launcher)
                                                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
                                                .setContentTitle("Guardian")
                                                .setContentText(Message);



                                 mBuilder.setSound(Uri.parse("android.resource://"
                                        + getApplicationContext().getPackageName() + "/" + R.raw.notification));



                                NotificationCompat.InboxStyle inboxStyle =  new NotificationCompat.InboxStyle();
                                inboxStyle.setBigContentTitle("Safer");

                                String[] arr = Message.split(" ");

                                if(arr.length > 7) // we need to split
                                {
                                    inboxStyle.addLine(Message.substring(0, Message.indexOf(arr[7])));
                                    inboxStyle.addLine(Message.substring(Message.indexOf(arr[7]), Message.length()));

                                }else {

                                    inboxStyle.addLine(Message);
                                }

                                mBuilder.setStyle(inboxStyle);

                                NotificationManager mNotifyMgr =(NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                                Notification notification = mBuilder.build();

                                notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

                                mNotifyMgr.notify(220, notification);


                                //********** Logging ********//

                                StaticMaster.logsList.add("Realtime location update for Guardian on notification bar@"+StaticMaster.getCurrentTimeStamp());

                                //**************************//

                                Log.w("DecodedAddress", stringBuffer.toString());

                            } else {

                                Log.w("DecodedAddress", "No Address Found");
                            }

                        }



                    }


                } catch (JSONException e) {
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if(volleyError != null && volleyError.getMessage() != null)
                {


                    Log.w(StaticMaster.SafeWalkLog, volleyError.getMessage());
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return StaticMaster.commonHttpHeaders(getApplicationContext());
            }

        });


    }

}
