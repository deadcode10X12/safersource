package tronbox.guardiansfunctionality.Maps;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.HandleGuardians.GuardianAlertList;
import tronbox.guardiansfunctionality.Home.CircleImagePhoto;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 4/7/2015.
 * Showing Guardian Activity List and User current location
 */
public class ShareLocationConfirm extends FragmentActivity implements LocationListener
{

    GoogleMap googleMap;
    GuardianAlertList GAListAdapter;
    Context myContext=ShareLocationConfirm.this;
    TextView txtGuardianAlertLocation;
    ImageView backActivity;
    Typeface robotoFont;

    Button shareLocation;


    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_share_location_test);


        robotoFont = Typeface.createFromAsset(getAssets(), "roboto.ttf");


        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if(status!= ConnectionResult.SUCCESS)
        {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }else {

            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            googleMap = fm.getMap();
            googleMap.setMyLocationEnabled(true);

              if(SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()) != null)
            {
                String coord[] = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");
                Location location1 = new Location("d");
                location1.setLatitude(Double.parseDouble(coord[0]));
                location1.setLongitude(Double.parseDouble(coord[1]));


                updateCoordinates(location1);
            }
        }

        backActivity = (ImageView)findViewById(R.id.share_location_back);
        backActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });

        shareLocation = (Button)findViewById(R.id.share_location);
        shareLocation.setTypeface(robotoFont);
        shareLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    try {

                             if(SharedPrefrenceStorage.GetUserCoordinates(myContext) != null)
                            {

                                String coord[] = SharedPrefrenceStorage.GetUserCoordinates(myContext).split("@");

                                JSONObject data = new JSONObject();
                                data.put("lng", coord[1]);
                                data.put("lat", coord[0]);
                                data.put("receiver", getIntent().getExtras().getString("user_id"));

                                Log.w(StaticMaster.SharedLocationLogs, "Json Data :- " + data.toString());

                                Volley.newRequestQueue(myContext).add(new JsonObjectRequest(Request.Method.POST, Networking.CommonUrl + "user/location/share", data, new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject object) {

                                        Log.w(StaticMaster.SharedLocationLogs, "Json Response :- " + object.toString());

                                    }

                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError volleyError) {

                                        Networking.commonVolleyLogs(volleyError, StaticMaster.SharedLocationLogs);

                                    }
                                }) {
                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {


                                        HashMap<String, String> params = new HashMap<String, String>();

                                        params.put("Content-Type", "application/json");

                                        if (SharedPrefrenceStorage.GetAuthToken(myContext) != null) {
                                            String accessToken = SharedPrefrenceStorage.GetAuthToken(myContext);

                                            Log.d(StaticMaster.log, "AccessToken = " + accessToken);
                                            params.put("authorization", "bearer " + accessToken);
                                        }


                                        return params;
                                    }

                                });


                                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);

                            }else {

                                 Log.w(StaticMaster.SharedLocationLogs, "User location is not available");

                             }


                        }catch (JSONException e){

                             if(e.getMessage() != null)
                            {
                                Log.w(StaticMaster.SharedLocationLogs, "Json Error "+e.getMessage());
                            }
                        }


            }
        });



    }

     public void updateCoordinates(Location location)
    {

        txtGuardianAlertLocation = (TextView) findViewById(R.id.txtGuardianAlertLocation);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        googleMap.addMarker(new MarkerOptions().position(latLng));
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

        //txtGuardianAlertLocation.setText("Latitude:" + latitude + ", Longitude:" + longitude);
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                txtGuardianAlertLocation.setText(strReturnedAddress.toString());
            }
            else{
                txtGuardianAlertLocation.setText("No Address identified!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            txtGuardianAlertLocation.setText("Cannot get Address!");
        }

    }

    @Override
    public void onProviderDisabled(String provider) {
    }
    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}