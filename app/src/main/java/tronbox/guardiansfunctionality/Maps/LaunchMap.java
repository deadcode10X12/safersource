package tronbox.guardiansfunctionality.Maps;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;

public class LaunchMap extends ActionBarActivity {

    Button launch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_map);

        long time = System.currentTimeMillis()/1000;

        Log.w("Timerrrr", ""+time);

        /*SharedPrefrenceStorage.StoreMobile(getApplicationContext(), "9873252281");

        JSONObject post_data = new JSONObject();
        try {

            post_data.put("primaryContact", SharedPrefrenceStorage.GetMobile(getApplicationContext()));

            Log.w("JsonData", String.valueOf(post_data));

            new SendCommonDataServer(getApplicationContext()).execute("http://54.169.182.72/api/v0/user", String.valueOf(post_data), "POST");

        } catch (JSONException e) {
            e.printStackTrace();
        }*/

      //  new AppIdGetterId(getApplicationContext()).execute();


/*
        SharedPrefrenceStorage.StoreVictimCoordinates(getApplicationContext(), 28.6735+"@"+77.2899); // Store the victims location
        new VictimLocationUpdate(getApplicationContext()).execute();

        startService(new Intent(getApplicationContext(), GuardianBrain.class));
*/


/*
        SharedPrefrenceStorage.StoreSafeWalkSource(getApplicationContext(), 28.6735+"@"+77.2899); // Store the victims location
        SharedPrefrenceStorage.StoreSafeWalkDestination(getApplicationContext(), 28.6735+"@"+77.2899); // Store the victims location
*/


        launch =  (Button)findViewById(R.id.launchmap);
        launch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getApplicationContext(), SafeWalk.class);
                intent.putExtra("Source", SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()));
                intent.putExtra("Destination", 28.6735+"@"+77.289);
                startActivity(intent);

            }
        });



    }

}
