package tronbox.guardiansfunctionality.Maps;


import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class MySupportMapFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnCameraChangeListener{
    public View mOriginalContentView;
    public TouchableWrapper mTouchView;
    GoogleMap googleMap;
    MarkerOptions markerOptions;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, parent, savedInstanceState);

        mTouchView = new TouchableWrapper(getActivity());
        mTouchView.addView(mOriginalContentView);

        markerOptions = new MarkerOptions();
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
        googleMap = getMap();

        googleMap.setOnCameraChangeListener(this);
        getMapAsync(this);


        return mTouchView;
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }

        @Override
        public void onCameraChange(CameraPosition cameraPosition)
    {

        Log.w("Address", ""+cameraPosition.target.latitude+"\n"+cameraPosition.target.longitude);
        StaticMaster.cords = cameraPosition.target;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        StaticMaster.cords = googleMap.getCameraPosition().target;
        StaticMaster.googleMap = googleMap;


        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(SharedPrefrenceStorage.GetUserCoordinates(getActivity().getApplicationContext()).split("@")[0]), Double.parseDouble(SharedPrefrenceStorage.GetUserCoordinates(getActivity().getApplicationContext()).split("@")[1]))));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(14));


        //********** Logging ********//

        StaticMaster.logsList.add("Map has loaded@"+StaticMaster.getCurrentTimeStamp());

        //**************************//

    }

}