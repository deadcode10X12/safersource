package tronbox.guardiansfunctionality.Maps;

import android.app.Dialog;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class SafeWalkSetup extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraChangeListener, TouchableWrapper.UpdateMapAfterUserInterection,  GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, SafeWalkDestination.destinationCallback {

    MapFragment mapFragment;
    GoogleMap googleMap;
    MarkerOptions pin;
    LatLng latLng;
    FrameLayout frameLayout;
    boolean click = false;
    TextView searched;

    GoogleApiClient googleApiClient;
    LocationRequest mLocationRequest;

    Typeface robotoFont;

    TextView title;

    Button startButton;
    String destination = null;
    ProgressDialog progressDialog;

    private boolean fragmentOn = false;

    private Geocoder geocoder;
    private GeoHandler handler;
    String log = "";

    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_draw);

        mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);

        StaticMaster.SaferWalkSimulation = false; // Run SaferWalk as a simulation from Source to Destination

        robotoFont = Typeface.createFromAsset(getAssets(), "roboto.ttf");

        title = (TextView)findViewById(R.id.safe_walk_title);
        title.setTypeface(robotoFont);

        startButton = (Button)findViewById(R.id.safe_walk_start);
        startButton.setTypeface(robotoFont);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                   if(searched.getText().length() > 0) // user has entered the address to search
                 {
                      if(destination == null) // User has Skipped Search Part - 1
                    {
                        // Let's reverse geocode the destination address

                        // Progress Dialog will hold the user for further operations

                        final Dialog storeDialog = new Dialog(SafeWalkSetup.this);
                        storeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        storeDialog.setContentView(R.layout.safer_walk_fav_location);

                        TextView textView = (TextView)storeDialog.findViewById(R.id.address);
                        textView.setText(searched.getText().toString());

                        final EditText editText = ((EditText)storeDialog.findViewById(R.id.tag));

                        Button yes = (Button)storeDialog.findViewById(R.id.yes);
                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                int dbResponse = StaticMaster.database(getApplicationContext()).insertDestination(searched.getText().toString(), editText.getText().toString());

                                Log.w(StaticMaster.SafeWalkLog, "Destination Insertion Response "+dbResponse); // if res = 1 Inserted, if 0 Not_Inserted

                                preLastOperation();
                                storeDialog.dismiss();
                            }
                        });


                        Button skip = (Button)storeDialog.findViewById(R.id.skip);
                        skip.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                int dbResponse = StaticMaster.database(getApplicationContext()).insertDestination(searched.getText().toString(), null);

                                Log.w(StaticMaster.SafeWalkLog, "Destination Insertion Response "+dbResponse); // if res = 1 Inserted, if 0 Not_Inserted

                                preLastOperation();
                                storeDialog.dismiss();
                            }
                        });


                        storeDialog.show();


                   }
                        else  // User has performed the precise destination search

                      {
                          // Let's jump to last operation

                          lastOperation();
                      }

                }
                   else
                 {
                   Toast.makeText(getApplicationContext(), "Please choose destination for SaferWalk", Toast.LENGTH_SHORT).show();
                 }



            }
        });


        searched = (TextView)findViewById(R.id.address);
        searched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getSupportFragmentManager().beginTransaction().addToBackStack("fraggy").replace(android.R.id.content, new SafeWalkDestination()).commit();

                fragmentOn = true;
            }
        });
        searched.setTypeface(robotoFont);


        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        HandlerThread thread = new HandlerThread("");
        thread.start();

         handler = new GeoHandler(thread.getLooper());


    }

      public void preLastOperation()
    {


        progressDialog = ProgressDialog.show(SafeWalkSetup.this, "Starting SaferWalk", "Please wait....", true);

        //********** Logging ********//

        StaticMaster.logsList.add("User has pressed the SafeWalk start button@"+StaticMaster.getCurrentTimeStamp());

        StaticMaster.logsList.add("Progress dialog is running@"+StaticMaster.getCurrentTimeStamp());

        //**************************//

        String url = "https://maps.googleapis.com/maps/api/geocode/json?address="+searched.getText().toString().replace(" ", "+")+"&key=AIzaSyDVmRv9bPH3JG7tUWpBl7BMBrJ8g0PGTWo";

        Log.w(StaticMaster.SafeWalkLog, "Final address to decode "+url);

        //Launching the volley request to reverse geocode

        //********** Logging ********//

        StaticMaster.logsList.add("Request for final reverse geocoding has initiated@"+StaticMaster.getCurrentTimeStamp());

        //**************************//


        Volley.newRequestQueue(getApplicationContext()).add(new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject object) {

                // Parsing JSON String fetching the lat and longs from JSON String

                try {

                    JSONArray array = object.getJSONArray("results");

                    JSONObject jsonObject = array.getJSONObject(0).getJSONObject("geometry").getJSONObject("location");

                    destination = jsonObject.getString("lat")+"@"+jsonObject.getString("lng");

                    Log.w(StaticMaster.SafeWalkLog, "Destination Location "+destination);

                    //********** Logging ********//

                    StaticMaster.logsList.add("Destination is reverse geocoded successfully@"+StaticMaster.getCurrentTimeStamp());

                    //**************************//


                    lastOperation();

                } catch (JSONException e) {

                    e.printStackTrace();
                    Log.w(StaticMaster.SafeWalkLog, "Json Error "+e.getMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if(progressDialog != null)
                {
                    progressDialog.dismiss();
                }

                dialog = new Dialog(SafeWalkSetup.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_dailog_without_image);

                dialog.setContentView(R.layout.custom_dailog_single_button);

                ((TextView)dialog.findViewById(R.id.message)).setText("Server is not responding try again");

                TextView ok = (TextView)dialog.findViewById(R.id.yes);
                ok.setText("Ok");
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
                        dialog.dismiss();
                    }
                });

                dialog.show();

                Networking.commonVolleyLogs(volleyError, StaticMaster.SafeWalkLog+"_Destination_Decode_Error");

            }
        }));

    }

    @Override
    protected void onResume() {
        super.onResume();

        StaticMaster.logsList.clear();

    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, (LocationListener) this);
    }

      public void refresh(LatLng latLng)
    {
        StaticMaster.googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        StaticMaster.googleMap.animateCamera(CameraUpdateFactory.zoomTo(13));
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

      String currentLocation[] = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");

      refresh(new LatLng(Double.parseDouble(currentLocation[0]), Double.parseDouble(currentLocation[1])));
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {


        latLng = cameraPosition.target;

        Log.w("Maps", ""+cameraPosition.target.latitude);
    }



    @Override
    public void onUpdateMapAfterUserInterection() {

          if(StaticMaster.cords != null)
        {

            Location location = new Location("");
            location.setLatitude(StaticMaster.cords.latitude);
            location.setLongitude(StaticMaster.cords.longitude);


            Message message = handler.obtainMessage(1, location);
//            message.sendToTarget();


            //********** Logging ********//

             StaticMaster.logsList.add("User is scrolling map@"+StaticMaster.getCurrentTimeStamp());

            //**************************//

            new GetAddressTask().execute(location);

        }

        Log.w("TouchListener", "Click");
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

       StaticMaster.location = location;

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void choosenDestination(LatLng coordinates, String address) {

         if(coordinates != null && address != null)
        {
            Log.w(StaticMaster.SafeWalkLog, "Choosen Destination Decoded "+coordinates.latitude+"-"+coordinates.longitude+"---"+address);

            fragmentOn = false;
            getSupportFragmentManager().popBackStack();
            searched.setText(address);
            refresh(coordinates);
            destination = String.valueOf(coordinates.latitude+"@"+coordinates.longitude);
        }
    }

    class GetAddressTask extends AsyncTask<Location, String, String>{

        @Override
        protected String doInBackground(Location... params) {

            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            Location loc = params[0];

            //Log.w("DecodedAddress", ""+msg.getData().getString("lat"));

            if(loc != null)
            {


                List<Address> addresses = null;
                try {

                    addresses = geocoder.getFromLocation(loc.getLatitude(),
                            loc.getLongitude(), 1);
                } catch (IOException e1) {


                } catch (IllegalArgumentException e2) {

                }

                if (addresses != null && addresses.size() > 0) {

                    StringBuffer stringBuffer = new StringBuffer();


                    Address addressData = addresses.get(0);


                     if(addressData != null && addressData.getAddressLine(0) != null)
                    {
                        stringBuffer.append(addressData.getAddressLine(0)+", ");

                         if(addressData.getSubLocality() != null)
                        {
                            stringBuffer.append(addressData.getSubLocality()+", ");
                        }

                         if(addressData.getLocality() != null)
                        {
                            stringBuffer.append(addressData.getLocality());
                        }

                        String postalCode = addressData.getPostalCode();

                         if(postalCode != null)
                        {
                            stringBuffer.append("-"+postalCode.substring(postalCode.length()-2,postalCode.length()));
                        }

                    }






                    /*for(Address address : addresses)
                    {
                        for(int i=0; i<address.getMaxAddressLineIndex(); i++)
                        {
                            Log.w("AddressDecipher", address.getAddressLine(i));
                            stringBuffer.append(address.getAddressLine(i)+" ");
                        }

                    }*/

                    log = stringBuffer.toString();

                } else {

                    log = "No Address Found";
                }



                Log.w("DecodedAddress", log);
            }

            return log;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            searched.setText(s);
        }
    }


    class GeoHandler extends Handler {

        Message msg;
        Location loc;

        Runnable task = new Runnable() {
            @Override
            public void run() {

                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                //Log.w("DecodedAddress", ""+msg.getData().getString("lat"));

                if(loc != null)
                {


                    List<Address> addresses = null;
                    try {

                        addresses = geocoder.getFromLocation(loc.getLatitude(),
                                loc.getLongitude(), 1);
                    } catch (IOException e1) {


                    } catch (IllegalArgumentException e2) {

                    }

                    if (addresses != null && addresses.size() > 0) {

                        StringBuffer stringBuffer = new StringBuffer();

                        Address addressData = addresses.get(0);

                        stringBuffer.append(addressData.getAddressLine(0)+",");
                        stringBuffer.append(addressData.getSubLocality()+",");
                        stringBuffer.append(addressData.getLocality());

                         /* for(Address address : addresses)
                        {

                              for(int i=0; i<address.getMaxAddressLineIndex(); i++)
                            {
                                Log.w("AddressDecipher", address.getAddressLine(i));
                                stringBuffer.append(address.getAddressLine(i)+" ");
                            }
                        }*/

                        log = stringBuffer.toString();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                searched.setText(log);
                            }
                        });

                    } else {

                        log = "No Address Found";
                    }



                    Log.w("DecodedAddress", log);
                }

            }
        };

        GeoHandler(Looper looper){
            super(looper);
        }

        @Override
        public void handleMessage(final Message msg) {

            super.handleMessage(msg);

            this.msg = msg;

            if(loc != null)
            {

                Log.w("DecodedAddress", "Removed Task");
                removeCallbacks(task);
            }

            loc = (Location)msg.obj;

            post(task);

        }
    }


    /*

     Last operation is :-

     1) Construct the proper json to send the safer_walk request to the choosen guardian or ward.
     2) Send the volley request.
     3) It it's a simulation then get the polyline coordinates as well otherwise jump to saferwalk class.

    */


      public void lastOperation()
    {


        if(SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()) != null && destination != null && StaticMaster.tempGuardianDetails != null)
        {

            JSONObject post_data = new JSONObject();

            try {

                JSONArray jsonArray1 = new JSONArray();
                String[] data = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@"); // enter the source
                jsonArray1.put(data[0]);
                jsonArray1.put(data[1]);


                JSONArray jsonArray2 = new JSONArray();
                jsonArray2.put(String.valueOf(StaticMaster.cords.latitude));
                jsonArray2.put(String.valueOf(StaticMaster.cords.longitude));

                JSONArray jsonArray3 = new JSONArray();

                jsonArray3.put(StaticMaster.tempGuardianDetails.getId());


                post_data.put("src", jsonArray1);
                post_data.put("dst", jsonArray2);
                post_data.put("actors", jsonArray3);

                Log.w(StaticMaster.SafeWalkLog, "Json Source & Destination "+post_data.toString());

            } catch (JSONException e) {

                Log.w(StaticMaster.SafeWalkLog, "Json Error "+e.getMessage());
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    //********** Logging ********//

                    StaticMaster.logsList.add("If response didn't came in 10 sec SaferWalk will stop@"+StaticMaster.getCurrentTimeStamp());

                    //**************************//


                    dialog = new Dialog(SafeWalkSetup.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.custom_dailog_without_image);

                    dialog.setContentView(R.layout.custom_dailog_single_button);

                    ((TextView)dialog.findViewById(R.id.message)).setText("Server is not responding try again");

                    TextView ok = (TextView)dialog.findViewById(R.id.yes);
                    ok.setText("Ok");
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            finish();
                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                }
            }, 20000);

            //********** Logging ********//

            StaticMaster.logsList.add("Sending SaferWalk request to the guardian@"+StaticMaster.getCurrentTimeStamp());

            //**************************//

            Volley.newRequestQueue(getApplicationContext()).add(new JsonObjectRequest(Request.Method.POST, "http://54.169.182.72/api/v0/safewalk", post_data, new Response.Listener<JSONObject>()

                 {
                     @Override
                     public void onResponse(JSONObject jsonObject) {

                         Log.w(StaticMaster.SafeWalkLog, "SafeWalk Json Response " + jsonObject.toString());

                         try {

                             if (jsonObject.has("safewalkID")) {


                                 //********** Logging ********//

                                 StaticMaster.logsList.add("Response is received successfully@"+StaticMaster.getCurrentTimeStamp());

                                 //**************************//

                                 StaticMaster.ActivityId = jsonObject.getString("safewalkID");
                                 SharedPrefrenceStorage.StoreActivityid(getApplicationContext(), StaticMaster.ActivityId);

                                 // Store the safer walk case id
                                 SharedPrefrenceStorage.StoreSaferWalkCaseid(getApplicationContext(), jsonObject.getString("safewalkID"));

                                 // Store the safer walk source
                                 SharedPrefrenceStorage.StoreSaferWalkSource(getApplicationContext(), SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()));

                                 // Store the safer walk destination
                                 SharedPrefrenceStorage.StoreSaferWalkDestination(getApplicationContext(), destination);

                                 // Store the safer walk initiator
                                 SharedPrefrenceStorage.StoreSaferWalkInitiator(getApplicationContext(), "Me");

                                 // Store the safer walk listener
                                 SharedPrefrenceStorage.StoreSaferWalkListener(getApplicationContext(), StaticMaster.tempGuardianDetails.getId());


                                 StaticMaster.CurrentState = StaticMaster.States.SafeWalk;
                                 StaticMaster.sosDuration = StaticMaster.safeWalkDuration;
                                 sendBroadcast(new Intent("LocationHandler"));

                                 if (StaticMaster.SaferWalkSimulation == true) {

                                     String[] data = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");
                                     String[] data2 = destination.split("@");

                                     Volley.newRequestQueue(getApplicationContext()).add(new StringRequest(Request.Method.GET, "https://maps.googleapis.com/maps/api/directions/json?mode=driving&key=AIzaSyDPrJAuUFQim7YEAvHq9CutVsFqqZY9dkE&origin=" + data[0] + "," + data[1] + "&destination=" + data2[0] + "," + data2[1], new Response.Listener<String>() {

                                         @Override
                                         public void onResponse(String response) {

                                             if (response != null) {

                                                 ArrayList<String> polyList = new ArrayList<String>();

                                                 try {

                                                     JSONObject jsonObject = new JSONObject(response);

                                                     JSONArray steps = jsonObject.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONArray("steps");

                                                     for (int i = 0; i < steps.length(); i++) {
                                                         JSONObject polyline = steps.getJSONObject(i).getJSONObject("polyline");
                                                         polyList.add(polyline.getString("points"));
                                                     }

                                                 } catch (JSONException e) {
                                                     e.printStackTrace();
                                                 }

                                                 StaticMaster.masterCordList.clear();

                                                 for (String id : polyList) {

                                                     for (LatLng l : PolyUtil.decode(id)) {


                                                         StaticMaster.masterCordList.add(l);
                                                     }

                                                 }

                                             }


                                             if (progressDialog != null) {
                                                 progressDialog.dismiss();
                                             }

                                             Intent intent = new Intent(getApplicationContext(), SafeWalk.class);
                                             intent.putExtra("user", true);
                                             intent.putExtra("user_id", StaticMaster.tempGuardianDetails.getId());
                                             intent.putExtra("Source", SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()));
                                             intent.putExtra("Destination", destination);
                                             startActivity(intent);

                                         }
                                     }, new Response.ErrorListener() {
                                         @Override
                                         public void onErrorResponse(VolleyError volleyError) {

                                             Networking.commonVolleyLogs(volleyError, StaticMaster.SafeWalkLog + "_Polyline_Fetch_Error");

                                             if (progressDialog != null) {
                                                 progressDialog.dismiss();
                                             }

                                             Intent intent = new Intent(getApplicationContext(), SafeWalk.class);
                                             intent.putExtra("user", true);
                                             intent.putExtra("user_id", StaticMaster.tempGuardianDetails.getId());
                                             intent.putExtra("Source", SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()));
                                             intent.putExtra("Destination", destination);
                                             startActivity(intent);


                                         }
                                     }));


                                 } else {

                                     if (progressDialog != null) {
                                         progressDialog.dismiss();
                                     }

                                     Intent intent = new Intent(getApplicationContext(), SafeWalk.class);
                                     intent.putExtra("user", true);
                                     intent.putExtra("user_id", StaticMaster.tempGuardianDetails.getId());
                                     intent.putExtra("Source", SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()));
                                     intent.putExtra("Destination", destination);
                                     startActivity(intent);

                                 }
                             }

                         } catch (JSONException e) {

                             Log.w(StaticMaster.SafeWalkLog, "Json format error " + e.getMessage());
                         }


                     }
                 }, new Response.ErrorListener() {
                     @Override
                     public void onErrorResponse(VolleyError volleyError) {

                         Networking.commonVolleyLogs(volleyError, StaticMaster.SafeWalkLog);

                     }
                 }) {
                     @Override
                     public Map<String, String> getHeaders() throws AuthFailureError {

                         return StaticMaster.commonHttpHeaders(getApplicationContext());
                     }

                 });

        }

    }


    @Override
    public void onBackPressed() {

         if(fragmentOn == true)
        {
            fragmentOn = false;
            getSupportFragmentManager().popBackStack();
        }
          else {
             finish();
             super.onBackPressed();
         }


    }

}