package tronbox.guardiansfunctionality.Maps;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;


public class SOS extends FragmentActivity implements GoogleMap.InfoWindowAdapter {

    GoogleMap mMap;
    Timer timer;
    boolean flip = false;
    Polyline polyline;
    PolylineOptions polylineOptions;

    String TAG = "PlottingMapCoordinates";

    TextView sosTitle, sosCancel, sosNavigate;
    LinearLayout bottomView;

    Typeface robotoFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plotting_map);

        robotoFont = Typeface.createFromAsset(getAssets(), "roboto.ttf");


        sosTitle = (TextView)findViewById(R.id.sos_title);
        sosTitle.setTypeface(robotoFont);

        sosCancel = (TextView)findViewById(R.id.sos_cancel);
        sosCancel.setTypeface(robotoFont);
        sosCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(SOS.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_dailog_without_image);
                TextView txtMiddleHeadingW = (TextView) dialog.findViewById(R.id.txtMiddleHeadingW);
                txtMiddleHeadingW.setText("Do you want to cancel ?");
                dialog.show();

                RelativeLayout rlDialogAcceptW = (RelativeLayout) dialog.findViewById(R.id.rlDialogAcceptW);
                rlDialogAcceptW.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPrefrenceStorage.StoreRunningActivity(getApplicationContext(), null);

                        StaticMaster.CurrentState = StaticMaster.States.Normal;
                        StaticMaster.sosDuration = StaticMaster.sosNormalDuration;
                        sendBroadcast(new Intent("LocationHandler"));
                        dialog.dismiss();

                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    }
                });

                RelativeLayout rlDialogDeclineW = (RelativeLayout) dialog.findViewById(R.id.rlDialogDeclineW);
                rlDialogDeclineW.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();

                    }
                });


            }
        });

        sosNavigate = (TextView)findViewById(R.id.sos_navigate);
        sosNavigate.setTypeface(robotoFont);

        bottomView = (LinearLayout)findViewById(R.id.sos_bottom_view);
        bottomView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(polyline != null)
                {

                    if(polyline.isVisible())
                    {

                          //  Toast.makeText(getApplicationContext(), "Polyline Visible", Toast.LENGTH_SHORT).show();

                        polyline.setVisible(false);
                        //polyline.remove();

                    }else {

                        //      Toast.makeText(getApplicationContext(), "Polyline Not Visible", Toast.LENGTH_SHORT).show();

                        polyline.setVisible(true);
                        //updatePolyLine();
                    }

                }else {

                    //  Toast.makeText(getApplicationContext(), "Polyline Null", Toast.LENGTH_SHORT).show();

                    updatePolyLine();
                }

            }
        });

        IntentFilter filter = new IntentFilter("Guardians_Update");
        registerReceiver(mMessageReceiver, filter);


        SupportMapFragment gMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.googleMap));
        mMap = gMap.getMap();
        //mMap.setInfoWindowAdapter(this);

        timer = new Timer();

            timer.schedule(new TimerTask() {
                @Override
                public void run() {

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            String url = "http://54.169.182.72/api/v0/activity/"+ SharedPrefrenceStorage.GetActivityId(getApplicationContext())+"/locations?profile=1&volunteer=0";

                            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject jsonObject) {

                                    Log.d("ServerResponse", jsonObject.toString());

                                    try{

                                    if(jsonObject.has("result"))
                                    {
                                        JSONObject master = jsonObject.getJSONObject("result");

                                        JSONObject victim = master.getJSONObject("victim");

                                        String id = victim.getString("user_id");
                                        String name = victim.getString("name");
                                        String mobile = victim.getString("primaryContact");
                                        String lat = victim.getString("lat");
                                        String lng = victim.getString("lng");
                                        SharedPrefrenceStorage.StoreVictimCoordinates(getApplicationContext(), lat+"@"+lng);
                                        Log.d(TAG, "Victim "+id+"_"+name+"_"+mobile+"_"+lat+"_"+lng);

                                        /*JSONArray array = master.getJSONArray("guardians");

                                          for(int i=0; i<array.length(); i++)
                                        {
                                            JSONObject temp = array.getJSONObject(i);

                                             id = temp.getString("user_id");
                                             name = temp.getString("name");
                                             mobile = temp.getString("primaryContact");
                                            String photo = master.getString("photo");
                                            String location = null; // complete this

                                              if(!StaticMaster.tempGuardiansList.containsKey(id))
                                            {
                                                StaticMaster.tempGuardiansList.put(id, new ContactDetails(id, name, mobile, photo, location));
                                                Log.d(TAG, "Guardian "+id+"_"+name+"_"+mobile);
                                            }

                                            //database.insertTemp(id, name, mobile, photo, null);

                                        }*/


                                        updateCoordinates();
                                    }

                                    }catch (JSONException e){}

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {

                                }
                            }){
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {

                                    return StaticMaster.commonHttpHeaders(getApplicationContext());
                                }

                            };


                            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                            requestQueue.add(jsObjRequest);
                            requestQueue.start();

                        }
                    });

                }

            }, 1000, StaticMaster.sosDistressDuration);

    }
    @Override
    public void onResume() {
        super.onResume();

        SharedPrefrenceStorage.StoreRunningActivity(getApplicationContext(), "SOS");

        registerReceiver(mMessageReceiver, new IntentFilter("receive_updates"));
        updateCoordinates();
    }

    //Must unregister onPause()
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver);
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            updateCoordinates();

            Log.w(TAG, "Guardians Update Received");

        }
    };


      public void updatePolyLine()
    {
        polylineOptions = new PolylineOptions();

         if(polylineOptions != null) // update the Polyline Options
        {
           Log.w("Polyoptions", "Not Null");

            if(StaticMaster.masterCordList.size() > 0)
            {
                for(LatLng l : StaticMaster.masterCordList)
                {
                    polylineOptions.add(l);
                }
            }

        }else {

             Log.w("Polyoptions", "Null");

         }

        polyline = mMap.addPolyline(polylineOptions);

    }

      public void updateCoordinates()
    {
        String[] coordinates = null;
        LatLng location;

        mMap.clear();

        coordinates = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");
        location = new LatLng(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));

        mMap.addMarker(new MarkerOptions().position(location).title("Home").icon(BitmapDescriptorFactory.fromBitmap(StaticMaster.getCustomMapPointer(getApplicationContext(), StaticMaster.UserProfilePic, null)))); // add victim

        coordinates = SharedPrefrenceStorage.GetVictimCoordinates(getApplicationContext()).split("@");
        location = new LatLng(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));

        mMap.addMarker(new MarkerOptions().position(location).title("Home").icon(BitmapDescriptorFactory.fromBitmap(StaticMaster.getCustomMapPointer(getApplicationContext(), SharedPrefrenceStorage.GetActivityUserId(getApplicationContext()), null)))); // add victim

        if(flip == false)
        {
            flip = true;

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14.0f), 5000, null);

        }

        Set<String> keys = StaticMaster.tempGuardiansList.keySet();

          for(String key : keys)
        {
            ContactDetails  contactDetails = StaticMaster.tempGuardiansList.get(key);

            String[] latLngList = contactDetails.getLocation().split("@");

            location = new LatLng(Double.parseDouble(latLngList[0]), Double.parseDouble(latLngList[1]));

            Networking.LiveStreamMapImage(getApplicationContext(), contactDetails.getpicURL(), mMap, location);

        }

        Log.d("ServerResponse", "Location Update Received");

        Toast.makeText(getApplicationContext(), "Position Updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public View getInfoWindow(Marker marker) {

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.map_info_window, null);

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {

        return null;
    }


}
