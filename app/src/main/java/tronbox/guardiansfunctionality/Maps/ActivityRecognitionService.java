
/*

 This service performs the following functions :-

 1) Extract result from the incomming intent and figure out the type of activity user is doing now, like still, on_Foot, in_vehical etc.
 2) Once activity is determined and user is not in still position it means system needs to update the gps coords to the server.
 3) If the coords are successfully stored with the valid response system will log it and if failed it will log the error.

 Log System :-

 a) User Activity Type.
 b) Get the latest coordinates.
 c) Reverse Geocode the coordinates to get the address.

   Log Schema :- [ Time -- Activity_Type -- Address ]

*/


package tronbox.guardiansfunctionality.Maps;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import tronbox.guardiansfunctionality.StaticMaster;

public class ActivityRecognitionService extends IntentService {

    private String TAG = "UserActivityDetectionnnnnnn";

    private enum Types {

        Unknown, Still, Walking, Running, IN_VEHICLE, ON_BICYCLE, ON_FOOT
    }


    public ActivityRecognitionService() {
        super("ActivityRecognitionService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

           if(ActivityRecognitionResult.hasResult(intent))
        {
            Types type = getUserActivityDetails(ActivityRecognitionResult.extractResult(intent).getMostProbableActivity().getType());

              if(Types.Still.compareTo(type) != 0 && Types.Unknown.compareTo(type) != 0)
            {
                // User is on move, get the current coord. and reverse geocode it

                sendBroadcast(new Intent("UserActivityDetection"));
            }
                else
              {
                  Log.w(StaticMaster.UserActivity, "User is "+type.name());

              }
        }

    }

      public Types getUserActivityDetails(int type)
    {
        Types activityType = Types.Unknown;

          switch (type)
        {

            case DetectedActivity.STILL : {

                activityType = Types.Still;

            }break;

            case DetectedActivity.WALKING : {

                activityType = Types.Walking;

            }break;

            case DetectedActivity.RUNNING : {

                activityType = Types.Running;

            }break;


            case DetectedActivity.IN_VEHICLE : {

                activityType = Types.IN_VEHICLE;

            }break;

            case DetectedActivity.UNKNOWN : {

                activityType = Types.Unknown;

            }break;

            case DetectedActivity.ON_BICYCLE : {

                activityType = Types.ON_BICYCLE;

            }break;

            case DetectedActivity.ON_FOOT : {

                activityType = Types.ON_FOOT;

            }break;

        }



        return activityType;
    }

}
