package tronbox.guardiansfunctionality.Maps;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;


public class MapDrawDecoder {

    private RequestQueue mapRequestQueue;
    private String url;
    private StringRequest stringRequest;



    public MapDrawDecoder(final Context context, String source, String destination){

        String[] data =  source.split("@");//SharedPrefrenceStorage.GetUserCoordinates(context).split("@");
        String[] data2 = destination.split("@");//SharedPrefrenceStorage.GetVictimCoordinates(context).split("@");


        mapRequestQueue = Volley.newRequestQueue(context);
        url = "https://maps.googleapis.com/maps/api/directions/json?mode=driving&key=AIzaSyDPrJAuUFQim7YEAvHq9CutVsFqqZY9dkE&origin="+data[0]+","+data[1]+"&destination="+data2[0]+","+data2[1];

       // Log.w("PolylineId", url);

        StaticMaster.masterCordList.clear();


        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {

                //Log.w("PolylineId", response);

                if(response != null){

                    ArrayList<String> polyList = getPlaceId(response);

                    StaticMaster.masterCordList.clear();

                    for(String id : polyList){

                        for(LatLng l : PolyUtil.decode(id)){

                            StaticMaster.masterCordList.add(l);
                        }

                    }

                    if(StaticMaster.masterCordList.size() > 1){

                     //   Toast.makeText(context, "Polyline Created", Toast.LENGTH_SHORT).show();

                        context.sendBroadcast(new Intent("Test_Update"));
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if(volleyError != null){


                }

            }
        });

        mapRequestQueue.add(stringRequest);


    }

    public ArrayList<String> getPlaceId(String response){

        ArrayList<String> polyList = new ArrayList<String>();

        try {
            JSONObject jsonObject = new JSONObject(response);

            JSONArray steps = jsonObject.getJSONArray("routes").getJSONObject(0).getJSONArray("legs").getJSONObject(0).getJSONArray("steps");

              for(int i=0; i<steps.length(); i++)
            {
               JSONObject polyline = steps.getJSONObject(i).getJSONObject("polyline");
               polyList.add(polyline.getString("points"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  polyList;
    }


}
