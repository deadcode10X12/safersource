/*
   The goal of this class is to Start the SaferWalk and maintain updation for both Ward and Guardian side.
*/


package tronbox.guardiansfunctionality.Maps;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;


public class SafeWalk extends FragmentActivity implements GoogleMap.InfoWindowAdapter{
    GoogleMap mMap;
    Timer timer, updateTimer;

    ImageView guardian, callButton;
    TextView timerText, title, guardianName, callText;

    boolean flip = false;
    Polyline polyline;
    PolylineOptions polylineOptions;

    String TAG = "PlottingMap";

    int minute = 0, second = 0;
    String minuteText, secondText, source, destination;

    private ArrayList<String> coordList = new ArrayList<String>();
    Marker marker = null;

    int i = -1;

    Button stopButton;
    Typeface robotoFont;

    boolean user = false;

    LatLng location = null;

    private Location sourceLocation = new Location("Source"), destinationLocation = new Location("Destination");

    private LatLng sourceLatLng, destinationLatLng;

    private GuardianDetails guardianDetails;

    private Bitmap bitmap;

    private String topName, topImage, topMobile, victimImage;

    private GuardianDetails saferWalkListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_safe_walk);

        initUI(); // Initialize the UI
        bundleParser(); // Parse the bundle from the incoming intent

        try {


              if(SharedPrefrenceStorage.GetSaferWalkInitiator(getApplicationContext()).equals("Me"))
            {
                SharedPrefrenceStorage.StoreVictimCoordinates(getApplicationContext(), SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()));

                saferWalkListener = StaticMaster.getSaferWalkListener(getApplicationContext(), SharedPrefrenceStorage.GetSaferWalkListener(getApplicationContext()));

                topImage = saferWalkListener.getId();
                topName = saferWalkListener.getName();
                topMobile = saferWalkListener.getMobile();
                victimImage = StaticMaster.UserProfilePic;

            }
               else if(SharedPrefrenceStorage.GetSaferWalkListener(getApplicationContext()).equals("Me"))

             {
                 saferWalkListener = StaticMaster.getSaferWalkListener(getApplicationContext(), SharedPrefrenceStorage.GetSaferWalkInitiator(getApplicationContext()));

                 topImage = saferWalkListener.getId();
                 topName = saferWalkListener.getName();
                 topMobile = saferWalkListener.getMobile();
                 victimImage = saferWalkListener.getId();

             }

            guardianName.setText(topName);
            bitmap = roundMyPhoto(BitmapFactory.decodeStream(openFileInput(topImage)));

            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + topMobile));
                    startActivity(callIntent);
                }
            });


        } catch (FileNotFoundException e) {
            Log.w(StaticMaster.SafeWalkLog, "Profile Pic Not Found");


        }
          finally {


            if(bitmap != null)
            {
                guardian.setImageBitmap(bitmap);
            }

        }



        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmCloser();


            }
        });



        coordList.clear();

        coordList.add(source);



        updateTimer = new Timer();

          if(user == false) // he is a guardian
        {

            stopButton.setVisibility(View.GONE);

            updateTimer.schedule(new TimerTask() {
                @Override
                public void run() {

                    //********** Logging ********//

                    StaticMaster.logsList.add("Starting Intent_Service to get ward's current location followed by geocoding@"+StaticMaster.getCurrentTimeStamp());

                    //**************************//

                    startService(new Intent(getApplicationContext(), SaferWalkLiveFeed.class));

                }

            }, 500, 60000);


        } else { // he is a ward

              StaticMaster.CurrentState = StaticMaster.States.SafeWalk; // change the state to safewalk.

              if(StaticMaster.SaferWalkSimulation == true)
            {
                source = StaticMaster.masterCordList.get(0).latitude+"@"+StaticMaster.masterCordList.get(0).longitude;
                destination = StaticMaster.masterCordList.get(StaticMaster.masterCordList.size()-1).latitude+"@"+StaticMaster.masterCordList.get(StaticMaster.masterCordList.size()-1).longitude;
            }

        }

        updateCoordinates();

    } // onCreate() ends here

      public void initUI()
    {

        //************* Setup the Map ****************//

        SupportMapFragment gMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.googleMap));
        mMap = gMap.getMap();

        //*************************************//

        robotoFont = Typeface.createFromAsset(getAssets(), "roboto.ttf");
        title = (TextView)findViewById(R.id.safe_walk_title);
        title.setTypeface(robotoFont);
        guardianName = (TextView)findViewById(R.id.safe_walk_guardian_name);
        guardianName.setTypeface(robotoFont);
        guardian = (ImageView)findViewById(R.id.safe_walk_guardian);
        callButton = (ImageView)findViewById(R.id.safe_walk_call);
        callText = (TextView)findViewById(R.id.safe_walk_call_text);
        callText.setTypeface(robotoFont);
        stopButton = (Button)findViewById(R.id.safe_walk_stop);
        stopButton.setTypeface(robotoFont);
        timerText = (TextView)findViewById(R.id.safe_walk_timer);
        timerText.setTypeface(robotoFont);


        //********** Setup & Start the Clock ***********//

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        second = second + 1;

                        if(second == 60)
                        {
                            second = 0;
                            minute = minute + 1;
                        }

                        if(second/10 == 0)
                        {
                            secondText = "0"+String.valueOf(second);
                        }else {
                            secondText = String.valueOf(second);
                        }

                        if(minute/10 == 0)
                        {
                            minuteText = "0"+String.valueOf(minute);
                        }else {
                            minuteText = String.valueOf(minute);
                        }

                        timerText.setText(minuteText+":"+secondText);

                    }
                });

            }

        }, 1000, 1000);

        //*************************************//
    }

      public void bundleParser()
    {

        /**** Parsing Bundle From Intent ****/

        // Source and Destination for SaferWalk

        source = getIntent().getExtras().getString("Source");
        destination = getIntent().getExtras().getString("Destination");

        // User(W/G) and UserId(W/G)

        user = getIntent().getExtras().getBoolean("user"); // true = Ward and false = Guardian

        /************************************/

        sourceLatLng = new LatLng(Double.parseDouble(source.split("@")[0]), Double.parseDouble(source.split("@")[1]));
        destinationLatLng = new LatLng(Double.parseDouble(destination.split("@")[0]), Double.parseDouble(destination.split("@")[1]));


        sourceLocation.setLatitude(Double.parseDouble(source.split("@")[0]));
        sourceLocation.setLongitude(Double.parseDouble(source.split("@")[1]));

        destinationLocation.setLatitude(Double.parseDouble(destination.split("@")[0]));
        destinationLocation.setLongitude(Double.parseDouble(destination.split("@")[1]));


    }

      public void confirmCloser()
    {

        final Dialog dialog = new Dialog(SafeWalk.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dailog_without_image);
        TextView txtMiddleHeadingW = (TextView) dialog.findViewById(R.id.txtMiddleHeadingW);
        txtMiddleHeadingW.setText("Do you want to stop safer walk ?");
        dialog.show();

        RelativeLayout rlDialogAcceptW = (RelativeLayout) dialog.findViewById(R.id.rlDialogAcceptW);
        rlDialogAcceptW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                lastOperation();

            }
        });

        RelativeLayout rlDialogDeclineW = (RelativeLayout) dialog.findViewById(R.id.rlDialogDeclineW);
        rlDialogDeclineW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
    }

    @Override
    public void onBackPressed() {

        confirmCloser();

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver);
        unregisterReceiver(stopRcvr);
        unregisterReceiver(updateGuardianMap);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //********** Logging ********//

        StaticMaster.logsList.add("SaferWalk is starting up@"+StaticMaster.getCurrentTimeStamp());

        //**************************//

        ((NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE)).cancelAll(); // Cancel any previous notifications

        SharedPrefrenceStorage.StoreRunningActivity(getApplicationContext(), "SafeWalk");
        registerReceiver(mMessageReceiver, new IntentFilter("UserLocationUpdate"));
        registerReceiver(stopRcvr, new IntentFilter("StopSaferWalk"));
        registerReceiver(updateGuardianMap, new IntentFilter("UpdateGuardianMap"));
    }

    public void lastOperation(){

        stopAllOperations();

        final ProgressDialog progressDialog = ProgressDialog.show(SafeWalk.this, "Stopping SaferWalk", "Please wait....", true);

        StaticMaster.CurrentState = StaticMaster.States.Normal;
        String url = Networking.CommonUrl+"safewalk/"+SharedPrefrenceStorage.GetSaferWalkCaseid(getApplicationContext())+"/close";
        Volley.newRequestQueue(getApplicationContext()).add(new JsonObjectRequest(Request.Method.PUT, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject object) {

                Log.w(StaticMaster.SafeWalkLog, object.toString());

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                progressDialog.dismiss();

                SharedPrefrenceStorage.StoreRunningActivity(getApplicationContext(), null);

                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                progressDialog.dismiss();

                SharedPrefrenceStorage.StoreRunningActivity(getApplicationContext(), null);

                finish();

                Networking.commonVolleyLogs(volleyError, StaticMaster.SafeWalkLog);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return StaticMaster.commonHttpHeaders(getApplicationContext());
            }

        });
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //********** Logging ********//

            StaticMaster.logsList.add("Ward's current location is recieved@"+StaticMaster.getCurrentTimeStamp());

            //**************************//

            SharedPrefrenceStorage.StoreVictimCoordinates(getApplicationContext(), SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()));

            coordList.add(SharedPrefrenceStorage.GetVictimCoordinates(getApplicationContext()));

            updateCoordinates();
            updateUserLocation();

        }
    };

    private BroadcastReceiver stopRcvr = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {

            Toast.makeText(getApplicationContext(), "User Location is updated", Toast.LENGTH_SHORT).show();

            Dialog dialog = StaticMaster.getDialogIntance(SafeWalk.this, (short)0, topName+" has stooped the Safer Walk");

            ((TextView)dialog.findViewById(R.id.yes)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    stopAllOperations();

                    Intent noIntent = new Intent(context, HomeActivity.class);
                    noIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(noIntent);
                    finish();

                }
            });

             dialog.show();
        }
    };

    private BroadcastReceiver updateGuardianMap = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            Log.w(StaticMaster.SafeWalkLog, "Updating Ward Location");

            coordList.add(SharedPrefrenceStorage.GetVictimCoordinates(getApplicationContext()));
            updateCoordinates();

        }
    };


    public void stopAllOperations(){

        timer.cancel();
        updateTimer.cancel();
    }

      public void updatePolyLine()
    {
        polylineOptions = new PolylineOptions();

        if(polylineOptions != null) // update the Polyline Options
        {
            Log.w("Polyoptions", "Not Null");

            if(coordList.size() > 0)
            {
                for(String str : coordList)
                {

                    String[] coordinates = str.split("@");
                    polylineOptions.add(new LatLng(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1])));
                    polylineOptions.color(Color.parseColor("#900084B4"));

                }
            }

        }else {

            Log.w("Polyoptions", "Null");

        }

        polyline = mMap.addPolyline(polylineOptions);


    }

      public void updateCoordinates()
    {

        // Clear the previous map

        mMap.clear();

        Log.w(StaticMaster.SafeWalkLog, "Updating Map Markers");


        //********** Logging ********//

        StaticMaster.logsList.add("Updating location markers on new location@"+StaticMaster.getCurrentTimeStamp());

        //**************************//


        // Update the current location of Ward/Guardian

        Location l = new Location("");

        if(SharedPrefrenceStorage.GetVictimCoordinates(getApplicationContext()) != null)
        {
            String data[] = SharedPrefrenceStorage.GetVictimCoordinates(getApplicationContext()).split("@");

            LatLng currentCoord = new LatLng(Double.parseDouble(data[0]), Double.parseDouble(data[1]));

            l.setLatitude(currentCoord.latitude);
            l.setLongitude(currentCoord.longitude);


            mMap.addMarker(new MarkerOptions().position(currentCoord).title("Home").icon(BitmapDescriptorFactory.fromBitmap(StaticMaster.getCustomMapPointer(getApplicationContext(), victimImage, null)))); // add victim
        }


         if(l.distanceTo(sourceLocation) > 1000)
        {

            mMap.addMarker(new MarkerOptions().position(sourceLatLng).title("Home").icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.s)))); // add victim

        }

    //

        // Update the "Camera Zoom"
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sourceLatLng, 15.0f), 3000, null);


        // ******* Update Destination Location *********** //

        mMap.addMarker(new MarkerOptions().position(destinationLatLng).title("Home").icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.d)))); // add victim

        //********************************************//

           if(destinationLocation.distanceTo(l) <= 100)
        {
            Toast.makeText(getApplicationContext(), "SafeWalk is complete !!", Toast.LENGTH_SHORT).show();
            lastOperation();
        }

        Log.w(StaticMaster.SafeWalkLog, "Distance between Source and Destination"+sourceLocation.distanceTo(destinationLocation));

        updatePolyLine(); // draw on the polyline still in testing
    }


    public Bitmap roundMyPhoto(Bitmap bPhoto)
    {
        Bitmap bCirclePhoto=Bitmap.createBitmap(bPhoto.getWidth(),bPhoto.getHeight(),Bitmap.Config.ARGB_8888);
        BitmapShader shader=new BitmapShader(bPhoto, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint=new Paint();
        paint.setShader(shader);
        paint.setAntiAlias(true);
        Canvas c=new Canvas(bCirclePhoto);
        c.drawCircle(bPhoto.getWidth()/2,bPhoto.getHeight()/2,bPhoto.getWidth()/2, paint);
        return bCirclePhoto;
    }

    @Override
    public View getInfoWindow(Marker marker) {

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.map_info_window, null);

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {



        return null;
    }

     /** Creating Coordinates Json String */

    private JSONObject data = new JSONObject();
    private String[] coordinates;
    private LatLng latLng;
    private int count = 0;


      public void updateUserLocation()
    {
        JSONObject jsonToSend = getUserCoordinates();

        Log.w(StaticMaster.SafeWalkLog, "Json To Send "+jsonToSend.toString());


        //********** Logging ********//

        StaticMaster.logsList.add("Forwarding ward's location SaferWalk guardian@"+StaticMaster.getCurrentTimeStamp());

        //**************************//

        Volley.newRequestQueue(getApplicationContext()).add(new JsonObjectRequest(Request.Method.PUT, Networking.CommonUrl+"safewalk/"+SharedPrefrenceStorage.GetActivityId(getApplicationContext())+"/location", jsonToSend, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {


                //********** Logging ********//

                StaticMaster.logsList.add("Response is received successfully@"+StaticMaster.getCurrentTimeStamp());

                //**************************//

                Log.w(StaticMaster.SafeWalkLog, "Update coords json response "+jsonObject.toString());


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                Networking.commonVolleyLogs(volleyError, StaticMaster.SafeWalkLog);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                return StaticMaster.commonHttpHeaders(getApplicationContext());
            }

        });
    }

    public JSONObject getUserCoordinates(){

        try
        {

            if(StaticMaster.SaferWalkSimulation == true)
            {
                if(count < StaticMaster.masterCordList.size())
                {
                    latLng = StaticMaster.masterCordList.get(count);
                    count = count + (StaticMaster.masterCordList.size() - count < 30 ? (StaticMaster.masterCordList.size()-count) : 30);
                    source = latLng.latitude+"@"+latLng.longitude;
                    coordinates = source.split("@");
                    coordList.add(source);

                    Log.w(StaticMaster.SafeWalkLog, "LatLng --- "+source);

//                    Toast.makeText(getApplicationContext(), "SafeWalkUpdated", Toast.LENGTH_SHORT).show();
                }

            }else {

                coordinates = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");
            }


            Log.w(StaticMaster.SafeWalkLog, "CordListArraySize --- "+StaticMaster.masterCordList.size());
            Log.w(StaticMaster.SafeWalkLog, "count --- "+count);

            data.put("lat", coordinates[0]);
            data.put("lng", coordinates[1]);

        }catch (JSONException e){

            Log.w(StaticMaster.SafeWalkLog, "Json Format Error (User Coordinates) "+e.getMessage() != null ? e.getMessage() : "No Message");
        }

        return data;
    }

    /**************************************/

}