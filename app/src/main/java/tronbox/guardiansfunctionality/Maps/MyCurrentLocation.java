package tronbox.guardiansfunctionality.Maps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.Timer;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;


public class MyCurrentLocation extends FragmentActivity implements GoogleMap.InfoWindowAdapter{
    GoogleMap mMap;
    Timer timer;
    boolean flip = false;
    Polyline polyline;
    PolylineOptions polylineOptions;

    String TAG = "PlottingMap";

    TextView sosTitle;
    LinearLayout bottomView;

    Typeface robotoFont;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_location);

        robotoFont = Typeface.createFromAsset(getAssets(), "roboto.ttf");


        sosTitle = (TextView)findViewById(R.id.sos_title);
        sosTitle.setText("My Location");
        sosTitle.setTypeface(robotoFont);

        SupportMapFragment gMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.googleMap));
        mMap = gMap.getMap();

    }
    @Override
    public void onResume() {
        super.onResume();

        updateCoordinates();
    }


      public void updateCoordinates()
    {
        String[] coordinates = null;
        LatLng location;

        mMap.clear();

        coordinates = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");
        location = new LatLng(Double.parseDouble(coordinates[0]), Double.parseDouble(coordinates[1]));

        mMap.addMarker(new MarkerOptions().position(location).title("Home").icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.pin)))); // add victim


        if(flip == false)
        {
            flip = true;

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 14.0f), 5000, null);

        }


    }


    @Override
    public View getInfoWindow(Marker marker) {

        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.map_info_window, null);

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {



        return null;
    }
}
