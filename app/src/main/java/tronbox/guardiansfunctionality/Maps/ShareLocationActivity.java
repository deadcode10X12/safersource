package tronbox.guardiansfunctionality.Maps;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.HandleGuardians.GuardianAlertList;
import tronbox.guardiansfunctionality.Home.CircleImagePhoto;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 4/7/2015.
 * Showing Guardian Activity List and User current location
 */
public class ShareLocationActivity extends FragmentActivity implements LocationListener
{

    GoogleMap googleMap;
    GuardianAlertList GAListAdapter;
    Context myContext=ShareLocationActivity.this;
    TextView txtGuardianAlertLocation;
    ImageView backActivity;


    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_location);

        TextView name = (TextView)findViewById(R.id.txtGAName);
        TextView mobile = (TextView)findViewById(R.id.txtGADistance);

        ImageView photo = ((CircleImagePhoto)findViewById(R.id.imgGAPhoto));



        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if(status!= ConnectionResult.SUCCESS)
        {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }else {

            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            googleMap = fm.getMap();
            googleMap.setMyLocationEnabled(true);

              if(SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()) != null)
            {
                String coord[] = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");
                Location location1 = new Location("d");
                location1.setLatitude(Double.parseDouble(coord[0]));
                location1.setLongitude(Double.parseDouble(coord[1]));

                onLocationChanged(location1);
            }
        }

        backActivity = (ImageView)findViewById(R.id.share_location_back);
        backActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), HomeActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);

            }
        });

        if(getIntent().getExtras() != null)
        {
            Bundle bundle = getIntent().getExtras();

            if(bundle.containsKey("lat") && bundle.containsKey("lng") && bundle.containsKey("id"))
            {
                Location location = new Location("");
                location.setLatitude(Double.parseDouble(bundle.getString("lat")));
                location.setLongitude(Double.parseDouble(bundle.getString("lng")));

                GuardianDetails guardianDetails = null;

                for(GuardianDetails details : StaticMaster.database(getApplicationContext()).getAllGuardians())
                {
                     if(details.getId().equals(bundle.getString("id")))
                    {
                        guardianDetails = details;
                    }
                }

                for(GuardianDetails details : StaticMaster.database(getApplicationContext()).getAllWard())
                {
                    if(details.getId().equals(bundle.getString("id")))
                    {
                        guardianDetails = details;
                    }
                }


                if(guardianDetails != null){

                    name.setText(guardianDetails.getName());
                    mobile.setText(guardianDetails.getMobile());

                    try {

                        photo.setImageBitmap(BitmapFactory.decodeStream(openFileInput(guardianDetails.getId())));

                    } catch (Exception e) {

                        photo.setImageBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.face));

                    }

                }


                updateCoordinates(location);

            }

        }

    }
    @Override
    public void onLocationChanged(Location location) {


    }

     public void updateCoordinates(Location location)
    {

        txtGuardianAlertLocation = (TextView) findViewById(R.id.txtGuardianAlertLocation);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        googleMap.addMarker(new MarkerOptions().position(latLng));
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

        //txtGuardianAlertLocation.setText("Latitude:" + latitude + ", Longitude:" + longitude);
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                txtGuardianAlertLocation.setText(strReturnedAddress.toString());
            }
            else{
                txtGuardianAlertLocation.setText("No Address identified!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            txtGuardianAlertLocation.setText("Cannot get Address!");
        }

    }

    @Override
    public void onProviderDisabled(String provider) {
    }
    @Override
    public void onProviderEnabled(String provider) {
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}