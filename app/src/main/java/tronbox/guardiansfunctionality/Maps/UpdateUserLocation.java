
/*

 The goal of this class is :-

 1) Get the Location object from the incoming intent and reverse geocode it.
 2) Store the address locally and update it on the server.

*/

package tronbox.guardiansfunctionality.Maps;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import com.android.volley.Request;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class UpdateUserLocation extends IntentService {

    private Geocoder geocoder;
    private Location location;

    public UpdateUserLocation() {
        super("UpdateUserLocation");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent != null && intent.getExtras() != null) {

            location = (Location) intent.getExtras().get("location");
            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

            if (location != null && geocoder != null) {

                // Brodcast the update for other services

                sendBroadcast(new Intent("UserLocationUpdate"));

                // Store the coordinates locally

                SharedPrefrenceStorage.StoreUserCoordinates(getApplicationContext(), new LatLng(location.getLatitude(), location.getLongitude()));

                // Store the coordinates on server

                StaticMaster.sendCommonData(getApplicationContext(), "Update_Location_ActivityId", "http://54.169.182.72/api/v0/user/location", Request.Method.POST);

                List<Address> addresses = null;

                try {
                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

                    if (addresses != null && addresses.size() > 0) {

                        StringBuffer stringBuffer = new StringBuffer();

                        for (Address address : addresses) {


                            if(address.getLocality() != null)
                            {
                                SharedPrefrenceStorage.StoreUserLocality(getApplicationContext(), address.getLocality());

                                Log.w(StaticMaster.UserActivity, address.getSubLocality());

                            }

                             if(address.getSubLocality() != null)
                            {
                                SharedPrefrenceStorage.StoreUserSubLocality(getApplicationContext(), address.getSubLocality());

                                Log.w(StaticMaster.UserActivity, address.getSubLocality());

                            }


                            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                Log.w("AddressDecipher", address.getAddressLine(i));
                                stringBuffer.append(address.getAddressLine(i) + " ");
                            }

                        }

                        Log.w(StaticMaster.UserActivity, "User new address "+stringBuffer.toString());



                        // Store the reverse geocode address locally

                        SharedPrefrenceStorage.StoreUserAddress(getApplicationContext(), stringBuffer.toString());

                    }

                } catch (IOException  | IllegalArgumentException e) {

                }

            }
        }

    }

}
