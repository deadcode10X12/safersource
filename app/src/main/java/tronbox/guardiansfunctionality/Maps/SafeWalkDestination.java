package tronbox.guardiansfunctionality.Maps;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by nikhil on 4/8/15.
 */


public class SafeWalkDestination extends Fragment{

    private String regularExpression = "^[a-z+ ]*$";
    private String TAG = "QueryChecker";

    public destinationCallback callback;

    Typeface robotoFont;

    public interface destinationCallback {

        public void choosenDestination(LatLng latLng, String value);

    }

    ProgressDialog progressDialog;
    private String address;

    private boolean logBool1 = false, logBool2 = false;

    private HashMap<String, String> temptags = new HashMap<>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

            callback = (destinationCallback)activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.safe_walk_destination, container, false);


        robotoFont = Typeface.createFromAsset(getActivity().getAssets(), "roboto.ttf");

        final EditText chooseDestination = (EditText)view.findViewById(R.id.safe_walk_etxt_dest);
        chooseDestination.setTypeface(robotoFont);

        final ListView predicationsListView = (ListView) view.findViewById(R.id.safe_walk_predictions);

        final List<String> predictions = new ArrayList<>();

         for(String d : StaticMaster.database(getActivity().getApplicationContext()).getAllDestinations())
        {
            Log.w(TAG, d);

             if(d.contains("@"))
            {
                String[] sub = d.split("@");

                  if(sub.length > 1 && sub[1] != null && sub[1].length() > 1)
                {
                    temptags.put(sub[1], sub[0]);
                    predictions.add(sub[1]);
                }

            }else {

                 predictions.add(d);
             }

        }

        final TextViewAdapter arrayAdapter = new TextViewAdapter(getActivity(), R.layout.safe_walk_predictions_tv, predictions);

        predicationsListView.setAdapter(arrayAdapter);


        chooseDestination.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

                //********** Logging ********//

                 if(logBool1 == false)
                {
                    logBool1 = true;
                    StaticMaster.logsList.add("User is entering the destination@"+StaticMaster.getCurrentTimeStamp());
                }

                //**************************//


                String str;

                if(charSequence.length() > 2){

                    predictions.clear();
                    arrayAdapter.notifyDataSetChanged();

                    if(charSequence.toString().contains(" ")){

                        Log.w(TAG, "space_removed");

                        str = charSequence.toString().replace(' ','+');
                    }else {
                        Log.w(TAG, "no space yet");
                        str = charSequence.toString();
                    }

                    if(Pattern.compile(regularExpression).matcher(str).matches()){

                        if(str.charAt(str.length()-1) == '+'){

                            str = str.substring(0,str.length()-2);

                        }

                        Log.w(TAG, "query is perfect "+str);

                        String url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=establishment&location=28.613333,77.208333&radius=500&key=AIzaSyDVmRv9bPH3JG7tUWpBl7BMBrJ8g0PGTWo&input="+str;

                        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject obj) {

                                try {

                                    JSONArray results = obj.getJSONArray("predictions");


                                    for(int i=0; i<results.length(); i++)
                                    {

                                        JSONObject o1 = results.getJSONObject(i);

                                        String value = o1.getString("description");

                                        //********** Logging ********//

                                        if(logBool2 == false)
                                        {
                                            logBool2 = true;
                                            StaticMaster.logsList.add("Receiving user's nearby destination@"+StaticMaster.getCurrentTimeStamp());
                                        }

                                        //**************************//

                                        predictions.add(value);
                                        arrayAdapter.notifyDataSetChanged();

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {

                            }
                        });


                        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                        requestQueue.add(jsObjRequest);
                        requestQueue.start();


                    }else {

                        Log.w(TAG, "query is not perfect "+str);

                    }


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        predicationsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.w(StaticMaster.SafeWalkLog, "Destination Choosen "+chooseDestination.getText());

                address = parent.getAdapter().getItem(position).toString();

                if(temptags.size() > 0)
                {
                   address   = temptags.get(address);
                }


                chooseDestination.setText(address);


                //********** Logging ********//

                 StaticMaster.logsList.add("User has selected a destination, retrieving address@"+StaticMaster.getCurrentTimeStamp());

                //**************************//

                progressDialog = ProgressDialog.show(getActivity(), "Loading Destination", "Please wait....", true);

                //********** Logging ********//

                StaticMaster.logsList.add("Progress Dialog is running@"+StaticMaster.getCurrentTimeStamp());

                //**************************//


                String url = "https://maps.googleapis.com/maps/api/geocode/json?address="+address.replace(" ", "+")+"&key=AIzaSyDVmRv9bPH3JG7tUWpBl7BMBrJ8g0PGTWo";


                Volley.newRequestQueue(getActivity()).add(new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject object) {

                        try {

                            JSONArray array = object.getJSONArray("results");

                              for(int i=0; i<array.length(); i++)
                            {
                                JSONObject jsonObject = array.getJSONObject(i).getJSONObject("geometry").getJSONObject("location");

                                LatLng latLng = new LatLng(Double.parseDouble(jsonObject.getString("lat")), Double.parseDouble(jsonObject.getString("lng")));

                                callback.choosenDestination(latLng, address);
                            }

                            //********** Logging ********//

                            StaticMaster.logsList.add("User selected destination is geocoded successfully@"+StaticMaster.getCurrentTimeStamp());

                            //**************************//


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                          finally {


                             if(progressDialog != null)
                            {
                                progressDialog.dismiss();
                            }

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Networking.commonVolleyLogs(volleyError, StaticMaster.SafeWalkLog);

                        //********** Logging ********//

                        StaticMaster.logsList.add("Geocoding error in response @"+StaticMaster.getCurrentTimeStamp());

                        //**************************//


                        if(progressDialog != null)
                        {
                            progressDialog.dismiss();
                        }
                    }
                }));

            }
        });


        return view;
    }

    class TextViewAdapter extends ArrayAdapter<String>{

        List<String> list;
        int resource;
        Context context;

        public TextViewAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
            this.list = objects;
            this.resource = resource;
            this.context = context;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            TextView tv = (TextView)getActivity().getLayoutInflater().inflate(resource,parent, false);
            tv.setText(list.get(position));
            tv.setTypeface(robotoFont);

            return tv;
        }
    }




}
