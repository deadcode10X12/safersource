package tronbox.guardiansfunctionality.Maps;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

public class VictimLocationUpdate extends AsyncTask<Void, Void, ArrayList<String>> {

	private Context context;

	public VictimLocationUpdate(Context context) {
		this.context = context; 
	}

	@Override
	protected ArrayList<String> doInBackground(Void... arg0) {


/*
    Get Coordinates from the server and store in a list
*/
        ArrayList<String> coordList = new ArrayList<String>();
        coordList.add("28.6698"+"@"+"77.2667");
        coordList.add("28.667489"+"@"+"77.228045");
        coordList.add("28.6473"+"@"+"77.3161");


		return coordList;
	}
	@Override
	protected void onPostExecute(ArrayList<String> result) {

/*
    Store Coordinates in the memory
*/


        Set<String> list = new HashSet<String>();

          for(int i=0; i<result.size(); i++)
        {
            list.add(result.get(i));
        }

        SharedPrefrenceStorage.StoreGuardianCoordinates(context, list);

        context.sendBroadcast(new Intent("Guardians_Update"));
	}



}
