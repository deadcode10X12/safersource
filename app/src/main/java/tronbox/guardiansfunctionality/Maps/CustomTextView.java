package tronbox.guardiansfunctionality.Maps;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by nikhil on 5/16/15.
 */

public class CustomTextView extends TextView{

    Context context;

    public CustomTextView(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public void setTypeface(Typeface tf) {


        tf = Typeface.createFromAsset(context.getAssets(), "roboto.ttf");

        super.setTypeface(tf);
    }
}
