package tronbox.guardiansfunctionality;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SimpleTimeZone;

import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.HandleGuardians.InvitePendingGuardians;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.Login.DeviceSyncActivity;
import tronbox.guardiansfunctionality.Login.DummyGuardianListActivity;
import tronbox.guardiansfunctionality.Login.MobileVerificationCodeActivity;
import tronbox.guardiansfunctionality.Login.SendCommonDataServer;
import tronbox.guardiansfunctionality.Networking.Networking;

public class StaticMaster {


    /******* Log's Variables ******/

    public static String SharedLocationLogs = "SharedLocationLogs";
    public static String log = "GuardianProgress";
    public static String addGuardian = "AddGuardianProgress";
    public static String downloadArchive = "DownloadGuardiansData";
    public static String ServiceLogs = "SaferServiceLogs";
    public static String SafeWalkLog = "SafeWalkLogs";
    public static String SOSLogs = "SaferSOSLogs";
    public static String LoginScreenLogs = "LoginScreenLogs";

    public static String UserActivity = "UserActivityTracker";
    /****************************/

    /*********** SaferWalk *****************/

    // Home Screen Related Variables //

    public static Bitmap profilePic = null;

    //***** SaferWalk Related Variables *****//

    public static enum Category { Guardian, Ward, Both }

    public static boolean SaferWalkSimulation = false;

    public static ArrayList<String> logsList = new ArrayList<String>();

      public static String getCurrentTimeStamp()
    {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);

        return cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND);
    }

    //************ GuardianBrain Service Related Varialbles ***************//


    public static enum States{Default, Normal, Distress, SafeWalk};

/*

    public static int sosNormalDuration = 10000;
    public static int sosDistressDuration = 30000;
    public static int safeWalkDuration = 30000;
    public static int sosDuration = sosNormalDuration;
*/

    public static States PreviousState = States.Normal;
    //public static States CurrentState = States.Normal;


    /*****************************************************/


    public static BluetoothGattService bluetoothGattService = null;

    public static Bitmap getCustomMapPointer(Context context, String userName, Bitmap bitmap)

    {
        Bitmap userBitmap = null,backBitmap = null;

        try {

             if(bitmap == null)
            {
                userBitmap = BitmapFactory.decodeStream(context.openFileInput(userName));

                Log.w(StaticMaster.SOSLogs, "Local Stream Bitmap");

            }else if(bitmap != null){

                 userBitmap = bitmap;

                 Log.w(StaticMaster.SOSLogs, "Bitmap Provided");
             }

            backBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.down_pointer);

        } catch (FileNotFoundException e) {


            Log.w(StaticMaster.SOSLogs, "Bitmap Not Found Dummy Bitmap");

            userBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.face);
        }



        Bitmap bitmap_1 = Bitmap.createBitmap(userBitmap.getWidth(), userBitmap.getHeight(),Bitmap.Config.ARGB_8888);
        new Canvas(bitmap_1).drawCircle(userBitmap.getWidth()/2, userBitmap.getHeight()/2, userBitmap.getWidth()/2, getPaint(userBitmap));

        Bitmap bitmap_2 = Bitmap.createBitmap(bitmap_1.getWidth()+6, bitmap_1.getHeight()+6, Bitmap.Config.ARGB_8888);

        Paint colorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        colorPaint.setStyle(Paint.Style.STROKE);
        colorPaint.setStrokeWidth(3);
        colorPaint.setColor(Color.parseColor("#0055b0"));


        Canvas cc = new Canvas(bitmap_2);
        cc.drawBitmap(bitmap_1,3,3,getPaint(bitmap_1));
        cc.drawCircle(bitmap_2.getWidth()/2, bitmap_2.getHeight()/2, bitmap_2.getWidth()/2-3, colorPaint);


        Bitmap finalBitmap = Bitmap.createBitmap(bitmap_2.getWidth(), bitmap_2.getHeight()+backBitmap.getHeight()/2, Bitmap.Config.ARGB_8888);

        Canvas ccc = new Canvas(finalBitmap);
        ccc.drawBitmap(backBitmap, bitmap_2.getWidth()/2 - backBitmap.getWidth()/2, bitmap_2.getHeight()-backBitmap.getHeight()/2, getPaint(backBitmap));
        ccc.drawBitmap(bitmap_2, 0, 0, getPaint(bitmap_2));

        return finalBitmap;
    }

       public static Paint getPaint(Bitmap bitmap)
    {
        Paint paint=new Paint();
        paint.setShader(new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        paint.setAntiAlias(true);

        return paint;
    }

    public static boolean flip = true;

    public static Typeface RobotoFont(Context context){

        return Typeface.createFromAsset(context.getAssets(), "roboto.ttf");
    }

   public static enum logs { MobileVerification, ServerResponse, GuardiansProcessing };

   public static List<ContactDetails> detailsArrayList;

    public static GoogleMap googleMap;

    public static String macAddress = null;

    public static String UserProfilePic = "UserProfilePic";
   public static HashSet<String> hashSet = new HashSet<String>();
   //public static LatLng latLng;
   public static Uri imageUri;
   public static LatLng cords;
    public static Location location;
   public static ArrayList<LatLng> masterCordList = new ArrayList<LatLng>();
   public static void Operations(Context context, String Message){


    }
   public static int i = 0;

   public static int temp = 0;

   public static ContactDetails tempContactDetails;
    public static GuardianDetails tempGuardianDetails = null;

    public static int sosNormalDuration = 60000;
    public static int sosDistressDuration = 20000;
    public static int safeWalkDuration = 5000;

    public static int sosDuration = sosNormalDuration;

    //public static enum States{Default, Normal, Distress, SafeWalk};
    public static States CurrentState = States.Default;

    public static HashMap<String, ContactDetails> tempGuardiansList = new HashMap<>();


    public static ArrayList<String> contactsList = new ArrayList<String>();

   public static String ActivityId;

   public static String tempData, mobile;

      public static SecurityParametres securityLevel(Context context)
    {

        final ArrayList<String> reasons = new ArrayList<String>();
        int level = 0;


        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        reasons.clear();

        /*********** Testing Essential Parametres *****/

         if(isBluetoothAvailable())
        {
            reasons.add("Bluetooth is ON");
        }else {
             reasons.add("Bluetooth Is Off");
         }

        if(isNetworkSignalsAvailable(context)){

            reasons.add("Network signals are present");
        }else {

            reasons.add("Network signals are not present");
        }

          if(isBluetoothAvailable() && isNetworkSignalsAvailable(context))
        {
            level = 100; // user is totally secure

            /*********** Testing Internet Parameters *****/

            SecurityParametres internetStateAndSource = internetStateAndSouce(context);

            if(internetStateAndSource.isConnected())
            {
                reasons.add("Internet is available via "+internetStateAndSource.getSource());
            }else {

                level = level - 40;
                reasons.add("Internet is not available");
            }

            /************* Testing GPS Parameters *******************/


            SecurityParametres gpsParametres = isGpsAvailable(context);

            if(gpsParametres.isLocationViaGps())
            {
                reasons.add("Location via GPS is available");

            }else if(gpsParametres.isLocationViaOthers()){

                level = level - 10;
                reasons.add("Location via other source is available");

            }else {

                level = level - 30;
                reasons.add("No location source is available");

            }


            /*************** Guardians Count Parameters ********/

            int numberOfGuardians = StaticMaster.database(context).getAllGuardians().size();

             if(numberOfGuardians < 3)
            {

                reasons.add("Your have low guardian count");
                level = level - 10;
            }

        }

        /********************************************/

         return new SecurityParametres(level, reasons);
    }


    public static boolean isBluetoothAvailable(){

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
    }

    public static boolean isNetworkSignalsAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
    }


    public static boolean isWifiAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static SecurityParametres internetStateAndSouce(Context context) {

        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        String source = null;

         if(isConnected)
        {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
            {
                source = "Mobile";
            }
            else if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
            {
                source = "Wifi";
            }
        }


        return new SecurityParametres(isConnected, source);
    }

      public static SecurityParametres isGpsAvailable(Context context)
    {
        LocationManager locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);


        boolean locationViaGps = false, locationViaOthers = false;

          if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            locationViaGps = true;
        }

        if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
        {
            locationViaOthers = true;
        }

        if(locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER))
        {
            locationViaOthers = true;
        }

        return new SecurityParametres(locationViaGps, locationViaOthers);
    }

      public static void getBatteryPct(Context context)
    {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

                float batteryPct = (level / (float) scale) * 100;

                Intent intent1 = new Intent("security_parameters");
                intent1.putExtra("battery_pct", batteryPct);
                context.sendBroadcast(intent1);

                context.unregisterReceiver(this);

            }
        }, intentFilter);

    }

    public static class SecurityParametres {

        private int level;
        private ArrayList<String> reasons;

        private boolean internet;
        private String source;

        private boolean locationViaGps, locationViaOthers, isBatteryLow;


        private float batteryPct = 0.0f;

        SecurityParametres(int level, ArrayList<String> reasons){
            this.level = level;
            this.reasons = reasons;
        }

        SecurityParametres(boolean internet, String source){
            this.internet = internet;
            this.source = source;
        }

        SecurityParametres(boolean locationViaGps, boolean locationViaOthers){
            this.locationViaGps = locationViaGps;
            this.locationViaOthers = locationViaOthers;
        }

        SecurityParametres(boolean isBatteryLow, float batteryPct){

            this.isBatteryLow = isBatteryLow;
            this.batteryPct = batteryPct;
        }



        public boolean isLocationViaGps() {
            return locationViaGps;
        }

        public boolean isLocationViaOthers() {
            return locationViaOthers;
        }

        public String getSource() {
            return source;
        }

        public boolean isConnected(){
            return internet;
        }

        public int getLevel() {
            return level;
        }

        public ArrayList<String> getReasons() {
            return reasons;
        }
    }

     public static void sendCommonData(Context context, String cmd, String url, int requestMethod)
    {
        try {

            MobileDatabase database = new MobileDatabase(context, "GUARDIAN", null, 1);

            JSONObject post_data = new JSONObject();
            String dataToBeSent = null;

            switch (cmd) {

                case "NewUser": {

                    post_data.put("primaryContact", SharedPrefrenceStorage.GetMobile(context)); // 9582152153 - motoG, 9899972269-ayush
                    dataToBeSent = post_data.toString();

                    Log.w("JsonDataaaaaaaa", String.valueOf(post_data));

                }
                break;

                case "UserVerify": {

                    post_data.put("primaryContact", SharedPrefrenceStorage.GetMobile(context));
                    post_data.put("verificationCode", SharedPrefrenceStorage.GetVerificationCode(context));

                    Networking.DownloadArchive(context, url, post_data, requestMethod, cmd);

                    dataToBeSent = post_data.toString();

                    Log.w("JsonData", String.valueOf(post_data));

                    return;
                }

                case "GcmId":{

                    post_data.put("deviceType", "android");
                    post_data.put("token", SharedPrefrenceStorage.GetRegId(context));
                    dataToBeSent = post_data.toString();


                }break;

                case "AddGuardians":{

                    try {

                        Log.d(StaticMaster.log, "Sending guardian addition request");

                        JSONArray jsonArray1 = new JSONArray(); // 9582152153 - motoG, 9899972269-ayush, sis = 9873252281, guardian-2 = 9933350989

                          for(ContactDetails details : StaticMaster.detailsArrayList)
                        {
                            JSONObject tempJson = new JSONObject();

                            String temp = details.getMobile().replace(" ", "").replace("-", "").replace("(", "").replace(")", ""); // mobile
                            String mobile = temp.substring(temp.length()-10, temp.length());

                            tempJson.put("n",mobile);
                            tempJson.put("a",details.getName());

                            jsonArray1.put(tempJson);

                        }

                        post_data.put("guardians", jsonArray1);
                        dataToBeSent = post_data.toString();

                        Networking.AddGuardians(context,url,post_data,requestMethod,cmd);

                        return;

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }break;

                case "Start_Safewalk":{

                    try {

                        JSONArray jsonArray1 = new JSONArray();
                        String[] data = SharedPrefrenceStorage.GetUserCoordinates(context).split("@"); // enter the source
                        jsonArray1.put(data[0]);
                        jsonArray1.put(data[1]);


                        JSONArray jsonArray2 = new JSONArray();
                        jsonArray2.put(String.valueOf(StaticMaster.cords.latitude));
                        jsonArray2.put(String.valueOf(StaticMaster.cords.longitude));

                        JSONArray jsonArray3 = new JSONArray();

                        for(ContactDetails guardianDetails : StaticMaster.detailsArrayList)
                        {
                            jsonArray3.put(guardianDetails.getId());

                            Log.w("GuardianYoYo", guardianDetails.getName()+" "+guardianDetails.getId()+" "+guardianDetails.getMobile());
                        }

                        post_data.put("actors", jsonArray3);
                        post_data.put("dst", jsonArray2);
                        post_data.put("src", jsonArray1);

                        dataToBeSent = post_data.toString();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }break;

                case "Send_SOS":{

                     if(SharedPrefrenceStorage.GetUserCoordinates(context) != null)
                    {

                        String[] coordinates = SharedPrefrenceStorage.GetUserCoordinates(context).split("@");

                        post_data.put("lat", coordinates[0]);
                        post_data.put("lng", coordinates[1]);

                        dataToBeSent = post_data.toString();

                    }else {
                         dataToBeSent = null;
                     }


                }break;

                case "Add_Profile":{

                    post_data.put("name", SharedPrefrenceStorage.GetName(context));
                    post_data.put("email", SharedPrefrenceStorage.GetEmail(context));
                    post_data.put("gender", SharedPrefrenceStorage.GetGender(context));
                    post_data.put("photo", SharedPrefrenceStorage.GetProfilePic(context));

                    dataToBeSent = post_data.toString();

                }break;

                case "Update_Location_ActivityId":{

                    String[] coordinates = SharedPrefrenceStorage.GetUserCoordinates(context).split("@");

                    post_data.put("lat", coordinates[0]);
                    post_data.put("lng", coordinates[1]);

                    dataToBeSent = post_data.toString();

                }break;

                case "Register_Device": {

                    post_data.put("deviceID", StaticMaster.tempData);

                    Networking.addDevice(context, url, post_data, requestMethod, cmd);

                    return;
                }


                case "Send_Feedback": {

                    post_data.put("feedback", StaticMaster.tempData);
                    dataToBeSent = post_data.toString();

                }break;

            }

            Log.d(StaticMaster.log, cmd+" data "+dataToBeSent);

            Networking.launch(context,url,post_data, requestMethod, cmd);

            //new SendCommonDataServer(context).execute(url, dataToBeSent, httpType, type);

        }catch (JSONException e) {
            e.printStackTrace();
        }
    }

      public static void registerBroadcastReceiver(final Activity activity, final String filterName)
    {

        final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Intent intent1;

                 switch (filterName)
                {
                    case "MobileVerificationActivity":{



                  }break;

                    case "UserRegisterActivity":{

                        SharedPrefrenceStorage.StorePosition(activity, "DeviceSyncActivity");
                        intent1 = new Intent(activity, DeviceSyncActivity.class);
                        activity.startActivity(intent1);
                        activity.finish();
                        activity.unregisterReceiver(this);

                    }break;

                    case "ContactListActivity": {


                        SharedPrefrenceStorage.StorePosition(activity, "HomeActivity");
                        intent1 = new Intent(activity, HomeActivity.class);
                        activity.startActivity(intent1);
                        activity.finish();
                        activity.unregisterReceiver(this);

                    }break;

                }


            }
        };

        activity.registerReceiver(broadcastReceiver, new IntentFilter(filterName));
    }

      public static String correctNumber(String number)
    {


        return number;
    }

    public static MobileDatabase database(Context context){


        return new MobileDatabase(context, "GUARDIAN", null, 1);
    }

    public static void loadContacts(final String userId, final String userMobile, final String userName, final String userPhoto, final String userStatus, final String category, final FragmentActivity context){

         LoaderManager.LoaderCallbacks<Cursor> loaderManager = new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {

                Uri phone = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                String[] PHONE_PROJECTION = new String[]
                        {
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                                ContactsContract.CommonDataKinds.Phone.DATA1,
                                ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,
                                ContactsContract.CommonDataKinds.Phone._ID,
                        };
                CursorLoader loader = new CursorLoader(context, phone, PHONE_PROJECTION, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
                return loader;
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

                boolean flip = false;

                if(cursor.moveToFirst())
                {
                    do
                    {
                        if(cursor.getString(1).length() >= 10 && cursor.getString(1).length() <= 13)
                        {
                            String name = cursor.getString(0); //name
                            String temp = cursor.getString(1).replace(" ", "").replace("-", "").replace("(", "").replace(")", ""); // mobile
                            String mobile = temp.substring(temp.length()-10, temp.length());

                            String url = cursor.getString(2); // url
                            Log.w("LoadingContacts", name);


                            if(mobile != null && mobile.equals(userMobile) && flip == false)
                            {
                                flip = true;

                                 /*if(category.equals("guardian"))
                                {
                                    StaticMaster.database(context).insertGuardian(userId, name, userMobile, userPhoto, userStatus, "true");
                                }
                                   else if(category.equals("ward"))
                                 {
                                     StaticMaster.database(context).insertWard(userId, name, userMobile, userPhoto, userStatus);
                                 }*/

                                Log.w("LoadingContacts", name);
                            }
                        }
                    }while(cursor.moveToNext());
                }
            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {

            }
        };

         context.getSupportLoaderManager().initLoader(0, null, loaderManager);

    }

     public static void aliasManager(final String userId, final String userName , final String userMobile, final String userPhoto, final String userStatus, final String category,final Context context)
    {

        Uri phone = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] PHONE_PROJECTION = new String[]
                {
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.DATA1,
                        ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,
                        ContactsContract.CommonDataKinds.Phone._ID,
                };
        CursorLoader loader = new CursorLoader(context, phone, PHONE_PROJECTION, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC");

        loader.registerListener(1,new Loader.OnLoadCompleteListener<Cursor>() {
            @Override
            public void onLoadComplete(Loader<Cursor> loader, Cursor cursor) {

                boolean flip = false;
                String alias = null;

                if(cursor.moveToFirst())
                {
                    do
                    {
                        if(cursor.getString(1).length() >= 10 && cursor.getString(1).length() <= 13)
                        {
                            String name = cursor.getString(0); //name
                            String temp = cursor.getString(1).replace(" ", "").replace("-", "").replace("(", "").replace(")", ""); // mobile
                            String mobile = temp.substring(temp.length()-10, temp.length());

                            String url = cursor.getString(2); // url


                              if(mobile != null && mobile.equals(userMobile) && flip == false)
                            {
                                flip = true;
                                alias = name;

                            }
                        }
                    }while(cursor.moveToNext());
                }

                 if(alias == null)
                {
                    alias = userName;
                }

                Log.w("LoadingContacts", alias);


                if(category.equals("guardian"))
                {
                    StaticMaster.database(context).insertGuardian(userId, alias, userMobile, userPhoto, userStatus, "true");
                }
                else if(category.equals("ward"))
                {
                    StaticMaster.database(context).insertWard(userId, alias, userMobile, userPhoto, userStatus);
                }


            }
        });
        loader.startLoading();

    }

    public static Map<String, String> commonHttpHeaders(Context context){


        HashMap<String, String> params = new HashMap<String, String>();

        params.put("Content-Type", "application/json");

        if(SharedPrefrenceStorage.GetAuthToken(context) != null)
        {
            String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

            Log.d(StaticMaster.log, "AccessToken = "+accessToken);
            params.put("authorization", "bearer " + accessToken);
        }

        return params;
    }

    public static class MyDialogFragment extends DialogFragment {
        int mNum;


        public static MyDialogFragment newInstance(int num) {
            MyDialogFragment f = new MyDialogFragment();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("num", num);
            f.setArguments(args);

            return f;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mNum = getArguments().getInt("num");

            // Pick a style based on the num.
            int style = DialogFragment.STYLE_NORMAL, theme = 0;
            switch ((mNum-1)%6) {
                case 1: style = DialogFragment.STYLE_NO_TITLE; break;
                case 2: style = DialogFragment.STYLE_NO_FRAME; break;
                case 3: style = DialogFragment.STYLE_NO_INPUT; break;
                case 4: style = DialogFragment.STYLE_NORMAL; break;
                case 5: style = DialogFragment.STYLE_NORMAL; break;
                case 6: style = DialogFragment.STYLE_NO_TITLE; break;
                case 7: style = DialogFragment.STYLE_NO_FRAME; break;
                case 8: style = DialogFragment.STYLE_NORMAL; break;
            }
            switch ((mNum-1)%6) {
                case 4: theme = android.R.style.Theme_Holo; break;
                case 5: theme = android.R.style.Theme_Holo_Light_Dialog; break;
                case 6: theme = android.R.style.Theme_Holo_Light; break;
                case 7: theme = android.R.style.Theme_Holo_Light_Panel; break;
                case 8: theme = android.R.style.Theme_Holo_Light; break;
            }
            setStyle(style, theme);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View v = inflater.inflate(R.layout.fragment_dialog, container, false);

            return v;
        }
    }


    //******* Get SaferWalk Listener Details *****//

       public static GuardianDetails getSaferWalkListener(Context context, String userId)
    {

        MobileDatabase database = StaticMaster.database(context);

          for(GuardianDetails guardianDetails : database.getAllGuardians())
        {
              if(guardianDetails.getId().equals(userId))
            {

                return guardianDetails;
            }

        }

          for(GuardianDetails guardianDetails : database.getAllWard())
        {
              if(guardianDetails.getId().equals(userId))
            {

                return guardianDetails;
            }

        }

          return null;
    }

    //***************************************//

    // Dialog Instance

      public static Dialog getDialogIntance(Activity activity, short button, String message)
    {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        Typeface robotoFont = Typeface.createFromAsset(activity.getAssets(), "roboto.ttf");

        TextView heading = null;

         if(button == 0) // Single Button
        {

            dialog.setContentView(R.layout.dialog_single_button);
            heading = (TextView) dialog.findViewById(R.id.message);
        }
          else if(button == 1) // Double Button
         {

             dialog.setContentView(R.layout.dialog_double_button);
             heading = (TextView) dialog.findViewById(R.id.message);
         }


        heading.setTypeface(robotoFont);
        heading.setText(message);

        return dialog;
    }


}

