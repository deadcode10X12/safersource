package tronbox.guardiansfunctionality.Networking;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.Home.ActivityLifeCycleHandler;
import tronbox.guardiansfunctionality.Login.AppIdGetterId;
import tronbox.guardiansfunctionality.Login.CommonFunctions;
import tronbox.guardiansfunctionality.Login.DeviceSyncActivity;
import tronbox.guardiansfunctionality.Login.DummyGuardianListActivity;

import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;


public class Networking extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        //registerActivityLifecycleCallbacks(new ActivityLifeCycleHandler());
    }

    public static String CommonUrl = "http://54.169.182.72/api/v0/";

    public static boolean isRunning = false;

      public static void launch(final Context context, final String url, final JSONObject data, int requestMethod, final String cmd)
    {

         final String TAG = "GuardianProgress";
         final MobileDatabase database = new MobileDatabase(context, "GUARDIAN", null, 1);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(requestMethod, url, data, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject jsonObject) {

                    Log.d(StaticMaster.log, "ServerResponse "+jsonObject.toString());

                    try {

                        if(jsonObject.getString("status").equals("success"))
                        {

                            switch (cmd)
                            {
                                case "NewUser":{

                                    Log.w(TAG, "Number Sent For Verification");

                                      if(jsonObject.has("verificationCode"))
                                    {
                                        context.sendBroadcast(new Intent("MobileVerificationActivity")); // call back the Activity that new user request is sent to the user.

                                    }

                                }break;

                                case "UserVerify": {

                                    Log.w(StaticMaster.log, jsonObject.toString());

/*
                                    StaticMaster.tempData = "Verification_Over";

                                      if(jsonObject.has("accessCode"))
                                    {
                                        SharedPrefrenceStorage.StoreAuthToken(context, jsonObject.getString("accessCode"));
                                        Log.d("AccessTokennnn", SharedPrefrenceStorage.GetAuthToken(context));

                                        new AppIdGetterId(context).execute(); // Send GCM_Id


                                        Log.w(TAG, "Number is verified, and access token is stored");
                                    }

                                      if(jsonObject.has("user"))
                                    {
                                        JSONObject master = jsonObject.getJSONObject("user");

                                          if(master.has("name"))
                                        {

                                            String name = master.getString("name");
                                            String email = master.getString("email");
                                            String gender = master.getString("gender");
                                            String photo = master.getString("photo");

                                            SharedPrefrenceStorage.StoreName(context, name);
                                            SharedPrefrenceStorage.StoreEmail(context, email);
                                            SharedPrefrenceStorage.StoreGender(context, gender);

                                            if(photo != null)
                                            {
                                                SharedPrefrenceStorage.StoreProfilePic(context, photo);
                                                Networking.DownloadAndStoreImage(context,photo,StaticMaster.UserProfilePic);
                                            }

                                        }


                                        if(master.has("friends"))
                                        {
                                            Log.d("DatabaseCheck", "Has Friends");

                                            JSONArray array = master.getJSONArray("friends");

                                              if(array.length() > 0)
                                            {


                                                Log.d("DatabaseCheck", "Has Array");

                                                for(int i=0 ; i<array.length(); i++)
                                                {
                                                    JSONObject object = array.getJSONObject(i);

                                                    if(object.getString("category").equals("guardian"))
                                                    {
                                                        String status = (object.getBoolean("pending"))? "true" : "false";
                                                        String id = object.getString("id");


                                                        StaticMaster.sendCommonData(context, "Download_Guardian_Profile", "http://54.169.182.72/api/v0/guardian/"+id, Request.Method.GET);

                                                    }


                                                }
                                            }

                                        }

                                    }

                                    context.sendBroadcast(new Intent("Verification_Over"));
*/
                                }break;

                                case "GcmId":{


                                    Log.d(TAG, "GCM Id sent to the server");

                                }break;

                                case "Add_Profile":{

                                    context.sendBroadcast(new Intent("UserRegisterActivity"));

                                    Log.d(StaticMaster.log, "Profile is successfully stored on the server");

                                }break;

                                case "AddGuardians":{


                                 /*   Log.d(TAG, "Guardian request is sent let's wait for the reply");

                                    JSONArray array = jsonObject.getJSONArray("result");

                                    Log.w("GuardianDetails", "Length "+array.length());

                                       for(int i=0; i<array.length(); i++)
                                    {
                                        JSONObject ob = array.getJSONObject(i);

                                          if(ob.has("guardian_id"))
                                        {

                                            StaticMaster.sendCommonData(context, "Download_Guardian_Profile", "http://54.169.182.72/api/v0/guardian/"+ob.getString("guardian_id"), Request.Method.GET);

                                        }
                                    }*/

                                }break;

                                case "Download_Guardian_Profile": {

                                       if(jsonObject.has("guardian"))
                                    {
                                        JSONObject master = jsonObject.getJSONObject("guardian");

                                        String data[] = url.split("/");
                                        String id = data[data.length-1];
                                        String name = master.getString("name");
                                        String mobile = master.getString("primaryContact");
                                        String photo = master.getString("photo");


                                        database.insertGuardian(id, name, mobile, photo, "false", "true");

                                        GuardianDetails guardianDetails = database.getGuardianFromId(id);

                                        Log.d(StaticMaster.log+" Database", "Guardian Inserted To DB:- "+guardianDetails.getName()+" "+guardianDetails.getMobile()+" "+guardianDetails.getStatus());

                                        Networking.DownloadAndStoreImage(context, photo, id);

                                    }

                                    context.sendBroadcast(new Intent("ContactListActivity"));

                                }break;

                                case "Send_SOS":{


                                    if(jsonObject.has("id"))
                                    {
                                        StaticMaster.ActivityId = jsonObject.getString("id");
                                        SharedPrefrenceStorage.StoreActivityid(context, StaticMaster.ActivityId);
                                        Log.d("SOSId", StaticMaster.ActivityId);
                                    }

                                }break;

                                case "Start_Safewalk":{

                                    if(jsonObject.has("id"))
                                    {
                                        StaticMaster.ActivityId = jsonObject.getString("id");
                                        SharedPrefrenceStorage.StoreActivityid(context, StaticMaster.ActivityId);
                                        Log.d("SafewalkId", StaticMaster.ActivityId);
                                    }

                                }break;

                                case "Get_Location_ActivityId":{

                                    if(jsonObject.has("location"))
                                    {
                                        JSONObject object = jsonObject.getJSONObject("location");
                                        SharedPrefrenceStorage.StoreVictimCoordinates(context, object.getString("lat")+"@"+object.getString("lng"));

                                        context.sendBroadcast(new Intent("SafeWalk_Update"));
                                    }

                                }break;

                                case "Get_SOS_Data_ActivityId":{

                                        if(jsonObject.has("result"))
                                        {
                                            JSONObject master = jsonObject.getJSONObject("result");

                                            JSONArray array = master.getJSONArray("guardians");

                                            for(int i=0; i<array.length(); i++)
                                            {
                                                JSONObject temp = array.getJSONObject(i);

                                                String id = temp.getString("user_id");
                                                String name = temp.getString("name");
                                                String mobile = temp.getString("primaryContact");
                                                String photo = master.getString("photo");
                                                String location = null; // complete this

                                                  if(!StaticMaster.tempGuardiansList.containsKey(id))
                                                {
                                                    StaticMaster.tempGuardiansList.put(id, new ContactDetails(id, name, mobile, photo, location));
                                                    Log.d(TAG, "Guardian "+id+"_"+name+"_"+mobile);
                                                }

    //                                            database.insertTemp(id, name, mobile, photo, null);
                                            }

                                            JSONObject victim = master.getJSONObject("victim");

                                            String id = victim.getString("user_id");
                                            String name = victim.getString("name");
                                            String mobile = victim.getString("primaryContact");
                                            String lat = victim.getString("lat");
                                            String lng = victim.getString("lng");

                                            SharedPrefrenceStorage.StoreVictimCoordinates(context, lat+"@"+lng);

                                            context.sendBroadcast(new Intent("receive_updates"));

                                            Log.d(TAG, "Victim "+id+"_"+name+"_"+mobile+"_"+lat+"_"+lng);
                                        }

                                }break;

                                case "Register_Device" :{

                                    if(jsonObject.has("reason"))
                                    {
                                        String result = jsonObject.getString("reason");

                                        if(result.equals("deviceAlreadyRegistered"))
                                        {


                                        }

                                    }else { // this is a new device, register it

                                        SharedPrefrenceStorage.StoreMacAdd(context, StaticMaster.tempData);
                                    }

                                }break;

                                case "Forget_Device": {

                                    SharedPrefrenceStorage.StoreMacAdd(context, null);

                                }break;

                                case "Download_Ward_Profile":{

                                     if(jsonObject.has("ward"))
                                    {
                                        JSONObject master = jsonObject.getJSONObject("ward");

                                        String id = StaticMaster.tempData;
                                        String name = master.getString("name");
                                        String mobile = master.getString("primaryContact");
                                        String photo = master.getString("photo");

                                        database.insertWard(id,name,mobile,photo, "false");

                                        //StaticMaster.loadContacts(id,name,mobile,photo,"false","ward",context);

                                        //Networking.DownloadAndStoreImage(context, photo, id);

                                    }

                                }break;

                                case "Accept_Guardian_Request":{ // Since Guardian has accepted the request let's download the ward profile_pic permanently.

                                    Networking.DownloadAndStoreImage(context, StaticMaster.tempGuardianDetails.getImage(), StaticMaster.tempGuardianDetails.getId());

                                    database.updateWardStatus(StaticMaster.tempGuardianDetails.getId(), "false");

                                    Log.d(StaticMaster.log, "Storing the ward profile_pic locally");

                                }break;


                                case "Decline_Guardian_Request":{ // Since Guardian has accepted the request let's download the profile_pic permanently.

                                    database.deleteWard(StaticMaster.tempGuardianDetails.getId());

                                    Log.d(StaticMaster.log, "Remove the ward record from database");

                                }break;


                            }

                            if(jsonObject.has("user"))
                            {
                                JSONObject userObject = jsonObject.getJSONObject("user");

                                SharedPrefrenceStorage.StoreUserId(context, userObject.getString("_id"));
                                Log.w("User_Id", SharedPrefrenceStorage.GetUserId(context));
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError volleyError) {


                     if(volleyError != null && volleyError.getMessage() != null)
                    {
                          if(volleyError.getMessage().length() > 0)
                        {

                            Log.d(StaticMaster.log, volleyError.getMessage());
                        }
                    }


                }
            }){

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {


                    HashMap<String, String> params = new HashMap<String, String>();

                    params.put("Content-Type", "application/json");

                     if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                    {
                        String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                        Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                        params.put("authorization", "bearer " + accessToken);
                    }



                    return params;
                }




            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(jsObjRequest);
            requestQueue.start();

    }

    public static  void DownloadAndStoreImage(final Context context, final String url, final String name){

        Log.d(StaticMaster.log, "Sending CMD to downlaod the profile pic from an url " + url);

        Networking.isRunning = true;
        final RequestQueue queue = Volley.newRequestQueue(context);

        ImageRequest request = new ImageRequest(url,
                new com.android.volley.Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {

                        try {

                            bitmap = Networking.getCompressBit(bitmap);
                            FileOutputStream out1  = context.openFileOutput(name, Context.MODE_PRIVATE);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                            byte[] buffer = bos.toByteArray();
                            out1.write(buffer, 0, buffer.length);
                            out1.close();

                            Networking.isRunning = false;

                            Log.d(StaticMaster.log+"Image", name+" Image is downlaoded and stored internally");

                        } catch (FileNotFoundException e) {

                            Log.d(StaticMaster.log+"Image", name+" Image is downlaoded and stored internally");
                        } catch (IOException e) {

                            Log.d(StaticMaster.log+"Image", name+" Image is downlaoded and stored internally");
                        } catch (Exception e){

                            Log.d(StaticMaster.log+"Image", name+" Image is downlaoded and stored internally");
                        }


                    }
                }, 0, 0, null,
                new com.android.volley.Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                        String json = null;

                         if(error.networkResponse != null)
                        {

                            NetworkResponse response = error.networkResponse;

                            Log.d(StaticMaster.log+"Image", name+" Image Download Response :- "+error.networkResponse.toString());

                            if(response != null && response.data != null){
                                switch(response.statusCode){
                                    case 400:
                                        json = new String(response.data);
                                        if(json != null) Log.d(StaticMaster.log+"Image", name+" Image Download Error :- "+json);

                                        break;
                                }
                                //Additional cases
                            }
                        }


                    }
                });

        queue.add(request);


/*
        HandlerThread thread = new HandlerThread("Images");
        thread.start();
        new DownloadImageHandler(thread.getLooper(), UserRegisterActivity.this, url);
*/

    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 4;
            final int halfWidth = width / 4;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Context context, GuardianDetails holder, int reqWidth, int reqHeight, ImageView imageView) {

        Bitmap bitmap = null;

        try {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(context.openFileInput(holder.getId()), null, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        bitmap = BitmapFactory.decodeStream(context.openFileInput(holder.getId()), null, options);

        } catch (FileNotFoundException e) {

            imageView.setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.face));
            Networking.LiveStreamImage(context, holder.getImage(), imageView, holder.getId());
        }


        return bitmap;
    }

    public static Bitmap getCompressBit(Bitmap bitmap)
    {
        Log.w(StaticMaster.LoginScreenLogs, "Bitmap Bytes Before "+bitmap.getAllocationByteCount());

        final int IMAGE_SIZE = 85;
        boolean landscape = bitmap.getWidth() > bitmap.getHeight();
        float scale_factor;
        if (landscape) scale_factor = (float) IMAGE_SIZE / bitmap.getHeight();
        else scale_factor = (float) IMAGE_SIZE / bitmap.getWidth();
        Matrix matrix = new Matrix();
        matrix.postScale(scale_factor, scale_factor);
        Bitmap croppedBitmap;
        if (landscape){
            int start = (bitmap.getWidth() - bitmap.getHeight()) / 4;
            croppedBitmap = Bitmap.createBitmap(bitmap, start, 0, bitmap.getHeight(), bitmap.getHeight(), matrix, true);
        } else {
            int start = (bitmap.getHeight() - bitmap.getWidth()) / 4;
            croppedBitmap = Bitmap.createBitmap(bitmap, 0, start, bitmap.getWidth(), bitmap.getWidth(), matrix, true);

        }

        Log.w(StaticMaster.LoginScreenLogs, "Bitmap Bytes After "+croppedBitmap.getAllocationByteCount());

        return croppedBitmap;
    }

    public static  void LiveStreamImage(final Context context, final String url, final ImageView imageView, final String name){

        Log.d(StaticMaster.log, "Sending CMD to downlaod the profile pic from an url " + url);

        ImageRequest request = new ImageRequest(url,
                new com.android.volley.Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {

                      imageView.setImageBitmap(new CommonFunctions().imageCirclexClip(context, bitmap));

                        try{

                            FileOutputStream out1  = context.openFileOutput(name, Context.MODE_PRIVATE);
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                            byte[] buffer = bos.toByteArray();
                            out1.write(buffer, 0, buffer.length);
                            out1.close();

                        }catch (IOException e){

                            Log.d(StaticMaster.log, "Sending CMD to downlaod the profile pic from an url " + (e.getMessage() != null ? e.getMessage() : "No Message"));
                        }

                    }
                }, 0, 0, null,
                new com.android.volley.Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                        imageView.setImageBitmap(new CommonFunctions().imageCirclexClip(context, BitmapFactory.decodeResource(context.getResources(), R.drawable.face)));

                        Log.d(StaticMaster.log, "Image Download Error :- "+error.getMessage());

                    }
                });

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);

/*
        HandlerThread thread = new HandlerThread("Images");
        thread.start();
        new DownloadImageHandler(thread.getLooper(), UserRegisterActivity.this, url);
*/

    }

    public static HashMap<String, Bitmap> cacheBitmaps = new HashMap<>();

    public static  void LiveStreamMapImage(final Context context, final String url, final GoogleMap mMap, final LatLng location){

        Log.d(StaticMaster.log, "Sending CMD to downlaod the profile pic from an url " + url);

        Bitmap liveBitmap = null;

        final CommonFunctions commonFunctions = new CommonFunctions();

         if(url != null)
        {
              if(cacheBitmaps.containsKey("url"))
            {
                liveBitmap = cacheBitmaps.get(url);

                mMap.addMarker(new MarkerOptions().position(location).title("Home").icon(BitmapDescriptorFactory.fromBitmap(StaticMaster.getCustomMapPointer(context, null, liveBitmap))));

            }else {

                  Volley.newRequestQueue(context).add(new ImageRequest(url,
                          new com.android.volley.Response.Listener<Bitmap>() {
                              @Override
                              public void onResponse(Bitmap bitmap) {

                                  cacheBitmaps.put(url, bitmap);

                                  Log.w(StaticMaster.SOSLogs, "Networking Operation");

                                  BitmapDescriptor temp = BitmapDescriptorFactory.fromBitmap(StaticMaster.getCustomMapPointer(context,"",bitmap));
                                  mMap.addMarker(new MarkerOptions().position(location).title("Home").icon(temp));

                              }
                          }, 0, 0, null,
                          new com.android.volley.Response.ErrorListener() {
                              public void onErrorResponse(VolleyError error) {

                                  Log.d(StaticMaster.log, "Image Download Error :- "+error.getMessage());

                                  mMap.addMarker(new MarkerOptions().position(location).title("Home").icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.face))));

                              }
                          }));

              }

        }else {

             mMap.addMarker(new MarkerOptions().position(location).title("Home").icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.face))));

         }


/*
        HandlerThread thread = new HandlerThread("Images");
        thread.start();
        new DownloadImageHandler(thread.getLooper(), UserRegisterActivity.this, url);
*/

    }


    public static Bitmap getBitmapFromId(Activity activity, String id)
    {
        Bitmap bitmap = null;

        try {

            FileInputStream inputStream = activity.openFileInput(id);
            bitmap = BitmapFactory.decodeStream(inputStream);

        } catch (FileNotFoundException e) {

            bitmap = BitmapFactory.decodeResource(activity.getResources(), R.drawable.dummy_image);
        }

        return bitmap;
    }

    public static int count = 0;
    public static HashMap<String, String> downloadList = new HashMap<>();


    public static void DownloadArchive(final Context context, final String url, final JSONObject data, int requestMethod, final String cmd)
    {
        final MobileDatabase database = new MobileDatabase(context, "GUARDIAN", null, 1);
        Networking.downloadList.clear();
        Networking.count = 0;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(requestMethod, url, data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                Log.d(StaticMaster.logs.ServerResponse.name(), jsonObject.toString());

                try {

                    if(jsonObject.has("status") && jsonObject.getString("status").equals("success"))
                   {
                       if(jsonObject.has("accessCode"))
                       {
                           SharedPrefrenceStorage.StoreAuthToken(context, jsonObject.getString("accessCode"));
                           Log.d(StaticMaster.logs.MobileVerification.toString(), SharedPrefrenceStorage.GetAuthToken(context));
                           new AppIdGetterId(context).execute(); // Send GCM_Id

                             if(jsonObject.has("hasProfile") && jsonObject.getBoolean("hasProfile")) // user is already registered
                           {
                                  if(jsonObject.has("user"))
                               {
                                   JSONObject master = jsonObject.getJSONObject("user");

                                   String name = master.getString("name");
                                   String email = master.getString("email");
                                   String gender = master.getString("gender");
                                   String photo = master.getString("photo");

                                   SharedPrefrenceStorage.StoreName(context, name);
                                   SharedPrefrenceStorage.StoreEmail(context, email);
                                   SharedPrefrenceStorage.StoreGender(context, gender);

                                     if(photo != null)
                                   {
                                       SharedPrefrenceStorage.StoreProfilePic(context, photo);
                                       DownloadAndStoreImage(context, photo, StaticMaster.UserProfilePic);
                                   }

                                   Networking.DownloadGuardiansFromArchive(context);
                                   Networking.DownloadWardsFromArchive(context);


                               }
                           }
                              else {

                                 context.sendBroadcast(new Intent("MobileVerification"));

                             }

                       }
                   }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        });


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsObjRequest);
        requestQueue.start();


    }

    public static void AddMyGuardian(final Context context){

         try {

              if(StaticMaster.detailsArrayList != null && StaticMaster.detailsArrayList.size() > 0)
            {

                JSONObject data = new JSONObject();
                final JSONArray jsonArray1 = new JSONArray();

                  for(ContactDetails details : StaticMaster.detailsArrayList)
                {
                    JSONObject tempJson = new JSONObject();

                    String temp = details.getMobile().replace(" ", "").replace("-", "").replace("(", "").replace(")", ""); // mobile
                    String mobile = temp.substring(temp.length()-10, temp.length());

                    tempJson.put("n",mobile);
                    tempJson.put("a",details.getName());

                    jsonArray1.put(tempJson);

                }

                data.put("guardians", jsonArray1);

                Log.w(StaticMaster.addGuardian, "Json_Data :- "+data.toString());


                Volley.newRequestQueue(context).add(new JsonObjectRequest(Request.Method.POST, Networking.CommonUrl+"guardians", data, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        Log.w(StaticMaster.addGuardian, "Response :- "+jsonObject.toString());

                        try {

                              if(jsonObject.has("status") && jsonObject.getString("status").equals("success"))
                            {
                                String id = null, name = null, mobile = null, photo = null;

                                final JSONArray array = jsonObject.getJSONArray("result");

                                  for(int i=0; i<array.length(); i++)
                                {
                                    JSONObject temp = array.getJSONObject(i);

                                     if(temp.has("guardian_id") && temp.has("primaryContact"))
                                    {
                                        id = temp.getString("guardian_id");
                                        mobile = temp.getString("primaryContact");
                                        name = Networking.GetNames(mobile);

                                        if(temp.has("photo"))
                                        {
                                            photo = temp.getString("photo");
                                            Networking.DownloadAndStoreImage(context,photo,id);
                                        }

                                        StaticMaster.database(context).insertGuardian(id,name,mobile,photo,"false","false");

                                        Log.w(StaticMaster.addGuardian, name+" "+mobile+" "+id+" "+(photo != null ? photo : "Photo_Null"));
                                    }
                                }

                            }

                        }catch (JSONException e) {

                            Log.w(StaticMaster.addGuardian, "Json Format Error :- "+e.getMessage());

                        } finally {

                            context.sendBroadcast(new Intent("GuardiansProcessing"));

                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {


                        HashMap<String, String> params = new HashMap<String, String>();

                        params.put("Content-Type", "application/json");

                        if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                        {
                            String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                            Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                            params.put("authorization", "bearer " + accessToken);
                        }



                        return params;
                    }

                });



            }else {

                 Log.w(StaticMaster.addGuardian, "StaticMaster is empty");
             }


        }catch (JSONException e){}

    }

      public static String GetNames(String value)
     {
         String name = null;

           for(ContactDetails details : StaticMaster.detailsArrayList)
         {

             String temp = details.getMobile().replace(" ", "").replace("-", "").replace("(", "").replace(")", ""); // mobile
             String mobile = temp.substring(temp.length()-10, temp.length());

              if(mobile.equals(value))
             {
                 name = details.getName();
             }
         }

         return name;
     }

      public static void AddGuardians(final Context context, final String url, final JSONObject data, int requestMethod, final String cmd)
    {
        final MobileDatabase database = new MobileDatabase(context, "GUARDIAN", null, 1);
        Networking.downloadList.clear();
        Networking.count = -1;

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(requestMethod, url, data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                try {

                    Log.d("ServerResponse", jsonObject.toString());

                    final JSONArray array = jsonObject.getJSONArray("result");

                      for(int i=0; i<array.length(); i++)
                    {
                        JSONObject ob = array.getJSONObject(i);

                          if(ob.has("guardian_id"))
                        {
                            final String id = ob.getString("guardian_id");

                            JsonObjectRequest subRequest = new JsonObjectRequest(Request.Method.GET, Networking.CommonUrl+"guardian/"+id, null, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject jsonObject) {

                                    Networking.count++;

                                    Log.d("ServerResponse", jsonObject.toString());

                                       if(jsonObject.has("guardian"))
                                    {
                                        try {

                                            JSONObject master = jsonObject.getJSONObject("guardian");

                                            String mobile = master.getString("primaryContact");

                                            String name = null, picUrl = null;

                                              if(master.has("name"))
                                            {
                                                name = master.getString("name");

                                            }else {

                                                   for(ContactDetails details : StaticMaster.detailsArrayList)
                                                  {
                                                        if(details.getMobile().replace(" ", "").replace("+91", "").equals(mobile))
                                                      {
                                                          name = details.getName();
                                                          picUrl = details.getpicURL();
                                                          mobile = details.getMobile();

                                                      }

                                                  }


                                              }

                                              String photo = null;

                                             if(master.has("photo"))
                                            {
                                                photo = master.getString("photo");

                                                if(photo != null)
                                                {
                                                    Networking.downloadList.put(id, photo);
                                                }

                                            }else {

                                                 photo = picUrl;
                                             }

                                                database.insertGuardian(id, name, mobile, photo, "false", "true");
                                                GuardianDetails guardianDetails = database.getGuardianFromId(id);
                                                Log.d(StaticMaster.logs.GuardiansProcessing.name(), "Guardian Inserted To DB:- "+guardianDetails.getName()+" "+guardianDetails.getMobile()+" "+guardianDetails.getStatus());

                                            Log.d("GuardiansProcessing", "Count = "+Networking.count+"   "+array.length());


                                                StaticMaster.detailsArrayList.clear();


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }finally {

                                            if(Networking.count == array.length()-1)
                                            {
                                                // this is the last request

                                                database.close();
                                                Networking.handlePendingDownloads(context, "GuardiansProcessing");
                                            }

                                        }
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError volleyError) {

                                }

                            }){
                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {


                                    HashMap<String, String> params = new HashMap<String, String>();

                                    params.put("Content-Type", "application/json");

                                    if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                                    {
                                        String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                                        Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                                        params.put("authorization", "bearer " + accessToken);
                                    }



                                    return params;
                                }


                            };


                            RequestQueue requestQueue = Volley.newRequestQueue(context);
                            requestQueue.add(subRequest);
                            requestQueue.start();


                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {


                HashMap<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");

                if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                {
                    String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                    Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                    params.put("authorization", "bearer " + accessToken);
                }



                return params;
            }

        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsObjRequest);
        requestQueue.start();

        }

      public static void handlePendingDownloads(final Context context, final String name)
    {
        Log.w(name, "Handling Pending Requests");

          if(Networking.downloadList.size() > 0)
        {
            Log.d(name, Networking.downloadList.size() + " images to download");

            final Set<String> keys = Networking.downloadList.keySet();
            Networking.count = 0;

              for(final String key : keys)
            {

                ImageRequest request = new ImageRequest(Networking.downloadList.get(key), new com.android.volley.Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap bitmap) {

                                ++Networking.count;

                                try {

                                    FileOutputStream out1  = context.openFileOutput(key, Context.MODE_PRIVATE);
                                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
                                    byte[] buffer = bos.toByteArray();
                                    out1.write(buffer, 0, buffer.length);
                                    out1.close();

                                    Log.d(name, "Image "+key+" is downlaoded and stored internally");

                                    Log.d(StaticMaster.logs.ServerResponse.name(), "Image Count = "+Networking.count);


                                } catch (FileNotFoundException e) {

                                    Log.d(name, "Image Download Error :- "+e.getMessage());
                                } catch (IOException e) {

                                    Log.d(name, "Image Download Error :- "+e.getMessage());
                                } catch (Exception e){

                                    Log.d(name, "Image Download Error :- "+e.getMessage());

                                }finally {

                                      if(Networking.count == keys.size())
                                    {
                                        // this is the last request

                                        Log.d(name, "This is the last request");
                                        context.sendBroadcast(new Intent(name));

                                    }
                                }

                            }
                        }, 0, 0, null,
                        new com.android.volley.Response.ErrorListener() {

                            public void onErrorResponse(VolleyError error) {

                                 Log.d(name, "Image Download Error :- "+error.getLocalizedMessage());


                                 context.sendBroadcast(new Intent(name));
                            }
                        });

                RequestQueue queue = Volley.newRequestQueue(context);
                queue.add(request);
                queue.start();

            }

        }else {

             context.sendBroadcast(new Intent(name));

         }

    }

     public static void addDevice(final Context context, final String url, final JSONObject data, int requestMethod, final String cmd)
    {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(requestMethod, url, data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject object) {

                Log.w("ServerResponse", object.toString());

                Log.w("ServerQueryData", data.toString());


                try {

                      if(object.getString("status").equals("success"))
                   {
                       SharedPrefrenceStorage.StoreMacAdd(context, StaticMaster.tempData);
                       Intent intent = new Intent("StartScan");
                       intent.putExtra("data", "mac");
                       context.sendBroadcast(intent);

                   }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {


                HashMap<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");

                if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                {
                    String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                    Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                    params.put("authorization", "bearer " + accessToken);
                }



                return params;
            }

        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsObjRequest);
        requestQueue.start();

     }

      public static void DownloadGuardiansFromArchive(final Context context) // Currently in use
    {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, Networking.CommonUrl+"guardians", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                try {

                    Log.w("ServerResponse", jsonObject.toString());

                    JSONArray jsonArray = jsonObject.getJSONArray("guardians");

                      for(int i=0; i<jsonArray.length(); i++)
                    {
                        JSONObject master = jsonArray.getJSONObject(i);
                        String id = null, name = null, mobile = null, photo = null, status = null;

                         if(master.has("id"))
                        {
                            id = master.getString("id");
                        }

                          if(master.has("alias"))
                        {
                            name = master.getString("alias");

                        }
                           else if(master.has("name"))
                        {
                            name = master.getString("name");
                        }

                         if(master.has("primaryContact"))
                        {
                            mobile = master.getString("primaryContact");
                        }

                         if(master.has("photo"))
                        {
                           photo = master.getString("photo");
                           Networking.DownloadAndStoreImage(context, photo, id);
                        }

                            if(master.has("pending"))
                         {
                            status = master.getString("pending");

                             if(status.equals("true"))
                            {
                                status = "false";
                            }
                              else if(status.equals("false"))

                             {
                                 status = "true";
                             }
                        }

                          StaticMaster.database(context).insertGuardian(id,name,mobile,photo,status,(master.has("email") ? "true" : "false"));

                        Log.w(StaticMaster.downloadArchive, id+" "+name+" "+mobile+" "+(photo == null ? "Photo_Null" : photo)+" "+(master.has("email") ? "true" : "false"));
                    }

                } catch (JSONException e) {

                    Log.w(StaticMaster.downloadArchive, e.getMessage());

                }
                  finally {

                    context.sendBroadcast(new Intent("MobileVerification"));

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {


                HashMap<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");

                if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                {
                    String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                    Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                    params.put("authorization", "bearer " + accessToken);
                }



                return params;
            }

        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsObjRequest);
        requestQueue.start();


    }

    public static void DownloadWardsFromArchive(final Context context)
    {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, Networking.CommonUrl+"wards", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                try {

                    Log.w(StaticMaster.downloadArchive, jsonObject.toString());

                    JSONArray jsonArray = jsonObject.getJSONArray("wards");

                    for(int i=0; i<jsonArray.length(); i++)
                    {
                        JSONObject master = jsonArray.getJSONObject(i);
                        String id = null, name = null, mobile = null, photo = null, status = null;

                        if(master.has("id"))
                        {
                            id = master.getString("id");
                        }

                        if(master.has("alias"))
                        {
                            name = master.getString("alias");

                        }
                        else if(master.has("name"))
                        {
                            name = master.getString("name");
                        }

                        if(master.has("primaryContact"))
                        {
                            mobile = master.getString("primaryContact");
                        }

                        if(master.has("photo"))
                        {
                            photo = master.getString("photo");

                            Networking.DownloadAndStoreImage(context, photo, id);
                        }

                          if(master.has("pending"))
                        {
                            status = master.getString("pending");
                        }

                          StaticMaster.database(context).insertWard(id,name,mobile,photo,status);

                        Log.w(StaticMaster.downloadArchive, id+" "+name+" "+mobile+" "+(photo == null ? "Photo_Null" : photo)+" "+(master.has("email") ? "true" : "false"));
                    }

                } catch (JSONException e) {

                    Log.w(StaticMaster.downloadArchive, e.getMessage());

                }
                finally {

                    context.sendBroadcast(new Intent("MobileVerification"));

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {


                HashMap<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");

                if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                {
                    String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                    Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                    params.put("authorization", "bearer " + accessToken);
                }



                return params;
            }

        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsObjRequest);
        requestQueue.start();


    }

     public static void commonVolleyLogs(VolleyError volleyError, String log)
    {

        if(volleyError.networkResponse != null && volleyError.networkResponse.data != null)
        {
            NetworkResponse response = volleyError.networkResponse;
            String responseData = new String(response.data);

            switch (response.statusCode){

                case 200:{

                    StaticMaster.logsList.add("Some error has occured in SaferWalk request -- "+responseData+"@"+StaticMaster.getCurrentTimeStamp());

                    Log.w(log, "ResponseCode_200 "+responseData);

                }break;

                case 400 :{


                    StaticMaster.logsList.add("Some error has occured in SaferWalk request -- "+responseData+"@"+StaticMaster.getCurrentTimeStamp());

                    Log.w(log, "ResponseCode_400 "+responseData);

                }break;
            }


        }

    }

}


/*

        Volley.newRequestQueue(context).add(new JsonObjectRequest(0,"",null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject object) {

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {


                HashMap<String, String> params = new HashMap<String, String>();

                params.put("Content-Type", "application/json");

                if(SharedPrefrenceStorage.GetAuthToken(context) != null)
                {
                    String accessToken = SharedPrefrenceStorage.GetAuthToken(context);

                    Log.d(StaticMaster.log, "AccessToken = "+accessToken);
                    params.put("authorization", "bearer " + accessToken);
                }



                return params;
            }

                });


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsObjRequest);
        requestQueue.start();
 */