package tronbox.guardiansfunctionality;

import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import tronbox.guardiansfunctionality.Home.AlertActivity;
import tronbox.guardiansfunctionality.Maps.ActivityRecognitionService;
import tronbox.guardiansfunctionality.Maps.UpdateUserLocation;

public class GuardianBrain extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private BluetoothDevice connectedDevice;
    private String bluetoothAddress;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothGatt bluetoothGatt;
    private BluetoothGattService bluetoothGattService;
    private BluetoothGattCharacteristic bluetoothGattCharacteristic;
    private BluetoothGattDescriptor bluetoothGattDescriptor;
    private String TAG = "SaviourBrain";
    boolean state = false, clickCount = false;
    public  GoogleApiClient googleApiClient;

    private LocationRequest mLocationRequest;

    private Location loc;
    private Geocoder geocoder;



    private Timer timer;
    private String macAddr = null;

    private String connectionState = "Not Known";

    PendingIntent pendingIntent;

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int i, byte[] bytes) {

            Log.w(TAG, "Device Bond State " + device.getBondState()+" "+device.getAddress());

             if(State != null && device != null)
            {
                  if(State.equals("Scan"))
                {
                    Intent intent = new Intent("search_mac");
                    intent.putExtra("name", device.getName());
                    intent.putExtra("mac", device.getAddress());
                    sendBroadcast(intent);
                }

                  else if(State.equals("ReConnect") && SharedPrefrenceStorage.GetMacAdd(getApplicationContext()) != null && device.getAddress().equals(SharedPrefrenceStorage.GetMacAdd(getApplicationContext())))
                {
                   connect(device.getAddress());
                }
                  else {

                         if(bluetoothAdapter != null && bluetoothAdapter.isEnabled())
                      {
                          bluetoothAdapter.stopLeScan(leScanCallback);
                          Log.w(TAG, "Unnecesarry Scan Stopped");
                      }
                  }

            }

        }
    };

       public void connect(String device)
    {
          if(bluetoothAdapter != null && bluetoothAdapter.isEnabled())
        {
              if(State.equals("Scan") || State.equals("ReConnect"))
            {
                bluetoothAdapter.stopLeScan(leScanCallback); // Stop the scan as no longer needed

                connectedDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(device);
                macAddr = device;
                Log.w(TAG, "Device No Null Found " + device);
            }
                 else if(State.equals("Connect"))
              {
                  connectedDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(device);
                  macAddr = device;
                  Log.w(TAG, "Device No Null Found " + device);
              }

              if (connectedDevice != null)
            {
                connectedDevice.connectGatt(getApplicationContext(), true, bluetoothGattCallback);
            }
        }
    }


    public android.os.Handler handler = new android.os.Handler();


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.w(TAG, "Service has started");

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(ActivityRecognition.API)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        state = true;
        googleApiClient.connect();

        registerReceiver(broadcastReceiver, new IntentFilter("StartScan"));

        registerReceiver(callReceiver, new IntentFilter("IncommingCall"));

        registerReceiver(userActivityRcvr, new IntentFilter("UserActivityDetection"));


        return START_STICKY;
    }

    public Runnable run = new Runnable() {

        @Override
        public void run() {

            State = "ReConnect";

            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

              if (bluetoothAdapter != null && bluetoothAdapter.isEnabled())
            {
                boolean value = bluetoothAdapter.startLeScan(leScanCallback);
                Log.w(TAG, "Scan Result " + value);
            }

/*
                Log.w(TAG, "BlueGatt Null");

            Intent intent1 = new Intent("StartScan");
            intent1.putExtra("data", "reconnect");
            sendBroadcast(intent1);
*/

      }
    };


    String State = null;


    BroadcastReceiver userActivityRcvr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            startLocationUpdates();

            Log.w(StaticMaster.UserActivity, "User activity recieved");
        }
    };

    BroadcastReceiver locationHandlerRcvr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Toast.makeText(getApplicationContext(), "HandlerRcvr", Toast.LENGTH_SHORT).show();

            mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            if(StaticMaster.CurrentState == StaticMaster.States.Normal)
            {
                mLocationRequest.setInterval(StaticMaster.sosNormalDuration);
            }

             else  if(StaticMaster.CurrentState == StaticMaster.States.Distress)
            {
                mLocationRequest.setInterval(StaticMaster.sosDistressDuration);
            }
             else if(StaticMaster.CurrentState == StaticMaster.States.SafeWalk)
            {
                mLocationRequest.setInterval(StaticMaster.safeWalkDuration);
            }


           // handler.removeCallbacks(run);

           // handler.postDelayed(run, 5000);
        }
    };

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle bundle = intent.getExtras();

             if(bundle != null && bundle.containsKey("data"))
            {
                State = null;

                String data = bundle.getString("data");

                  if(data.equals("scan"))
                {

                    State = "Scan";

                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                      if (bluetoothAdapter != null && bluetoothAdapter.isEnabled() && !bluetoothAdapter.isDiscovering())
                    {
                        boolean value = bluetoothAdapter.startLeScan(leScanCallback);
                        Log.w(TAG, "Scan Result " + value);

                         if(value == false)
                        {
                            value = bluetoothAdapter.startLeScan(leScanCallback);
                            Log.w(TAG, "Scan Result " + value);
                        }

                        Toast.makeText(getApplicationContext(), "ScanStarted", Toast.LENGTH_SHORT).show();
                    }

                }
                    else if(data.equals("connect_device"))
                  {
                      State = "Connect";

                      String macAdd = bundle.getString("mac");
                      connect(macAdd);
                  }

                     else if(data.equals("reconnect"))
                  {
                      State = "ReConnect";


                       if(bluetoothGatt != null)
                      {
                          Log.w(TAG, "BlueGatt Not Null");
                      }
                        else {

                           bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

                              if (bluetoothAdapter != null && bluetoothAdapter.isEnabled())
                           {
                               boolean value = bluetoothAdapter.startLeScan(leScanCallback);
                               Log.w(TAG, "Scan Result " + value);
                           }

                           Log.w(TAG, "BlueGatt Null");
                       }
                  }

                     else if(data.equals("after_destroy"))
                  {
                      State = "ReConnect";

                  }
            }

        }
    };



    BroadcastReceiver callReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Toast toast = Toast.makeText(context, "IncommingRecieved", Toast.LENGTH_SHORT);
            toast.show();

            send(new byte[]{1});

        }
    };


      protected void startLocationUpdates()
    {
       LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
    }

    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    //********* Device Callback Handler *************//

    BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {


        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {

            if (newState == BluetoothProfile.STATE_CONNECTED) { // If connection to the remote device is active

                bluetoothGatt = gatt; // Store the BluetoothGatt for the future use.
                gatt.discoverServices(); // Retrieve all the Services being offered by the BLE.

                connectionState = "Connected";

                SharedPrefrenceStorage.StoreConnectionStatus(getApplicationContext(), "Connected");

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) { // If connection to the remote device is active

                connectionState = "Dis_Connected";

                SharedPrefrenceStorage.StoreConnectionStatus(getApplicationContext(), "Dis_Connected");

                Log.w(TAG, "Saviour Disconnected");
                bluetoothAdapter.startLeScan(leScanCallback);

                Intent intent = new Intent("connect");
                intent.putExtra("data", "disconnected");
                sendBroadcast(intent);

            }


        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            if (status == BluetoothGatt.GATT_SUCCESS) {

                bluetoothGattService =  gatt.getService(BluetoothHelper.sixteenBitUuid(0xFFE0)); // UUID_SERVICE is the desired service. //gatt.getService(UUID.fromString("0000FFE0-0000-1000-8000-00805F9B34FB"));


                if (bluetoothGattService != null) { // Found your service.

                    StaticMaster.bluetoothGattService = bluetoothGattService;

                    handler.removeCallbacks(run);

                    Log.w(TAG, "My Service Found");

                    bluetoothGattCharacteristic = bluetoothGattService.getCharacteristic(BluetoothHelper.sixteenBitUuid(0xFFE1));  //UUID.fromString("0000FFE1-0000-1000-8000-00805F9B34FB")


                    if (bluetoothGattCharacteristic != null) {

                        bluetoothGattDescriptor = bluetoothGattCharacteristic.getDescriptor(BluetoothHelper.sixteenBitUuid(0x2902));  //UUID.fromString("00002902-0000-1000-8000-00805F9B34FB")

                        Log.w(TAG + "_Data", "My Characterstics Found");

                        if (bluetoothGattDescriptor != null) { // device is connected

                              if(macAddr != null)
                            {
                                SharedPrefrenceStorage.StoreMacAdd(getApplicationContext(), macAddr);
                            }

                            Intent intent = new Intent("connect");
                            intent.putExtra("data", "connected");
                            sendBroadcast(intent);

                            Log.w(TAG + "_Data", "My Descripter Found");

                            gatt.setCharacteristicNotification(bluetoothGattCharacteristic, true);

                            bluetoothGattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                            bluetoothGattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);


                            gatt.writeDescriptor(bluetoothGattDescriptor);

                            gatt.readCharacteristic(bluetoothGattCharacteristic);

                        }else {

                            Log.w(TAG + "_Data", "My Descripter Not Found");
                        }

                    }
                }


            }


        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                String readData = BluetoothHelper.bytesToString(characteristic.getValue());

                Log.w("Reading_Data", readData);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {


           Log.w(TAG, "*"+characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8,0)+"*");


         Log.w(TAG, "*"+characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8,1)+"*");
/*
            Log.w(TAG, "*"+characteristic.getStringValue(0)+"*");
            Log.w(TAG, "*"+characteristic.getStringValue(1)+"*");
            Log.w(TAG, "*"+characteristic.getStringValue(2)+"*");
*/

            if(characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT8,0) == 1)
            {
                if(clickCount == false)

                {

                    clickCount = true;

                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {

                            StaticMaster.i = 1;

                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {

                                    if(StaticMaster.i == 1){

                                        Log.w("TrackingClicks", "Single Click");

                                          if(SharedPrefrenceStorage.GetPosition(getApplicationContext()).equals("SelfieActivity"))
                                        {
                                            sendBroadcast(new Intent("Selfie"));
                                        }

                                    }else if(StaticMaster.i == 2){


                                        if(SharedPrefrenceStorage.GetPosition(getApplicationContext()).equals("HomeActivity"))
                                        {
                                            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                            v.vibrate(1000);

                                            Intent intent = new Intent(getApplicationContext(), AlertActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);

                                            mLocationRequest.setInterval(StaticMaster.sosDistressDuration);
                                        }

                                        Log.w("TrackingClicks", "Double Click");
                                    }


                                    clickCount = false;
                                    StaticMaster.i = 0;

                                }
                            }, 1000);

                        }
                    }, 0);

                }else if(clickCount == true) {

                    StaticMaster.i = 2;

                }
            }
        }
    };

    public boolean send(byte[] data) {
        if (bluetoothGatt == null || bluetoothGattService == null) {
            Log.w(TAG, "BluetoothGatt not initialized");
            return false;
        }


        BluetoothGattCharacteristic characteristic = bluetoothGatt.getService(BluetoothHelper.sixteenBitUuid(0x1802)).getCharacteristic(BluetoothHelper.sixteenBitUuid(0x2A06));

        if (characteristic == null) {
            Log.w(TAG, "Send characteristic not found");
            return false;
        }

        Log.w(TAG, "Sending Data");

        characteristic.setValue(data);
        characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
        return bluetoothGatt.writeCharacteristic(characteristic);
    }

      public void registerService(BluetoothGatt gatt, long service, long characterstics)
    {

       BluetoothGattService bluetoothGattService = gatt.getService(BluetoothHelper.sixteenBitUuid(service)); // UUID_SERVICE is the desired service.

        if (bluetoothGattService != null) { // service is found

            BluetoothGattCharacteristic bluetoothGattCharacteristic = bluetoothGattService.getCharacteristic(BluetoothHelper.sixteenBitUuid(characterstics));

              if (bluetoothGattCharacteristic != null)
            {
                BluetoothGattDescriptor bluetoothGattDescriptor = bluetoothGattCharacteristic.getDescriptor(BluetoothHelper.sixteenBitUuid(0x2902));

                if (bluetoothGattDescriptor != null) {

                    gatt.setCharacteristicNotification(bluetoothGattCharacteristic, true);

                    bluetoothGattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);

                    gatt.writeDescriptor(bluetoothGattDescriptor);

                    gatt.readCharacteristic(bluetoothGattCharacteristic);

                }

            }
        }


    }

    //***********************************************//


    @Override
    public void onConnected(Bundle bundle) {

        Log.w(StaticMaster.UserActivity, "User Activity Tracker Connected");

        startLocationUpdates();

        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, new Intent(getApplicationContext(), ActivityRecognitionService.class), PendingIntent.FLAG_UPDATE_CURRENT);
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(googleApiClient, 10000, pendingIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.w(TAG, "Service has Stopped");

     //   startActivity(new Intent(getApplicationContext(), HomeActivity.class));
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.w(StaticMaster.ServiceLogs, "Location Disconneted :- "+i);

    }

    @Override
    public void onLocationChanged(Location location) {

        Log.w(StaticMaster.UserActivity, "User's new location rcvd");

        Toast.makeText(getApplicationContext(), "New Location Received", Toast.LENGTH_SHORT).show();

        // Sending the request for Reverse_Geocode and Local + Server Storage of user location

        Intent intent = new Intent(getApplicationContext(), UpdateUserLocation.class);
        intent.putExtra("location", location);
        startService(intent);

        //*********************//

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {


        Log.w(StaticMaster.ServiceLogs, "Location Disconneted :- "+connectionResult.getErrorCode());
    }


}

class BluetoothHelper {
    public static String shortUuidFormat = "0000%04X-0000-1000-8000-00805F9B34FB";

    public static UUID sixteenBitUuid(long shortUuid) {
        assert shortUuid >= 0 && shortUuid <= 0xFFFF;
        return UUID.fromString(String.format(shortUuidFormat, shortUuid & 0xFFFF));
    }


    private static String parseScanRecord(byte[] scanRecord) {
        StringBuilder output = new StringBuilder();
        int i = 0;
        while (i < scanRecord.length) {
            int len = scanRecord[i++] & 0xFF;
            if (len == 0) break;
            switch (scanRecord[i] & 0xFF) {
                // https://www.bluetooth.org/en-us/specification/assigned-numbers/generic-access-profile
                case 0x0A: // Tx Power
                    output.append("\n  Tx Power: ").append(scanRecord[i+1]);
                    break;
                case 0xFF: // Manufacturer Specific data (RFduinoBLE.advertisementData)
                    output.append("\n  Advertisement Data: ")
                            .append(BluetoothHelper.bytesToHex(scanRecord, i + 3, len));

                    String ascii = BluetoothHelper.bytesToAsciiMaybe(scanRecord, i + 3, len);
                    if (ascii != null) {
                        output.append(" (\"").append(ascii).append("\")");
                    }
                    break;
            }
            i += len;
        }
        return output.toString();
    }

    public static int PRINTABLE_ASCII_MIN = 0x20; // ' '
    public static int PRINTABLE_ASCII_MAX = 0x7E; // '~'

    public static boolean isPrintableAscii(int c) {
        return c >= PRINTABLE_ASCII_MIN && c <= PRINTABLE_ASCII_MAX;
    }


    public static String bytesToHex(byte[] data, int offset, int length) {
        if (length <= 0) {
            return "";
        }

        StringBuilder hex = new StringBuilder();
        for (int i = offset; i < offset + length; i++) {
            hex.append(String.format(" %02X", data[i] % 0xFF));
        }
        hex.deleteCharAt(0);
        return hex.toString();
    }



    public static String bytesToAsciiMaybe(byte[] data, int offset, int length) {
        StringBuilder ascii = new StringBuilder();
        boolean zeros = false;
        for (int i = offset; i < offset + length; i++) {
            int c = data[i] & 0xFF;
            if (isPrintableAscii(c)) {
                if (zeros) {
                    return null;
                }
                ascii.append((char) c);
            } else if (c == 0) {
                zeros = true;
            } else {
                return null;
            }
        }
        return ascii.toString();
    }

    public static String bytesToString(byte[] data){

        return new String(data);
    }


}







