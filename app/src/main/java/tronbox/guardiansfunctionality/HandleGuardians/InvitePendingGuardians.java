
package tronbox.guardiansfunctionality.HandleGuardians;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.Request;

import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.AddGuardians.MobileDatabase;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.Login.CommonFunctions;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.StaticMaster;

public class InvitePendingGuardians extends FragmentActivity {

    StableArrayAdapter mAdapter;
    ListView mListView;
    BackgroundContainer mBackgroundContainer;
    boolean mSwiping = false;
    boolean mItemPressed = false;
    HashMap<Long, Integer> mItemIdTopMap = new HashMap<Long, Integer>();

    private static final int SWIPE_DURATION = 250;
    private static final int MOVE_DURATION = 150;

    private ArrayList<GuardianDetails> detailsArrayList = new ArrayList<>();
    CommonFunctions comFunc=new CommonFunctions();

    boolean tap = false;

    private Point point = new Point();

    private ImageView navigateBack;

    MobileDatabase database = new MobileDatabase(InvitePendingGuardians.this, "GUARDIAN", null, 1);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_deletion);

        getWindowManager().getDefaultDisplay().getSize(point);

        navigateBack = (ImageView)findViewById(R.id.invite_pending_back);
        navigateBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);

            }
        });

        detailsArrayList.clear();

        for(GuardianDetails details : database.getAllWard())
        {
             if(details.getStatus().equals("true"))
            {
                detailsArrayList.add(details);
            }
        }



        mBackgroundContainer = (BackgroundContainer) findViewById(R.id.listViewBackground);
        mListView = (ListView) findViewById(R.id.lstContact);
        android.util.Log.d("Debug", "d=" + mListView.getDivider());

        mAdapter = new StableArrayAdapter(this, R.layout.custom_gurdian_invite_list, detailsArrayList,mTouchListener, clickListener);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                GuardianDetails guardianDetails = (GuardianDetails)parent.getAdapter().getItem(position);

                StaticMaster.tempGuardianDetails = guardianDetails;

                Log.d(StaticMaster.log+"InvitePending", StaticMaster.tempContactDetails.getName()+" Request is accepted");


                //RelativeLayout layout = (RelativeLayout)view.findViewById(R.id.llGurdianList);


            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * Handle touch events to fade/move dragged items as they are swiped out
     */

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {

            ImageView click = (ImageView)v;


            Log.d(StaticMaster.log+"InvitePending", "Position = "+StaticMaster.temp);

            GuardianDetails details = mAdapter.getItem(StaticMaster.temp);

             if(details != null)
            {
               StaticMaster.tempGuardianDetails = details;
            }


            if(v.getId() == R.id.guardian_invite_accept)
            {

                Log.d(StaticMaster.log+"InvitePending", details.getName()+" Request is accepted");

                boolean wardStatus = database.updateWardStatus(details.getId(), "true");

                Log.w("PendingIntent", "Status "+wardStatus);

                click.setImageBitmap(comFunc.imageCirclexClipBackground(getApplicationContext(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_accept), "#34bf49"));

               StaticMaster.sendCommonData(getApplicationContext(), "Accept_Guardian_Request", Networking.CommonUrl+"guardian/" + details.getId() + "/accept", Request.Method.PUT);

                getApplicationContext().sendBroadcast(new Intent("Refresh_Home"));

            }else if(v.getId() == R.id.guardian_invite_cancel){

                Log.d(StaticMaster.log+"InvitePending", details.getName()+" Request is declined");

                click.setImageBitmap(comFunc.imageCirclexClipBackground(getApplicationContext(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_cancel), "#fb0a2a"));

                StaticMaster.sendCommonData(getApplicationContext(), "Decline_Guardian_Request", Networking.CommonUrl+"guardian/" + details.getId() + "/decline", Request.Method.PUT);


            }



            NotificationManager mNotifyMgr = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            mNotifyMgr.cancelAll();


            mListView.getChildAt(StaticMaster.temp).animate().setDuration(1000).
                    alpha(0).translationX(700).
                    withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            // Restore animated values
                            v.setAlpha(1);
                            v.setTranslationX(0);

                            detailsArrayList.remove(StaticMaster.temp);
                            mAdapter.notifyDataSetChanged();

                               if(detailsArrayList.size() == 0)
                            {
                                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }

                               //animateRemoval(mListView, mListView.getChildAt(StaticMaster.temp));

                        }
                    });

            //animateRemoval(mListView, v);


        }
    };

    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        
        float mDownX;
        private int mSwipeSlop = -1;
        
        @Override
        public boolean onTouch(final View v, MotionEvent event) {
            if (mSwipeSlop < 0) {
                mSwipeSlop = ViewConfiguration.get(InvitePendingGuardians.this).
                        getScaledTouchSlop();
            }
            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mItemPressed) {
                    // Multi-item swipes not handled
                    return false;
                }
                mItemPressed = true;
                mDownX = event.getX();
                Log.w("XPosition", ""+mDownX);

/*
                 if(mDownX > (point.x - 45))
                {

                    mListView.setEnabled(false);
                    ImageView click = (ImageView)v.findViewById(R.id.guardian_invite_cancel);
                    click.setImageBitmap(comFunc.imageCirclexClipBackground(getApplicationContext(), BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_cancel), "#fb0a2a"));


                    tap = true;

                }
*/

                break;
            case MotionEvent.ACTION_CANCEL:
                v.setAlpha(1);
                v.setTranslationX(0);
                mItemPressed = false;
                break;
            case MotionEvent.ACTION_MOVE:
                {
                    float x = event.getX() + v.getTranslationX();
                    float deltaX = x - mDownX;
                    float deltaXAbs = Math.abs(deltaX);
                    if (!mSwiping) {
                        if (deltaXAbs > mSwipeSlop) {
                            mSwiping = true;
                            mListView.requestDisallowInterceptTouchEvent(true);
                            mBackgroundContainer.showBackground(v.getTop(), v.getHeight());
                        }
                    }
                    if (mSwiping) {
                        v.setTranslationX((x - mDownX));
                        v.setAlpha(1 - deltaXAbs / v.getWidth());
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                {

                     /* if(mDownX > (point.x - 50))
                    {
                        v.animate().setDuration(1000).
                                alpha(0).translationX(700).
                                withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Restore animated values
                                        v.setAlpha(1);
                                        v.setTranslationX(0);
                                        animateRemoval(mListView, v);

                                    }
                                });

                    }*/

                        // User let go - figure out whether to animate the view out, or back into place
                    if (mSwiping) {
                        float x = event.getX() + v.getTranslationX();
                        float deltaX = x - mDownX;
                        float deltaXAbs = Math.abs(deltaX);
                        float fractionCovered;
                        float endX;
                        float endAlpha;
                        final boolean remove;
                        if (deltaXAbs > v.getWidth() / 4) {
                            // Greater than a quarter of the width - animate it out
                            fractionCovered = deltaXAbs / v.getWidth();
                            endX = deltaX < 0 ? -v.getWidth() : v.getWidth();
                            endAlpha = 0;
                            remove = true;
                        } else {
                            // Not far enough - animate it back
                            fractionCovered = 1 - (deltaXAbs / v.getWidth());
                            endX = 0;
                            endAlpha = 1;
                            remove = false;
                        }
                        // Animate position and alpha of swiped item
                        // NOTE: This is a simplified version of swipe behavior, for the
                        // purposes of this demo about animation. A real version should use
                        // velocity (via the VelocityTracker class) to send the item off or
                        // back at an appropriate speed.
                        long duration = (int) ((1 - fractionCovered) * SWIPE_DURATION);
                        mListView.setEnabled(false);
                        v.animate().setDuration(duration).
                                alpha(endAlpha).translationX(endX).
                                withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Restore animated values
                                        v.setAlpha(1);
                                        v.setTranslationX(0);
                                        if (remove) {
                                            animateRemoval(mListView, v);
                                        } else {
                                            mBackgroundContainer.hideBackground();
                                            mSwiping = false;
                                            mListView.setEnabled(true);
                                        }
                                    }
                                });
                    }else {
                        }

                }
                mItemPressed = false;
                break;
            default: 
                return false;
            }
            return true;
        }
    };

    /**
     * This method animates all other views in the ListView container (not including ignoreView)
     * into their final positions. It is called after ignoreView has been removed from the
     * adapter, but before layout has been run. The approach here is to figure out where
     * everything is now, then allow layout to run, then figure out where everything is after
     * layout, and then to run animations between all of those start/end positions.
     */
    private void animateRemoval(final ListView listview, View viewToRemove) {
        int firstVisiblePosition = listview.getFirstVisiblePosition();
        for (int i = 0; i < listview.getChildCount(); ++i) {
            View child = listview.getChildAt(i);
            if (child != viewToRemove) {
                int position = firstVisiblePosition + i;
                long itemId = mAdapter.getItemId(position);
                mItemIdTopMap.put(itemId, child.getTop());
            }
        }
        // Delete the item from the adapter
        int position = mListView.getPositionForView(viewToRemove);
        mAdapter.remove(mAdapter.getItem(position));

        final ViewTreeObserver observer = listview.getViewTreeObserver();
        observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                observer.removeOnPreDrawListener(this);
                boolean firstAnimation = true;
                int firstVisiblePosition = listview.getFirstVisiblePosition();
                for (int i = 0; i < listview.getChildCount(); ++i) {
                    final View child = listview.getChildAt(i);
                    int position = firstVisiblePosition + i;
                    long itemId = mAdapter.getItemId(position);
                    Integer startTop = mItemIdTopMap.get(itemId);
                    int top = child.getTop();
                    if (startTop != null) {
                        if (startTop != top) {
                            int delta = startTop - top;
                            child.setTranslationY(delta);
                            child.animate().setDuration(MOVE_DURATION).translationY(0);
                            if (firstAnimation) {
                                child.animate().withEndAction(new Runnable() {
                                    public void run() {
                                        mBackgroundContainer.hideBackground();
                                        mSwiping = false;
                                        mListView.setEnabled(true);

                                        detailsArrayList.remove(StaticMaster.temp);
                                        mAdapter.notifyDataSetChanged();

                                        if(detailsArrayList.size() == 0)
                                        {

                                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }


                                    }
                                });
                                firstAnimation = false;
                            }
                        }
                    } else {
                        // Animate new views along with the others. The catch is that they did not
                        // exist in the start state, so we must calculate their starting position
                        // based on neighboring views.
                        int childHeight = child.getHeight() + listview.getDividerHeight();
                        startTop = top + (i > 0 ? childHeight : -childHeight);
                        int delta = startTop - top;
                        child.setTranslationY(delta);
                        child.animate().setDuration(MOVE_DURATION).translationY(0);
                        if (firstAnimation) {
                            child.animate().withEndAction(new Runnable() {
                                public void run() {
                                    mBackgroundContainer.hideBackground();
                                    mSwiping = false;
                                    mListView.setEnabled(true);


                                    detailsArrayList.remove(StaticMaster.temp);
                                    mAdapter.notifyDataSetChanged();

                                     if(detailsArrayList.size() == 0)
                                    {

                                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }

                                }
                            });
                            firstAnimation = false;
                        }
                    }
                }
                mItemIdTopMap.clear();
                return true;
            }
        });
    }

}


/*

Dummy Data

        detailsArrayList.add(new ContactDetails("djjd", "djdj", "122djdj", null));
        detailsArrayList.add(new ContactDetails("djsjd", "ddjdjdj", "3323djdj", null));
        detailsArrayList.add(new ContactDetails("djjd", "djdw92j", "555djdj", null));
        detailsArrayList.add(new ContactDetails("djjd", "djd09029j", "d677jdj", null));


 */