/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tronbox.guardiansfunctionality.HandleGuardians;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import tronbox.guardiansfunctionality.AddGuardians.ContactDetails;
import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.Login.CommonFunctions;
import tronbox.guardiansfunctionality.Networking.Networking;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.StaticMaster;

public class StableArrayAdapter extends ArrayAdapter<GuardianDetails> {

    HashMap<GuardianDetails, Integer> mIdMap = new HashMap<GuardianDetails, Integer>();
    View.OnTouchListener mTouchListener;
    View.OnClickListener clickListener;
    private List<GuardianDetails> list;


    private Context context;
    private int resource;
    CommonFunctions comFunc=new CommonFunctions();


    public StableArrayAdapter(Context context, int textViewResourceId,
            List<GuardianDetails> objects, View.OnTouchListener listener, View.OnClickListener clickListener) {
        super(context, textViewResourceId, objects);

        this.context = context;
        this.list = objects;
        this.resource = textViewResourceId;

        mTouchListener = listener;
        this.clickListener = clickListener;

        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(objects.get(i), i);
        }
    }

    @Override
    public long getItemId(int position) {
        GuardianDetails item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        StaticMaster.temp = position;

        View view = LayoutInflater.from(context).inflate(resource, parent, false);

        if (view != convertView) {
            // Add touch listener to every new view to track swipe motion
            view.setOnTouchListener(mTouchListener);


            GuardianDetails details = list.get(position);

            TextView name = (TextView)view.findViewById(R.id.txtContactName);
            name.setText(details.getName());

            TextView mobile = (TextView)view.findViewById(R.id.txtContactMobile);
            mobile.setText(details.getMobile());

             if(details.getImage() != null)
            {
                try {


                ((ImageView) view.findViewById(R.id.imgContactPhoto)).setImageBitmap(comFunc.imageCirclexClip(context, BitmapFactory.decodeStream(context.openFileInput(details.getId()))));

                }catch (FileNotFoundException e){

                    ((ImageView) view.findViewById(R.id.imgContactPhoto)).setImageBitmap(comFunc.imageCirclexClip(context, BitmapFactory.decodeResource(context.getResources(), R.drawable.face)));
                }
                    //Networking.LiveStreamImage(context, details.getImage(), ((ImageView) view.findViewById(R.id.imgContactPhoto)));

            }else {

                 ((ImageView) view.findViewById(R.id.imgContactPhoto)).setImageBitmap(comFunc.imageCirclexClip(context, BitmapFactory.decodeResource(context.getResources(), R.drawable.face)));

             }

            ImageView click = (ImageView)view.findViewById(R.id.guardian_invite_accept);
            click.setImageBitmap(comFunc.imageCirclexClipBackground(context, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_action_accept), "#9f9fa3"));
            click.setOnClickListener(clickListener);


            ImageView cancel = (ImageView)view.findViewById(R.id.guardian_invite_cancel);
            cancel.setImageBitmap(comFunc.imageCirclexClipBackground(context, BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_action_cancel), "#9f9fa3"));
            cancel.setOnClickListener(clickListener);

        }
        return view;

    }

}
