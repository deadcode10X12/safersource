package tronbox.guardiansfunctionality.HandleGuardians;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.Home.HomeActivity;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.SharedPrefrenceStorage;
import tronbox.guardiansfunctionality.StaticMaster;

/**
 * Created by Shrinivas Mishra on 4/7/2015.
 * Showing Guardian Activity List and User current location
 */
public class GuardianAlertActivity extends FragmentActivity implements LocationListener
{

    GoogleMap googleMap;
    GuardianAlertList GAListAdapter;
    Context myContext=GuardianAlertActivity.this;
    TextView txtGuardianAlertLocation;
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guardian_alert);
        ListView lstvGuardianAlertList = (ListView) findViewById(R.id.lstvGuardianAlertList);
        ArrayList<GuardianDetails> detailsArrayList = new ArrayList<GuardianDetails>();

         for(GuardianDetails details : StaticMaster.database(myContext).getAllGuardians())
        {
            detailsArrayList.add(details);
        }

        GAListAdapter = new GuardianAlertList(myContext, R.layout.guardian_alert_list, detailsArrayList);
        lstvGuardianAlertList.setAdapter(GAListAdapter);

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if(status!= ConnectionResult.SUCCESS)
        {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }else {

            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            googleMap = fm.getMap();
            googleMap.setMyLocationEnabled(true);

              if(SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()) != null)
            {
                String coord[] = SharedPrefrenceStorage.GetUserCoordinates(getApplicationContext()).split("@");
                Location location1 = new Location("d");
                location1.setLatitude(Double.parseDouble(coord[0]));
                location1.setLongitude(Double.parseDouble(coord[1]));

                onLocationChanged(location1);
            }
        }
    }
    @Override
    public void onLocationChanged(Location location) {
        txtGuardianAlertLocation = (TextView) findViewById(R.id.txtGuardianAlertLocation);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng = new LatLng(latitude, longitude);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

        //txtGuardianAlertLocation.setText("Latitude:" + latitude + ", Longitude:" + longitude);
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                txtGuardianAlertLocation.setText(strReturnedAddress.toString());
            }
            else{
                txtGuardianAlertLocation.setText("No Address identified!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            txtGuardianAlertLocation.setText("Cannot get Address!");
        }

        TextView txtGuardianAlertCancel=(TextView)findViewById(R.id.txtGuardianAlertCancel);
        txtGuardianAlertCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(GuardianAlertActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_dailog_without_image);
                TextView txtMiddleHeadingW = (TextView) dialog.findViewById(R.id.txtMiddleHeadingW);
                txtMiddleHeadingW.setText("Are you safe now ?");
                dialog.show();

                RelativeLayout rlDialogAcceptW = (RelativeLayout) dialog.findViewById(R.id.rlDialogAcceptW);
                rlDialogAcceptW.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        StaticMaster.sosDuration = StaticMaster.sosNormalDuration;
                        StaticMaster.CurrentState = StaticMaster.States.Normal;

                        String url = "http://54.169.182.72/api/v0/activity/"+SharedPrefrenceStorage.GetActivityId(getApplicationContext())+"/close";

                        Volley.newRequestQueue(myContext).add(new JsonObjectRequest(Request.Method.PUT, url, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject object) {

                                Log.w("SOSOperationReciever", object.toString());

                                Intent intent = new Intent(myContext, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {

                            }
                        }){
                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {


                                HashMap<String, String> params = new HashMap<String, String>();

                                params.put("Content-Type", "application/json");

                                if(SharedPrefrenceStorage.GetAuthToken(getApplicationContext()) != null)
                                {
                                    String accessToken = SharedPrefrenceStorage.GetAuthToken(myContext);

                                    Log.d(StaticMaster.log, "AccessToken = " + accessToken);
                                    params.put("authorization", "bearer " + accessToken);
                                }



                                return params;
                            }

                        });
                    }
                });

                RelativeLayout rlDialogDeclineW = (RelativeLayout) dialog.findViewById(R.id.rlDialogDeclineW);
                rlDialogDeclineW.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        dialog.dismiss();

                    }
                });

            }
        });
    }
    @Override
    public void onProviderDisabled(String provider) {
    }
    @Override
    public void onProviderEnabled(String provider) {
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}