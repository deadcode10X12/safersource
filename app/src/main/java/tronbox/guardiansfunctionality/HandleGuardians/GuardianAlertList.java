package tronbox.guardiansfunctionality.HandleGuardians;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tronbox.guardiansfunctionality.AddGuardians.GuardianDetails;
import tronbox.guardiansfunctionality.Home.CircleImagePhoto;
import tronbox.guardiansfunctionality.R;

/**
 * Created by Shrinivas Mishra on 4/7/2015.
 * Class use for show guardian list with distance for alert
 */
public class GuardianAlertList extends ArrayAdapter<GuardianDetails>
{
    private Context context;
    private List<GuardianDetails> list;
    private int resource;
    private Set<Integer> set = new HashSet<Integer>();
    public GuardianAlertList(Context context, int resource, List<GuardianDetails> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.list = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        View view = LayoutInflater.from(context).inflate(resource, parent, false);

        final GuardianDetails holder = list.get(position);

        TextView name = (TextView)view.findViewById(R.id.txtGAName);
        name.setText(holder.getName());
        TextView mobile = (TextView)view.findViewById(R.id.txtGADistance);
        mobile.setText(holder.getDistance());

          if(holder.getImage() != null && holder.getImage().isEmpty()==false)
        {
             if(holder.getImage().contains("https"))
            {
                try {

                    ((CircleImagePhoto)view.findViewById(R.id.imgGAPhoto)).setImageBitmap(BitmapFactory.decodeStream(context.openFileInput(holder.getId())));

                } catch (Exception e) {

                    ((CircleImagePhoto)view.findViewById(R.id.imgGAPhoto)).setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.face));

                }
            }else {

                 Uri photo = Uri.parse(holder.getImage());
                 AssetFileDescriptor afd = null;
                 InputStream inputStream = null;
                 try {
                     inputStream = context.getContentResolver().openInputStream(photo);
                     ((CircleImagePhoto) view.findViewById(R.id.imgGAPhoto)).setImageBitmap(BitmapFactory.decodeStream(inputStream));
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }
        }

        LinearLayout llGLPhoneCall=(LinearLayout)view.findViewById(R.id.llGLPhoneCall);
        llGLPhoneCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+holder.getMobile()));
                context.startActivity(intent);
            }
        });
        return view;
    }
    public void addToSet(int value){
        if(set.contains(value)){
            set.remove(value);
        }else {
            set.add(value);
        }
        notifyDataSetChanged();
    }
    public Set<Integer> getSelectedList(){
        return set;
    }

}
