package tronbox.guardiansfunctionality.AddGuardians;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

import tronbox.guardiansfunctionality.R;

public class PendingGuardians extends ActionBarActivity {

    private ArrayList<ContactDetails> detailsArrayList = new ArrayList<>();
    private ContactsList contactsListAdapter;
    private ListView friendsListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact_list);



        contactsListAdapter = new ContactsList(this, R.layout.custom_gurdian_list, detailsArrayList);

        friendsListView = (ListView)findViewById(R.id.lstContact);
        friendsListView.setAdapter(contactsListAdapter);

    }

}
