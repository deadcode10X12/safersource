package tronbox.guardiansfunctionality.AddGuardians;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolder {

    public TextView text1;
    public TextView text2;
    public ImageView pic;
    public Boolean value;
    public int id;
    public String name;
    public Button invite;
    public TextView getText1()
    { return text1;}
    public void setText1(TextView value)
    { text1=value;}
    public TextView getText2()
    {return text2;}
    public void setText2(TextView value)
    {text2=value;}
    public ImageView getPic()
    {return pic;}
    public void setPic(ImageView value)
    {pic=value;}
    public Button getInvite()
    {return invite;}
    public void setInvite(Button value)
    {invite=value;}
    public String getName()
    {return name;}
    public void setName(String value)
    {name=value;}
    public int getId()
    {return id;}
    public void setId(int value)
    {id=value;}
}
