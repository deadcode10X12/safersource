package tronbox.guardiansfunctionality.AddGuardians;


public class ContactDetails {

    String id;
    String name;
    String mobile;
    String picUrl;
    String color;
    String location;

    public ContactDetails(){

    }

      public ContactDetails(String id, String name, String mobile, String picUrl)
    {
      this.id = id;
      this.name = name;
      this.mobile = mobile;
      this.picUrl = picUrl;
    }

    public ContactDetails(String id, String name, String mobile, String picUrl, String location)
    {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.picUrl = picUrl;
        this.location = location;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String value)
    {
        name=value;
    }
    public String getMobile()
    {
        return mobile;
    }
    public void setMobile(String value)
    {
        mobile=value;
    }
    public String getpicURL()
    {
        return picUrl;
    }
    public void setpicURL(String value)
    {
        picUrl=value;
    }
    public String getColor()
    {
        return color;
    }
    public void setColor(String value)
    {
        color=value;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
