package tronbox.guardiansfunctionality.AddGuardians;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;
import java.security.Guard;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import tronbox.guardiansfunctionality.StaticMaster;

public class MobileDatabase extends SQLiteOpenHelper {

    private Context context;

    private String[] tables =

            {                   // Col#
                    "guardian", // 0
                    "ward",     // 1
                    "temp",      // 2
                    "recentDestinations" //3
            };

    private String[] column =

   {                  // Column#
            "id",    // 0
            "name",   // 1
            "mobile", // 2
            "image",  // 3
            "status", // 4
            "app",     // 5
            "location", // 6
            "address", // 7
            "tag"      // 8

    };

    public MobileDatabase(Context context, String name, CursorFactory factory,
                          int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String guardianTable = "create table "+tables[0]+"( "

                + column[0] + " text primary key,"
                + column[1]+" text ,"
                + column[2]+" text,"
                + column[3]+" text,"
                + column[4]+" text,"
                + column[5]+" text"
                + "); ";


        String wardTable = "create table "+tables[1]+"( "

                + column[0] + " text primary key,"
                + column[1]+" text ,"
                + column[2]+" text,"
                + column[3]+" text,"
                + column[4]+" text"
                + "); ";


        String tempTable = "create table "+tables[2]+"( "

                + column[0] + " text primary key,"
                + column[1]+" text ,"
                + column[2]+" text,"
                + column[3]+" text,"
                + column[6]+" text"
                + "); ";


        String recentDestinationsTable = "create table "+tables[3]+"( "

                + column[0] +  " integer primary key autoincrement,"
                + column[7] + " text,"
                + column[8] + " text); ";


        String scoreTable="create table SCOREDATA("
                + "id"
                + " integer primary key autoincrement,"
                + "CATAGORY text,CATAGORY_NAME text,CHAP_ID text not null UNIQUE,CHAP_NAME text,GLOBAL_RANK text,TOTAL_SCORE text,CORRECT_QUES text, COUNT text, WIN text,LOOSE text,KILL_TIME text); ";


        String friendsTable = "create table FRIENDSDATA("
                + "id"
                + " integer primary key autoincrement,"
                + "Uid text not null UNIQUE, Name text, Pic text not null UNIQUE); ";

        String friendsScoreTable = "create table FRIENDS_SCORE_DATA("
                +                 "id"
                +                 " integer primary key autoincrement,"
                +                 "USER_CODE text,CHAP_ID text,GLOBAL_RANK text,TOTAL_SCORE text,WIN text,LOOSE text,KILL_TIME text,TAG text); ";

        String user_friends_Table="create table USER_FRIENDS_TABLE("
                + "id"
                + " integer primary key autoincrement,"
                + "F_ID text not null UNIQUE,USER_CODE text); ";

        String user_logs_Table="create table USER_LOGS_TABLE("
                + "id"
                + " integer primary key autoincrement,"
                + "USER_LOGS text); ";


        db.execSQL(guardianTable);
        db.execSQL(wardTable);
        db.execSQL(tempTable);
        db.execSQL(recentDestinationsTable);

    }

    // ******* Adding Guardians ************ //

      public int insertGuardian(String id, String name, String mobile, String image, String status, String app)
    {

        ContentValues cv = new ContentValues();

        cv.put(column[0], id);
        cv.put(column[1], name);
        cv.put(column[2], mobile);
        cv.put(column[3], image);
        cv.put(column[4], status);
        cv.put(column[5], app);


        try {

            getWritableDatabase().insertOrThrow(tables[0], null, cv);
            return 1;

        }catch (SQLiteConstraintException ex){return 0;}
        catch (Exception ex){return 0;}

    }

    public void deleteGuardian(String id)
    {
        int value = getWritableDatabase().delete(tables[0], column[0] + " = ?", new String[] { id });

        if(value != 0)
        {
            Log.d(StaticMaster.log, "Guardian record is deleted");
        }
    }



    public ArrayList<GuardianDetails> getAllGuardians()
    {
        Cursor c = getWritableDatabase().rawQuery("select * from "+tables[0], null);

        ArrayList<GuardianDetails> guardiansList = new ArrayList<GuardianDetails>();

          if(c.moveToFirst())
        {
           do {

               GuardianDetails details = new GuardianDetails();
               details.setId(c.getString(0));
               details.setName(c.getString(1));
               details.setMobile(c.getString(2));
               details.setImage(c.getString(3));
               details.setStatus(c.getString(4));
               details.setApp(c.getString(5));
               details.setCategory(StaticMaster.Category.Guardian);

               guardiansList.add(details);

           } while(c.moveToNext());

        }

        return guardiansList;
    }

      public boolean updateGuardianStatus(String id, String value, int col) // col is the column# to be changed
    {

        ContentValues args = new ContentValues();
        args.put(column[col], value);

        return getWritableDatabase().update(tables[0], args, "id='"+id+"'", null) > 0;
    }

      public boolean updateGuardianProfile(String id, String name, String mobile, String image, String app) // col is the column# to be changed
    {

        ContentValues cv = new ContentValues();
        cv.put(column[1], name);
        cv.put(column[2], mobile);
        cv.put(column[3], image);
        cv.put(column[5], app);

        return getWritableDatabase().update(tables[0], cv, "id='"+id+"'", null) > 0;
    }


    public GuardianDetails getGuardianFromMobile(String mob){

        Cursor c = getWritableDatabase().query(tables[0], null, column[2]+"=?", new String[] {mob}, null,null, null, null);
        c.moveToFirst();

        GuardianDetails details = new GuardianDetails();
        details.setId(c.getString(0));
        details.setName(c.getString(1));
        details.setMobile(c.getString(2));
        details.setImage(c.getString(3));
        details.setStatus(c.getString(4));
        details.setApp(c.getString(5));

        return details;
    }

    public GuardianDetails getGuardianFromId(String id){

        Cursor c = getWritableDatabase().query(tables[0], null, column[0]+"=?", new String[] {id}, null,null, null, null);
        c.moveToFirst();

         if(c.moveToFirst())
        {
            GuardianDetails details = new GuardianDetails();
            details.setId(c.getString(0));
            details.setName(c.getString(1));
            details.setMobile(c.getString(2));
            details.setImage(c.getString(3));
            details.setStatus(c.getString(4));
            details.setApp(c.getString(5));

            return details;

        }

        return null;
    }


    // ************************************* //

    // *************  Add ward ***************** //

    public int insertWard(String id, String name, String mobile, String image, String status)
    {

        ContentValues cv = new ContentValues();

        cv.put(column[0], id);
        cv.put(column[1], name);
        cv.put(column[2], mobile);
        cv.put(column[3], image);
        cv.put(column[4], status);

        try {

            getWritableDatabase().insertOrThrow(tables[1], null, cv);
            return 1;

        }catch (SQLiteConstraintException ex){return 0;}
        catch (Exception ex){return 0;}

    }

    public GuardianDetails getWardFromId(String id){

        Cursor c = getWritableDatabase().query(tables[1], null, column[0]+"=?", new String[] {id}, null,null, null, null);
        c.moveToFirst();

        GuardianDetails details = new GuardianDetails();
        details.setId(c.getString(0));
        details.setName(c.getString(1));
        details.setMobile(c.getString(2));
        details.setImage(c.getString(3));
        details.setStatus(c.getString(4));

        return details;
    }

    public boolean updateWardStatus(String id, String value) // col is the column# to be changed
    {

        ContentValues args = new ContentValues();
        args.put(column[4], value);

        return getWritableDatabase().update(tables[1], args, "id='"+id+"'", null) > 0;
    }


    public ArrayList<GuardianDetails> getAllWard()
    {
        Cursor c = getWritableDatabase().rawQuery("select * from "+tables[1], null);

        ArrayList<GuardianDetails> wardList = new ArrayList<GuardianDetails>();

        if(c.moveToFirst())
        {
            do {

                GuardianDetails details = new GuardianDetails();
                details.setId(c.getString(0));
                details.setName(c.getString(1));
                details.setMobile(c.getString(2));
                details.setImage(c.getString(3));
                details.setStatus(c.getString(4));
                details.setCategory(StaticMaster.Category.Ward);

                wardList.add(details);

            } while(c.moveToNext());

        }

        return wardList;
    }

    // ************************************* //

    // ****** Insert Temp Data ************* //

    public int insertTemp(String id, String name, String mobile, String image, String location)
    {

        ContentValues cv = new ContentValues();

        cv.put(column[0], id);
        cv.put(column[1], name);
        cv.put(column[2], mobile);
        cv.put(column[3], image);
        cv.put(column[6], location);


        try {

            getWritableDatabase().insertOrThrow(tables[2], null, cv);
            return 1;

        }catch (SQLiteConstraintException ex){return 0;}
        catch (Exception ex){return 0;}

    }

    public ArrayList<GuardianDetails> getAllTemp()
    {
        Cursor c = getWritableDatabase().rawQuery("select * from "+tables[2], null);

        ArrayList<GuardianDetails> guardiansList = new ArrayList<GuardianDetails>();
        guardiansList.clear();


        if(c.moveToFirst())
        {
            do {

                GuardianDetails details = new GuardianDetails();
                details.setId(c.getString(0)); // id
                details.setName(c.getString(1)); // name
                details.setMobile(c.getString(2)); // mobile
                details.setImage(c.getString(3)); // image
                details.setLocation(c.getString(6)); // location

                guardiansList.add(details);

            } while(c.moveToNext());

        }

        return guardiansList;
    }

     public void deleteWard(String id)
    {
       int value = getWritableDatabase().delete(tables[1], column[0] + " = ?", new String[] { id });

         if(value != 0)
        {
            Log.d(StaticMaster.log, "Ward record is deleted");
        }
    }


    //***************************************//


    //************ Add Destination **********//

      public int insertDestination(String address, String tag)
    {

        ContentValues cv = new ContentValues();

        cv.put(column[7], address);
        cv.put(column[8], tag);

        try {

            getWritableDatabase().insertOrThrow(tables[3], null, cv);
            return 1;

        }catch (SQLiteConstraintException ex){return 0;}
        catch (Exception ex){return 0;}

    }


     public ArrayList<String> getAllDestinations()
    {
        Cursor c = getWritableDatabase().rawQuery("select * from "+tables[3], null);

        ArrayList<String> destinations = new ArrayList<String>();

        if(c.moveToFirst())
        {
            do {

                destinations.add(c.getString(1)+"@"+c.getString(2));

            } while(c.moveToNext());

        }

        Collections.reverse(destinations);

        return destinations;
    }

    //***************************************//

    public void insertLogs(String log){

        Log.w("DatabaseLogs", log);


        ContentValues cv = new ContentValues();
        cv.put("USER_LOGS", log);


        try
        {
            getWritableDatabase().insertOrThrow("USER_LOGS_TABLE", null, cv);

        }catch (SQLiteConstraintException e)
        {
            Log.w("Database_Error", e.getMessage());
        }catch (Exception e){

            Log.w("Database_Error", e.getMessage());
        }

    }

    public Cursor getUserLogs(){

        Cursor c = getWritableDatabase().rawQuery("select * from USER_LOGS_TABLE", null);

        return c;
    }

    public void insertUpdatesQUIZDATA(String CATAGORY,String CATAGORY_NAME,String CATAGORY_IMG_PATH,String CHAP_ID, String CHAP_NAME, String CHAP_IMG_PATH,String CHAP_DES,int LOCK) {
        ContentValues cv = new ContentValues();
        cv.put("CATAGORY", CATAGORY);
        cv.put("CATAGORY_NAME", CATAGORY_NAME);
        cv.put("CATAGORY_IMG_PATH", CATAGORY_IMG_PATH);
        cv.put("CHAP_ID", CHAP_ID);
        cv.put("CHAP_NAME", CHAP_NAME);
        cv.put("CHAP_IMG_PATH", CHAP_IMG_PATH);
        cv.put("CHAP_DES", CHAP_DES);
        cv.put("LOCK", LOCK);

        try
        {
            getWritableDatabase().insertOrThrow("QUIZDATA", null, cv);
        }catch (SQLiteConstraintException e)
        {

        }catch (Exception e){

        }

    }

    public void insertUpdatesSCOREDATA(String CATAGORY,String CATAGORY_NAME,String CHAP_ID,String CHAP_NAME,String GLOBAL_RANK,String TOTAL_SCORE, String CORRECT_QUES, String COUNT) {
        ContentValues cv = new ContentValues();
        cv.put("CATAGORY", CATAGORY);
        cv.put("CATAGORY_NAME", CATAGORY_NAME);
        cv.put("CHAP_ID", CHAP_ID);
        cv.put("CHAP_NAME", CHAP_NAME);
        cv.put("GLOBAL_RANK", GLOBAL_RANK);
        cv.put("TOTAL_SCORE", TOTAL_SCORE);
        cv.put("CORRECT_QUES", CORRECT_QUES);
        cv.put("COUNT", COUNT);

        cv.put("WIN", COUNT);
        cv.put("LOOSE", COUNT);
        cv.put("KILL_TIME", COUNT);

        try
        {
            getWritableDatabase().insertOrThrow("SCOREDATA", null, cv);
        }catch (SQLiteConstraintException e)
        {

        }catch (Exception e)
        {

        }

    }

    public int insertFriends(String Id, String Name, String Pic){

        ContentValues cv = new ContentValues();
        cv.put("Uid", Id);
        cv.put("Name", Name);
        cv.put("Pic", Pic);

        try {

            getWritableDatabase().insertOrThrow("FRIENDSDATA", null, cv);
            return 1;

        }catch (SQLiteConstraintException ex){return 0;}
        catch (Exception ex){return 0;}

    }


    public String getUserNameAndWithFBId()
    {

        return null;
    }


    public boolean updateTotalScore(String CHAP_ID, String TOTAL_SCORE) {

        Cursor c = getDataSCOREDATA(CHAP_ID);
        c.moveToFirst();
        String rowId = c.getString(0);
        c.close();
        ContentValues args = new ContentValues();
        args.put("TOTAL_SCORE", TOTAL_SCORE);
        return getWritableDatabase().update("SCOREDATA", args, "id='"+rowId+"'", null) > 0;
    }

    public boolean updateGlobalRank(String CHAP_ID, String GLOBAL_RANK) {
        Cursor c = getDataSCOREDATA(CHAP_ID);
        c.moveToFirst();
        String rowId = c.getString(0);
        c.close();
        ContentValues args = new ContentValues();
        args.put("GLOBAL_RANK", GLOBAL_RANK);
        return getWritableDatabase().update("SCOREDATA", args, "id='"+rowId+"'", null) > 0;
    }


    public boolean updateFullScore(String CHAP_ID, String GLOBAL_RANK,String TOTAL_SCORE,String CORRECT_QUES,String COUNT) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rowId = c.getString(0);
        c.close();
        ContentValues args = new ContentValues();
        args.put("GLOBAL_RANK", GLOBAL_RANK);
        args.put("TOTAL_SCORE", TOTAL_SCORE);
        args.put("CORRECT_QUES", CORRECT_QUES);
        args.put("COUNT", COUNT);
        return getWritableDatabase().update("SCOREDATA", args, "id='"+rowId+"'", null) > 0;
    }

    public boolean updateTotalCount(String CHAP_ID, String COUNT) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rowId = c.getString(0);
        c.close();
        ContentValues args = new ContentValues();
        args.put("COUNT", COUNT);
        return getWritableDatabase().update("SCOREDATA", args, "id='"+rowId+"'", null) > 0;
    }

    public boolean updateTotalCorrect(String CHAP_ID, String CORRECT_QUES) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rowId = c.getString(0);
        c.close();
        ContentValues args = new ContentValues();
        args.put("CORRECT_QUES", CORRECT_QUES);
        return getWritableDatabase().update("SCOREDATA", args, "id='"+rowId+"'", null) > 0;
    }

    public boolean updateWin(String CHAP_ID) {
        try
        {
            Cursor c = getDataSCOREDATA( CHAP_ID);
            c.moveToFirst();
            String rowId = c.getString(0);
            c.close();
            ContentValues args = new ContentValues();
            args.put("WIN", String.valueOf(Integer.valueOf(getWin(CHAP_ID)) + 1));
            return getWritableDatabase().update("SCOREDATA", args, "id='"+rowId+"'", null) > 0;
        }catch (Exception e){
            return false;
        }
    }

    public boolean updateLoose(String CHAP_ID) {
        try
        {
            Cursor c = getDataSCOREDATA( CHAP_ID);
            c.moveToFirst();
            String rowId = c.getString(0);
            c.close();
            ContentValues args = new ContentValues();
            args.put("LOOSE", String.valueOf(Integer.valueOf(getLoose(CHAP_ID)) + 1));
            return getWritableDatabase().update("SCOREDATA", args, "id='"+rowId+"'", null) > 0;
        }catch (Exception e){
            return false;
        }
    }

    public String getScoreTotalAllCatagory()
    {
        Cursor c = getWritableDatabase().query( "SCOREDATA", new String[]{"TOTAL_SCORE"}, null, null, null,null, null, null);
        int score = 0;
        if(c.moveToFirst())
        {
            do {
                score = score + Integer.valueOf(c.getString(0));
            }while (c.moveToNext());
        }
        c.close();
        return String.valueOf(score);
    }

    public String getTitleProfileAllCatagory()
    {
        Cursor c = getWritableDatabase().query( "SCOREDATA", new String[]{"TOTAL_SCORE"}, null, null, null,null, null, null);
        int score = 0;
        int c1 = 0;
        if(c.moveToFirst())
        {
            do {
                if(Integer.valueOf(c.getString(0))>0)
                {
                    c1++;
                }
                score = score + Integer.valueOf(c.getString(0));

            }while (c.moveToNext());
        }
        c.close();

        try{

            score = score/c1;

        }catch (ArithmeticException ex){}
        catch (Exception ex){}



        if(score<=400)
        {
            return "Newbee";
        }
        else if(score>400 && score <=800)
        {
            return "Hustler";
        }
        else if(score>800 && score <=1200)
        {
            return "Pro";
        }
        else if(score>1200 && score <=1600)
        {
            return "Avenger";
        }
        else
        {
            return "Genius";
        }
    }

    public String getWinTotalAllCatagory()
    {
        Cursor c = getWritableDatabase().query( "SCOREDATA", new String[]{"WIN"}, null, null, null,null, null, null);
        int score = 0;
        if(c.moveToFirst())
        {
            do {
                score = score + Integer.valueOf(c.getString(0));
            }while (c.moveToNext());
        }
        c.close();
        return String.valueOf(score);
    }
    public String getLooseTotalAllCatagory()
    {
        Cursor c = getWritableDatabase().query( "SCOREDATA", new String[]{"LOOSE"}, null, null, null,null, null, null);
        int score = 0;
        if(c.moveToFirst())
        {
            do {
                score = score + Integer.valueOf(c.getString(0));
            }while (c.moveToNext());
        }
        c.close();
        return String.valueOf(score);
    }

    public String getKillTimeTotalAllCatagory()
    {
        Cursor c = getWritableDatabase().query( "SCOREDATA", new String[]{"KILL_TIME"}, null, null, null,null, null, null);
        float score = 0;
        int c1 = 0;
        if(c.moveToFirst())
        {
            do {
                score = score + Float.valueOf(c.getString(0));
                if(Float.valueOf(c.getString(0))>0){c1++;}
            }while (c.moveToNext());
        }

        score =  (score/c1);
        c.close();

        if(String.valueOf(score).length()>4)
        {
            return String.valueOf(score).substring(0,4);
        }
        else
        {
            return String.valueOf(score);
        }
    }

    public String getTotalWinAllCatagory()
    {
        Cursor c = getWritableDatabase().query( "SCOREDATA", new String[]{"WIN"}, null, null, null,null, null, null);
        int score = 0;
        if(c.moveToFirst())
        {
            do {
                score = score + Integer.valueOf(c.getString(0));
            }while (c.moveToNext());
        }
        c.close();
        return String.valueOf(score);
    }

    public String getTotalLooseAllCatagory()
    {
        Cursor c = getWritableDatabase().query( "SCOREDATA", new String[]{"LOOSE"}, null, null, null,null, null, null);
        int score = 0;
        if(c.moveToFirst())
        {
            do {
                score = score + Integer.valueOf(c.getString(0));
            }while (c.moveToNext());
        }
        c.close();
        return String.valueOf(score);
    }

    /**
     * function to store data of friends, following parameters are required :
     *
     * @param USER_CODE user_code of user
     * @param CHAP_ID chapter id
     * @param GLOBAL_RANK global rank count
     * @param TOTAL_SCORE total score count
     * @param WIN win game count
     * @param LOOSE loose game count
     * @param KILL_TIME kill time of user
     * @param TAG tag of user like New bee ,Hustler ,Pro ,Avenger ,Genius
     *
     */
    public void insertDataOfFriend(String USER_CODE,String CHAP_ID,String GLOBAL_RANK,String TOTAL_SCORE,String WIN,String LOOSE,String KILL_TIME,String TAG)
    {
        Cursor c = getWritableDatabase().query("FRIENDS_SCORE_DATA", null, "USER_CODE=? AND CHAP_ID=?" ,new String[] {USER_CODE,CHAP_ID}, null, null, null, null);

        if(c.moveToFirst()) //record exhist
        {
            String rowId = c.getString(0);
            c.close();
            ContentValues args = new ContentValues();
            args.put("GLOBAL_RANK", GLOBAL_RANK);
            args.put("TOTAL_SCORE", TOTAL_SCORE);
            args.put("WIN", WIN);
            args.put("LOOSE", LOOSE);
            args.put("KILL_TIME", KILL_TIME);
            args.put("TAG", TAG);
            getWritableDatabase().update("FRIENDS_SCORE_DATA", args, "id='" + rowId + "'", null);
            Log.w("DANGER", "updated successfully");
        }
        else
        {
            c.close();
            ContentValues cv = new ContentValues();
            cv.put("USER_CODE", USER_CODE);
            cv.put("CHAP_ID", CHAP_ID);
            cv.put("GLOBAL_RANK", GLOBAL_RANK);
            cv.put("TOTAL_SCORE", TOTAL_SCORE);
            cv.put("WIN", WIN);
            cv.put("LOOSE", LOOSE);
            cv.put("KILL_TIME", KILL_TIME);
            try
            {
                getWritableDatabase().insertOrThrow("FRIENDS_SCORE_DATA", null, cv);
                Log.w("DANGER", "inserted successfully");
            }catch (SQLiteConstraintException e)
            {

            }catch (Exception e){

            }
        }
    }


    public Cursor getDataSCOREDATA(String CHAP_ID) {
        Cursor c = getWritableDatabase().query( "SCOREDATA", null,"CHAP_ID"+"=?", new String[] { CHAP_ID }, null,null, null, null);
        return c;
    }



    public String getGlobalRank(String CHAP_ID) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rank = c.getString(c.getColumnIndex("GLOBAL_RANK"));
        c.close();
        return rank;
    }

    public String getTotalScore(String CHAP_ID) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rank = c.getString(c.getColumnIndex("TOTAL_SCORE"));
        c.close();
        return rank;
    }

    public String getTotalCount(String CHAP_ID) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rank = c.getString(c.getColumnIndex("COUNT"));
        c.close();
        return rank;
    }


    public Cursor getFriendsList() {

        Cursor c = getWritableDatabase().rawQuery("select * from FRIENDSDATA", null);

        return c;

    }


    public Cursor getUpdatesQUIZDATA() {

        Cursor c = getWritableDatabase().rawQuery("select * from QUIZDATA", null);

        return c;
    }


    public Cursor getListOfChaptersInCatagory(String CATAGORY_VAL)
    {
        return getWritableDatabase().query("QUIZDATA",new String[]{"CATAGORY","CATAGORY_NAME","CHAP_ID","CHAP_NAME","CHAP_DES","LOCK"},"CATAGORY "+"=?",new String[]{CATAGORY_VAL},null,null,null,null);
    }

    public Cursor getListOfCatagories(){

        return getWritableDatabase().query("QUIZDATA",new String[]{"CATAGORY","CATAGORY_NAME"},null,null,null,null,"CATAGORY "+"DESC",null);
    }

    public Cursor getUpdatesSCOREDATA() {

        Cursor c = getWritableDatabase().rawQuery("select * from SCOREDATA", null);

        return c;
    }

    public String getTotalCorrect(String CHAP_ID) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rank = c.getString(c.getColumnIndex("CORRECT_QUES"));
        c.close();
        return rank;
    }

    public boolean updateKillTime(String CHAP_ID, String KILL_TIME) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rowId = c.getString(0);
        c.close();
        ContentValues args = new ContentValues();
        args.put("KILL_TIME", KILL_TIME);
        return getWritableDatabase().update("SCOREDATA", args, "id='"+rowId+"'", null) > 0;
    }


    public String getWin(String CHAP_ID) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rank = c.getString(c.getColumnIndex("WIN"));
        c.close();
        return rank;
    }

    public String getLoose(String CHAP_ID) {
        Cursor c = getDataSCOREDATA( CHAP_ID);
        c.moveToFirst();
        String rank = c.getString(c.getColumnIndex("LOOSE"));
        c.close();
        return rank;
    }

    /**
     * Insert data into user-friends table
     *
     * @param F_ID  facebook id of friend
     * @param USER_CODE  user code of friend
     * @return 1 if insertion is OK else 0
     */
    public int insertUserFriendsTable(String F_ID, String USER_CODE) {
        try
        {
            ContentValues cv = new ContentValues();
            cv.put("F_ID", F_ID);
            cv.put("USER_CODE", USER_CODE);
            getWritableDatabase().insertOrThrow("USER_FRIENDS_TABLE", null, cv);
            return 1;
        } catch (SQLiteConstraintException e)
        {
            return 0;
        }catch (Exception e)
        {
            return 0;
        }
    }

    /**
     * Get data of all user code of friends in user-friend table
     *
     * @return Cursor of all friends user codes
     */

    public Cursor getUserCodesFriends()
    {
        return getWritableDatabase().query("USER_FRIENDS_TABLE", new String[]{"USER_CODE"}, null, null, null,null, null, null);
    }

    public Cursor getFBIdFriends()
    {
        return getWritableDatabase().query("USER_FRIENDS_TABLE", new String[]{"F_ID"}, null, null, null,null, null, null);
    }

    public String getUserCode(String F_ID) {
        Cursor c = getWritableDatabase().query( "USER_FRIENDS_TABLE", new String[]{"USER_CODE"},"F_ID"+"=?", new String[] { F_ID }, null,null, null, null);
        try
        {
            if(c.moveToFirst())
            {
                return c.getString(0);
            }
            else
            {
                return "null";
            }
        }finally {
            c.close();
        }
    }

    public String getFBIdWithUserCode(String USER_CODE) {
        Cursor c = getWritableDatabase().query( "USER_FRIENDS_TABLE", new String[]{"F_ID"},"USER_CODE"+"=?", new String[] { USER_CODE }, null,null, null, null);
        try
        {
            if(c.moveToFirst())
            {
                return c.getString(0);
            }
            else
            {
                return "null";
            }
        }finally {
            c.close();
        }
    }

    public String getNamedWithFbID(String FB_ID) {
        Cursor c = getWritableDatabase().query( "FRIENDSDATA", new String[]{"Name"},"Uid"+"=?", new String[] { FB_ID }, null,null, null, null);
        try
        {
            if(c.moveToFirst())
            {
                return c.getString(0);
            }
            else
            {
                return "null";
            }
        }finally {
            c.close();
        }
    }

    public String getTotalScoreFriendsScoreData(String USER_CODE,String CHAP_ID) {
        Cursor c = getWritableDatabase().query( "FRIENDS_SCORE_DATA", new String[]{"TOTAL_SCORE"},"USER_CODE=? AND CHAP_ID=?", new String[] { USER_CODE, CHAP_ID }, null,null, null, null);
        try
        {
            if(c.moveToFirst())
            {
                return c.getString(0);
            }
            else
            {
                return "0";
            }
        }finally {
            c.close();
        }
    }
    public Cursor getFullRecordScoreFriendsScoreData(String USER_CODE,String CHAP_ID) {
        return getWritableDatabase().query( "FRIENDS_SCORE_DATA", null,"USER_CODE=? AND CHAP_ID=?", new String[] { USER_CODE, CHAP_ID }, null,null, null, null);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int arg1, int arg2) {

        //Log.w("DANGER","onUpgrade CALLED");
    }

    // not null UNIQUE

    public boolean updateListOfLock(String CATAGORY)
    {
        Cursor c = getListOfChaptersInCatagory(CATAGORY);
        int val = 1;
        String chapter_id = "";
        if(c.moveToFirst())
        {
            do{
                if(c.getInt(c.getColumnIndex("LOCK")) == 0)
                {
                    chapter_id = c.getString(c.getColumnIndex("CHAP_ID"));
                    val = 0;
                }
            }while (val==1 && c.moveToNext());
            return updateLock(chapter_id,1);
        }
        else
        {
            return false;
        }
    }

    public boolean updateLock(String CHAP_ID, int LOCK) {
        ContentValues args = new ContentValues();
        args.put("LOCK", LOCK);
        return getWritableDatabase().update("QUIZDATA", args, "CHAP_ID='"+CHAP_ID+"'", null) > 0;
    }
}
