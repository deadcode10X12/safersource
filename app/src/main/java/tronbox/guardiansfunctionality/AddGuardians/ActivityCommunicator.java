package tronbox.guardiansfunctionality.AddGuardians;

import java.util.List;

/**
 * Created by Shrinivas Mishra on 3/29/2015.
 */
public interface ActivityCommunicator {
    public void SelectedContacts(List<ContactDetails> someValue, int pos);
}
