package tronbox.guardiansfunctionality.AddGuardians;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import tronbox.guardiansfunctionality.SharedPrefrenceStorage;

public class AddGuardiansServer extends AsyncTask<String, String, String>{

    private Context context;
    private String verificationCode;

    public AddGuardiansServer(Context context){

        this.context = context;

    }

    /*
        params[0] = url where data needs to be send.
        params[1] = Data to be sent.

     */


    @Override
    protected String doInBackground(String... params) {

        try{

            HttpURLConnection httpUrlConnection = null;
            URL url = new URL("http://54.169.182.72/api/v0/guardians");
            httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setUseCaches(false);
            httpUrlConnection.setDoOutput(true);

            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
            httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
            httpUrlConnection.addRequestProperty("Content-Type","application/json");
            httpUrlConnection.addRequestProperty("authorization","bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NTAxN2FkZTNiZDMxYzE5NTdhZWUwYWMiLCJleHAiOiIyMDE1LTAzLTEyVDExOjM5OjEwLjMyMFoifQ.p97xSG1ADIwXvJNTLUanc0qCVhZdbgMn01hOXjmoBiA");
            httpUrlConnection.setDoOutput(true);

            JSONArray array = new JSONArray();
            array.put("7838591951");

            String buffer = "{guardians: "+String.valueOf(array)+"}";


            OutputStreamWriter wr = new OutputStreamWriter(httpUrlConnection.getOutputStream());
            wr.write(buffer);
            wr.flush();



            InputStream responseStream = new BufferedInputStream(httpUrlConnection.getErrorStream());

            Log.w("ServerResponseFromServer", "Processing Data Wait !!!");

            BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));

            Log.w("ServerResponseFromServer", responseStreamReader.readLine());

            String line = "";
            StringBuilder stringBuilder = new StringBuilder();



            while ((line = responseStreamReader.readLine()) != null)
            {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();

            String response = stringBuilder.toString();
            Log.w("ServerResponseFromServer", response);

            JSONObject jsonObject = new JSONObject(response);




        } catch(IOException io){}
          catch (JSONException e) {e.printStackTrace();}

        return null;
    }

}
