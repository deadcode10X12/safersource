package tronbox.guardiansfunctionality.AddGuardians;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tronbox.guardiansfunctionality.Home.CircleImagePhoto;
import tronbox.guardiansfunctionality.R;

public class DummyContactsList extends ArrayAdapter<ContactDetails>{

    private Context context;
    private List<ContactDetails> list;
    private SparseBooleanArray SelectedRowIds;
    private int resource;
    private Set<Integer> set = new HashSet<Integer>();
    public DummyContactsList(Context context, int resource, List<ContactDetails> objects)
    {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.list = objects;
        SelectedRowIds = new SparseBooleanArray();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        View view = LayoutInflater.from(context).inflate(resource, parent, false);
        view.setBackgroundColor(Color.parseColor("#00000000"));
        if(SelectedRowIds.get(position)==true)
        {
            view.setBackgroundColor(Color.parseColor("#800e75b3"));
        }

        Log.e("ListChange",String.valueOf(position));
        ContactDetails holder = list.get(position);

        //TextView name = (TextView)view.findViewById(R.id.friend_name);
        TextView name = (TextView)view.findViewById(R.id.txtContactName);
        name.setText(holder.name);

        //TextView mobile = (TextView)view.findViewById(R.id.mobile);
        TextView mobile = (TextView)view.findViewById(R.id.txtContactMobile);
        mobile.setText(holder.mobile);

        try {
            FileInputStream inputStream = context.openFileInput(holder.getId());

            ((CircleImagePhoto)view.findViewById(R.id.imgContactPhoto)).setImageBitmap(BitmapFactory.decodeStream(inputStream));


        } catch (FileNotFoundException e) {
            ((CircleImagePhoto) view.findViewById(R.id.imgContactPhoto)).setImageBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.dummy_image));
        }

        return view;
    }

    @Override
    public int getCount() {

        return list.size();
    }

    public void addToSet(int value)
    {
        if(set.contains(value))
        {
            set.remove(value);
        }else
        {
            set.add(value);
        }
        notifyDataSetChanged();
    }
    public Set<Integer> getSelectedList()
    {
        return set;
    }
    public void toggleRowSelection(int position) {
        selectRowView(position, !SelectedRowIds.get(position));
    }
    public void removeRowSelection() {
        SelectedRowIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }
    public void selectRowView(int position, boolean value)
    {
        if (value)
            SelectedRowIds.put(position, value);
        else
            SelectedRowIds.delete(position);
        notifyDataSetChanged();
    }

    public int getSelectedRowCount() {
        return SelectedRowIds.size();
    }

    public SparseBooleanArray getSelectedRowIds() {
        return SelectedRowIds;
    }
}
