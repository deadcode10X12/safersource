package tronbox.guardiansfunctionality.AddGuardians;

import tronbox.guardiansfunctionality.StaticMaster;

public class GuardianDetails {

    private String id, name, mobile, image, status, app, location, remarks, distance;

    private StaticMaster.Category category;

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMobile() {
        return mobile;
    }

    public String getImage() {
        return image;
    }

    public String getStatus() {
        return status;
    }

    public String getApp() {
        return app;
    }

    public String getLocation() {
        return location;
    }
    public String getRemarks(){return remarks;}
    public String getDistance(){return distance;}
    public void setDistance(String distance) {this.distance=distance;}

    public StaticMaster.Category getCategory() {
        return category;
    }

    public void setCategory(StaticMaster.Category category) {
        this.category = category;
    }
}
