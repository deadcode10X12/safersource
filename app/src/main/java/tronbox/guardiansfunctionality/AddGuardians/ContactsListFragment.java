package tronbox.guardiansfunctionality.AddGuardians;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import tronbox.guardiansfunctionality.Login.CommonFunctions;
import tronbox.guardiansfunctionality.Login.ContactListActivity;
import tronbox.guardiansfunctionality.R;
import tronbox.guardiansfunctionality.StaticMaster;

public class ContactsListFragment extends Fragment implements LoaderCallbacks<Cursor> {

    ContactsAdapter adapters;
    ContactsList contactsListAdapter;
    private Set<Integer> set = new HashSet<Integer>();
    Bundle data;
    int screenValue;
    CommonFunctions comFunc = new CommonFunctions();

    HashMap<String, String> sparseArray = new HashMap<String, String>();

    MobileDatabase database;

    ArrayList<ContactDetails> detailsArrayList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);

        database = new MobileDatabase(getActivity(), "GUARDIAN", null, 1);
        data = getArguments();
        screenValue = data.getInt("Screen");

        detailsArrayList.clear();

        contactsListAdapter = new ContactsList(getActivity(), R.layout.custom_gurdian_list, detailsArrayList);
        adapters = new ContactsAdapter(getActivity());

        final ListView friendsListView = (ListView) view.findViewById(R.id.lstContact);

        friendsListView.setAdapter(contactsListAdapter);
        friendsListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        friendsListView.setMultiChoiceModeListener(new MultiChoiceModeListener()
        {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
            {
                final int checkedcount= friendsListView.getCheckedItemCount();
                mode.setTitle(checkedcount + " Selected");

                Log.d(StaticMaster.log, "Selected "+checkedcount);

                contactsListAdapter.toggleRowSelection(position);
            }
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu)
            {
                mode.getMenuInflater().inflate(R.menu.menu_activity, menu);
                return true;
            }
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu)
            {
                return false;
            }
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.delete:
                        SparseBooleanArray selected = contactsListAdapter.getSelectedRowIds();
                        for (int i = (selected.size() - 1); i >= 0; i--) {
                            if (selected.valueAt(i)) {
                                ContactDetails selectedrow = contactsListAdapter.getItem(selected.keyAt(i));
                                contactsListAdapter.remove(selectedrow);
                            }
                        }
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                contactsListAdapter.removeRowSelection();
            }
        });
        getLoaderManager().initLoader(0, null, this);

      /*  ((TextView)view1.findViewById(R.id.add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONArray array = new JSONArray();


                ArrayList<ContactDetails> detailsArray = new ArrayList<ContactDetails>();

                for(Integer integer : contactsListAdapter.getSelectedList()){

                    JSONObject object = new JSONObject();



                    ContactDetails detail = detailsArrayList.get(integer-1);
                    try {

                        object.put("mobile", detail.mobile);

                        array.put(detail.mobile);

                        int value = database.insertGuardian(detail.name, detail.mobile, detail.picUrl, "pending", "no");
                        int value2 = database.insertChildren(detail.name, detail.mobile, detail.picUrl);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    detailsArray.add(detail);

                }


                String buffer = "{guardians: "+String.valueOf(array)+"}";


                new AddGuardiansServer(getActivity()).execute("http://54.169.182.72/api/v0/guardians", buffer);


                Log.w("JsonResponseeeeeee", buffer);


                contactsListAdapter.getSelectedList().clear();
                detailsArrayList.clear();
                detailsArrayList.addAll(detailsArray);

                contactsListAdapter.notifyDataSetChanged();
  /*              boolean status1 = database.updateGuardianStatus("9899972269", "confirmed", 4);
                boolean status2 = database.updateGuardianStatus("9899972269", "yes", 5);

*//**/
/*
                GuardianDetails g = database.getGuardianFromMobile("9899972269");
                Log.w("DatabaseEntry", g.getName()+" "+g.getMobile() + " "+ " " +g.getStatus() + " " + g.getApp());
                if(g.getImage() != null)
                {
                    Log.w("DatabaseEntry", "Image url "+g.getImage());
                }
*/
/*
                Log.w("DatabaseEntryChildren", "Image url "+status1+" "+status2);
*/
/*
                  for(GuardianDetails g : database.getAllChildren())
                {
                    Log.w("DatabaseEntry", g.getName()+" "+g.getMobile()); //  + " "+ " " +g.getStatus() + " " + g.getApp());
                     if(g.getImage() != null)
                    {
                        Log.w("DatabaseEntry", "Image url "+g.getImage());
                    }
                }
            }
        });*/

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


    }

    @Override
    public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1)
    {
        Uri phone = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] PHONE_PROJECTION = new String[]
                {
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.DATA1,
                        ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI,
                        ContactsContract.CommonDataKinds.Phone._ID,
                };
        CursorLoader loader = new CursorLoader(getActivity(), phone, PHONE_PROJECTION, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor)
    {
        Log.w("SwapData", "Invoked 1");
        if(cursor.moveToFirst())
        {
            do
            {
                if(cursor.getString(1).length() >= 10 && cursor.getString(1).length() <= 13)
                {
                    String name = cursor.getString(0); //name
                    String mobile = cursor.getString(1); // mobile
                    String url = cursor.getString(2); // url
                    if(mobile != null)
                    {
                        sparseArray.put(mobile, name + "@" + mobile + "@" + url);
                    }
                }
            }while(cursor.moveToNext());
        }
        Set<String> keyset = sparseArray.keySet();
        ArrayList<String> sortList = new ArrayList<String>();
        sortList.clear();
        if(keyset.size() > 0)
        {
            for(String key : keyset)
            {
                Log.w("KeySettttttttt", key);
                if(sparseArray.containsKey(key))
                {
                    sortList.add(sparseArray.get(key));
                    Log.w("KeySet", sparseArray.get(key));
                }
            }
        }
        else
        {
            Log.w("KeySet", "Empty");
        }
        Collections.sort(sortList, new Comparator<String>()
        {
            @Override
            public int compare(String s1, String s2)
            {
                return s1.compareToIgnoreCase(s2);
            }
        });
        int bTrue=1;
        for(String value : sortList)
        {
            String[] values =  value.split("@");
            ContactDetails details = new ContactDetails();
            details.name = values[0];
            details.mobile = values[1];
            details.picUrl = values[2];
            if (bTrue%2==1)
            {
                details.color="2";//"#dedede";
            }
            else
            {
                details.color="1";
            }
            detailsArrayList.add(details);
            contactsListAdapter.notifyDataSetChanged();
        }
        adapters.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0)
    {
        adapters.swapCursor(null);
    }

      public void show()
    {

         for(int i : contactsListAdapter.getSelectedList())
       {
         ContactDetails details = detailsArrayList.get(i);

          Log.d(StaticMaster.log, details.getName());
       }

    }
}
