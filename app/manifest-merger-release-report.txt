-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionCode
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.BLUETOOTH
ADDED from AndroidManifest.xml:5:5
	android:name
		ADDED from AndroidManifest.xml:5:22
uses-permission#android.permission.BLUETOOTH_ADMIN
ADDED from AndroidManifest.xml:6:5
	android:name
		ADDED from AndroidManifest.xml:6:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:7:5
	android:name
		ADDED from AndroidManifest.xml:7:22
uses-permission#android.permission.READ_PROFILE
ADDED from AndroidManifest.xml:8:5
	android:name
		ADDED from AndroidManifest.xml:8:22
uses-permission#android.permission.READ_CONTACTS
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.RECEIVE_SMS
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-permission#android.permission.READ_SMS
ADDED from AndroidManifest.xml:15:5
	android:name
		ADDED from AndroidManifest.xml:15:22
uses-permission#android.permission.SEND_SMS
ADDED from AndroidManifest.xml:16:5
	android:name
		ADDED from AndroidManifest.xml:16:22
uses-permission#android.permission.GET_ACCOUNTS
ADDED from AndroidManifest.xml:17:5
	android:name
		ADDED from AndroidManifest.xml:17:22
uses-permission#android.permission.USE_CREDENTIALS
ADDED from AndroidManifest.xml:18:5
	android:name
		ADDED from AndroidManifest.xml:18:22
uses-permission#android.permission.ACCESS_WIFI_STATE
ADDED from AndroidManifest.xml:19:5
	android:name
		ADDED from AndroidManifest.xml:19:22
uses-permission#android.permission.WIFI_STATE_CHANGED
ADDED from AndroidManifest.xml:21:5
	android:name
		ADDED from AndroidManifest.xml:21:22
uses-permission#android.permission.CALL_PHONE
ADDED from AndroidManifest.xml:22:5
	android:name
		ADDED from AndroidManifest.xml:22:22
permission#android.permission.FLASHLIGHT
ADDED from AndroidManifest.xml:24:5
	android:protectionLevel
		ADDED from AndroidManifest.xml:26:9
	android:permissionGroup
		ADDED from AndroidManifest.xml:25:9
	android:name
		ADDED from AndroidManifest.xml:24:17
uses-permission#android.permission.CAMERA
ADDED from AndroidManifest.xml:29:5
	android:name
		ADDED from AndroidManifest.xml:29:22
uses-feature#android.hardware.camera
ADDED from AndroidManifest.xml:31:5
	android:required
		ADDED from AndroidManifest.xml:33:9
	android:name
		ADDED from AndroidManifest.xml:32:9
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:35:5
	android:name
		ADDED from AndroidManifest.xml:35:22
uses-permission#android.permission.ACCESS_COARSE_LOCATION
ADDED from AndroidManifest.xml:36:5
	android:name
		ADDED from AndroidManifest.xml:36:22
uses-feature#0x00020000
ADDED from AndroidManifest.xml:38:5
	android:required
		ADDED from AndroidManifest.xml:40:9
	android:glEsVersion
		ADDED from AndroidManifest.xml:39:9
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:42:5
	android:name
		ADDED from AndroidManifest.xml:42:22
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:47:5
	android:name
		ADDED from AndroidManifest.xml:47:22
uses-permission#com.google.android.c2dm.permission.RECEIVE
ADDED from AndroidManifest.xml:48:5
	android:name
		ADDED from AndroidManifest.xml:48:22
permission#tronbox.guardiansfunctionality.permission.C2D_MESSAGE
ADDED from AndroidManifest.xml:50:5
	android:protectionLevel
		ADDED from AndroidManifest.xml:52:9
	android:name
		ADDED from AndroidManifest.xml:51:9
uses-permission#tronbox.guardiansfunctionality.permission.C2D_MESSAGE
ADDED from AndroidManifest.xml:54:5
	android:name
		ADDED from AndroidManifest.xml:54:22
application
ADDED from AndroidManifest.xml:57:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.google.android.gms:play-services:6.5.87:20:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:recyclerview-v7:21.0.3:17:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.facebook.android:facebook:3.23.0:24:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.google.maps.android:android-maps-utils:0.3.4:7:5
	android:label
		ADDED from AndroidManifest.xml:60:9
	android:allowBackup
		ADDED from AndroidManifest.xml:58:9
	android:icon
		ADDED from AndroidManifest.xml:59:9
	android:theme
		ADDED from AndroidManifest.xml:61:9
receiver#tronbox.guardiansfunctionality.Login.GcmBroadcastReceiver
ADDED from AndroidManifest.xml:62:9
	android:permission
		ADDED from AndroidManifest.xml:64:13
	android:name
		ADDED from AndroidManifest.xml:63:13
intent-filter#com.example.gcm+com.google.android.c2dm.intent.RECEIVE
ADDED from AndroidManifest.xml:65:13
action#com.google.android.c2dm.intent.RECEIVE
ADDED from AndroidManifest.xml:66:17
	android:name
		ADDED from AndroidManifest.xml:66:25
category#com.example.gcm
ADDED from AndroidManifest.xml:67:17
	android:name
		ADDED from AndroidManifest.xml:67:27
receiver#tronbox.guardiansfunctionality.Login.BluetoothStateReciever
ADDED from AndroidManifest.xml:71:9
	android:name
		ADDED from AndroidManifest.xml:71:19
intent-filter#android.bluetooth.adapter.action.STATE_CHANGED+android.intent.action.ACTION_BATTERY_LOW+android.intent.action.ACTION_BATTERY_OKAY+android.intent.action.ACTION_POWER_CONNECTED+android.intent.action.ACTION_POWER_DISCONNECTED+android.intent.action.BATTERY_CHANGED+android.location.PROVIDERS_CHANGED+android.net.conn.CONNECTIVITY_CHANGE+android.net.wifi.WIFI_STATE_CHANGED
ADDED from AndroidManifest.xml:72:13
action#android.bluetooth.adapter.action.STATE_CHANGED
ADDED from AndroidManifest.xml:73:17
	android:name
		ADDED from AndroidManifest.xml:73:25
action#android.net.conn.CONNECTIVITY_CHANGE
ADDED from AndroidManifest.xml:74:17
	android:name
		ADDED from AndroidManifest.xml:74:25
action#android.net.wifi.WIFI_STATE_CHANGED
ADDED from AndroidManifest.xml:75:17
	android:name
		ADDED from AndroidManifest.xml:75:25
action#android.location.PROVIDERS_CHANGED
ADDED from AndroidManifest.xml:76:17
	android:name
		ADDED from AndroidManifest.xml:76:25
action#android.intent.action.BATTERY_CHANGED
ADDED from AndroidManifest.xml:77:17
	android:name
		ADDED from AndroidManifest.xml:77:25
action#android.intent.action.ACTION_POWER_CONNECTED
ADDED from AndroidManifest.xml:78:17
	android:name
		ADDED from AndroidManifest.xml:78:25
action#android.intent.action.ACTION_POWER_DISCONNECTED
ADDED from AndroidManifest.xml:79:17
	android:name
		ADDED from AndroidManifest.xml:79:25
action#android.intent.action.ACTION_BATTERY_LOW
ADDED from AndroidManifest.xml:80:17
	android:name
		ADDED from AndroidManifest.xml:80:25
action#android.intent.action.ACTION_BATTERY_OKAY
ADDED from AndroidManifest.xml:81:17
	android:name
		ADDED from AndroidManifest.xml:81:25
receiver#tronbox.guardiansfunctionality.Device.IncommingCallHandler
ADDED from AndroidManifest.xml:84:9
	android:name
		ADDED from AndroidManifest.xml:84:19
intent-filter#android.intent.action.PHONE_STATE
ADDED from AndroidManifest.xml:85:13
action#android.intent.action.PHONE_STATE
ADDED from AndroidManifest.xml:86:17
	android:name
		ADDED from AndroidManifest.xml:86:25
service#tronbox.guardiansfunctionality.Login.GcmIntentService
ADDED from AndroidManifest.xml:90:9
	android:name
		ADDED from AndroidManifest.xml:90:18
meta-data#com.facebook.sdk.ApplicationId
ADDED from AndroidManifest.xml:92:9
	android:name
		ADDED from AndroidManifest.xml:93:13
	android:value
		ADDED from AndroidManifest.xml:94:13
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:95:9
MERGED from com.google.android.gms:play-services:6.5.87:21:9
	android:name
		ADDED from AndroidManifest.xml:96:13
	android:value
		ADDED from AndroidManifest.xml:97:13
meta-data#com.google.android.maps.v2.API_KEY
ADDED from AndroidManifest.xml:98:9
	android:name
		ADDED from AndroidManifest.xml:99:13
	android:value
		ADDED from AndroidManifest.xml:100:13
activity#tronbox.guardiansfunctionality.Login.SplashActivity
ADDED from AndroidManifest.xml:104:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:107:13
	android:label
		ADDED from AndroidManifest.xml:106:13
	android:name
		ADDED from AndroidManifest.xml:105:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:110:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:111:17
	android:name
		ADDED from AndroidManifest.xml:111:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:112:17
	android:name
		ADDED from AndroidManifest.xml:112:27
activity#tronbox.guardiansfunctionality.Login.MobileVerificationActivity
ADDED from AndroidManifest.xml:116:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:119:13
	android:label
		ADDED from AndroidManifest.xml:118:13
	android:name
		ADDED from AndroidManifest.xml:117:13
activity#tronbox.guardiansfunctionality.Login.MobileVerificationCodeActivity
ADDED from AndroidManifest.xml:121:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:124:13
	android:label
		ADDED from AndroidManifest.xml:123:13
	android:name
		ADDED from AndroidManifest.xml:122:13
activity#tronbox.guardiansfunctionality.Login.UserRegisterActivity
ADDED from AndroidManifest.xml:126:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:129:13
	android:label
		ADDED from AndroidManifest.xml:128:13
	android:name
		ADDED from AndroidManifest.xml:127:13
activity#tronbox.guardiansfunctionality.Login.DeviceSyncActivity
ADDED from AndroidManifest.xml:131:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:134:13
	android:label
		ADDED from AndroidManifest.xml:133:13
	android:name
		ADDED from AndroidManifest.xml:132:13
activity#tronbox.guardiansfunctionality.Login.ContactListActivity
ADDED from AndroidManifest.xml:136:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:139:13
	android:label
		ADDED from AndroidManifest.xml:138:13
	android:name
		ADDED from AndroidManifest.xml:137:13
activity#tronbox.guardiansfunctionality.Home.HomeActivity
ADDED from AndroidManifest.xml:141:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:144:13
	android:label
		ADDED from AndroidManifest.xml:143:13
	android:name
		ADDED from AndroidManifest.xml:142:13
activity#tronbox.guardiansfunctionality.Home.UserProfileActivity
ADDED from AndroidManifest.xml:146:9
	android:windowSoftInputMode
		ADDED from AndroidManifest.xml:150:13
	android:screenOrientation
		ADDED from AndroidManifest.xml:149:13
	android:label
		ADDED from AndroidManifest.xml:148:13
	android:name
		ADDED from AndroidManifest.xml:147:13
activity#tronbox.guardiansfunctionality.Maps.ShareLocationActivity
ADDED from AndroidManifest.xml:157:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:160:13
	android:label
		ADDED from AndroidManifest.xml:159:13
	android:name
		ADDED from AndroidManifest.xml:158:13
activity#tronbox.guardiansfunctionality.Device.DeviceSettingActivity
ADDED from AndroidManifest.xml:162:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:165:13
	android:label
		ADDED from AndroidManifest.xml:164:13
	android:name
		ADDED from AndroidManifest.xml:163:13
activity#tronbox.guardiansfunctionality.Maps.SOS
ADDED from AndroidManifest.xml:170:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:173:13
	android:label
		ADDED from AndroidManifest.xml:172:13
	android:name
		ADDED from AndroidManifest.xml:171:13
activity#tronbox.guardiansfunctionality.MainActivity
ADDED from AndroidManifest.xml:175:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:178:13
	android:label
		ADDED from AndroidManifest.xml:177:13
	android:name
		ADDED from AndroidManifest.xml:176:13
activity#com.facebook.LoginActivity
ADDED from AndroidManifest.xml:183:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:186:13
	android:label
		ADDED from AndroidManifest.xml:185:13
	android:name
		ADDED from AndroidManifest.xml:184:13
activity#tronbox.guardiansfunctionality.Login.LoginActivity
ADDED from AndroidManifest.xml:187:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:190:13
	android:label
		ADDED from AndroidManifest.xml:189:13
	android:name
		ADDED from AndroidManifest.xml:188:13
activity#tronbox.guardiansfunctionality.Login.Dashboard
ADDED from AndroidManifest.xml:192:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:195:13
	android:label
		ADDED from AndroidManifest.xml:194:13
	android:name
		ADDED from AndroidManifest.xml:193:13
activity#tronbox.guardiansfunctionality.Maps.SafeWalkSetup
ADDED from AndroidManifest.xml:197:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:200:13
	android:label
		ADDED from AndroidManifest.xml:199:13
	android:name
		ADDED from AndroidManifest.xml:198:13
activity#tronbox.guardiansfunctionality.HandleGuardians.GuardianAlertActivity
ADDED from AndroidManifest.xml:202:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:205:13
	android:label
		ADDED from AndroidManifest.xml:204:13
	android:name
		ADDED from AndroidManifest.xml:203:13
service#tronbox.guardiansfunctionality.GuardianBrain
ADDED from AndroidManifest.xml:208:9
	android:name
		ADDED from AndroidManifest.xml:208:18
service#tronbox.guardiansfunctionality.Device.DeviceSyncService
ADDED from AndroidManifest.xml:209:9
	android:name
		ADDED from AndroidManifest.xml:209:18
activity#tronbox.guardiansfunctionality.Maps.ShareLocationConfirm
ADDED from AndroidManifest.xml:212:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:215:13
	android:label
		ADDED from AndroidManifest.xml:214:13
	android:name
		ADDED from AndroidManifest.xml:213:13
activity#tronbox.guardiansfunctionality.Maps.LaunchMap
ADDED from AndroidManifest.xml:217:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:220:13
	android:label
		ADDED from AndroidManifest.xml:219:13
	android:name
		ADDED from AndroidManifest.xml:218:13
activity#tronbox.guardiansfunctionality.Maps.MyCurrentLocation
ADDED from AndroidManifest.xml:222:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:225:13
	android:label
		ADDED from AndroidManifest.xml:224:13
	android:name
		ADDED from AndroidManifest.xml:223:13
activity#tronbox.guardiansfunctionality.Maps.SafeWalk
ADDED from AndroidManifest.xml:227:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:230:13
	android:label
		ADDED from AndroidManifest.xml:229:13
	android:name
		ADDED from AndroidManifest.xml:228:13
activity#tronbox.guardiansfunctionality.Login.DummyGuardianListActivity
ADDED from AndroidManifest.xml:232:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:235:13
	android:label
		ADDED from AndroidManifest.xml:234:13
	android:name
		ADDED from AndroidManifest.xml:233:13
activity#tronbox.guardiansfunctionality.Login.DummyNotificationHandler
ADDED from AndroidManifest.xml:237:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:240:13
	android:label
		ADDED from AndroidManifest.xml:239:13
	android:name
		ADDED from AndroidManifest.xml:238:13
activity#tronbox.guardiansfunctionality.Home.AboutUsActivity
ADDED from AndroidManifest.xml:242:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:245:13
	android:label
		ADDED from AndroidManifest.xml:244:13
	android:name
		ADDED from AndroidManifest.xml:243:13
activity#tronbox.guardiansfunctionality.Home.AlertActivity
ADDED from AndroidManifest.xml:247:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:250:13
	android:label
		ADDED from AndroidManifest.xml:249:13
	android:name
		ADDED from AndroidManifest.xml:248:13
activity#tronbox.guardiansfunctionality.Home.AlertCancelActivity
ADDED from AndroidManifest.xml:252:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:255:13
	android:label
		ADDED from AndroidManifest.xml:254:13
	android:name
		ADDED from AndroidManifest.xml:253:13
activity#tronbox.guardiansfunctionality.Home.GaurdianAnswerActivity
ADDED from AndroidManifest.xml:257:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:260:13
	android:label
		ADDED from AndroidManifest.xml:259:13
	android:name
		ADDED from AndroidManifest.xml:258:13
activity#tronbox.guardiansfunctionality.Home.GuardianQuestionActivity
ADDED from AndroidManifest.xml:262:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:265:13
	android:label
		ADDED from AndroidManifest.xml:264:13
	android:name
		ADDED from AndroidManifest.xml:263:13
activity#tronbox.guardiansfunctionality.AddGuardians.PendingGuardians
ADDED from AndroidManifest.xml:267:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:270:13
	android:label
		ADDED from AndroidManifest.xml:269:13
	android:name
		ADDED from AndroidManifest.xml:268:13
activity#tronbox.guardiansfunctionality.HandleGuardians.InvitePendingGuardians
ADDED from AndroidManifest.xml:272:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:275:13
	android:label
		ADDED from AndroidManifest.xml:274:13
	android:name
		ADDED from AndroidManifest.xml:273:13
activity#tronbox.guardiansfunctionality.Home.Recycle
ADDED from AndroidManifest.xml:277:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:280:13
	android:label
		ADDED from AndroidManifest.xml:279:13
	android:name
		ADDED from AndroidManifest.xml:278:13
activity#tronbox.guardiansfunctionality.Home.Selfie
ADDED from AndroidManifest.xml:282:9
	android:label
		ADDED from AndroidManifest.xml:284:13
	android:name
		ADDED from AndroidManifest.xml:283:13
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.android.gms:play-services:6.5.87:18:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:recyclerview-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.facebook.android:facebook:3.23.0:20:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.maps.android:android-maps-utils:0.3.4:5:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
activity#android.support.v7.widget.TestActivity
ADDED from com.android.support:recyclerview-v7:21.0.3:18:9
	android:label
		ADDED from com.android.support:recyclerview-v7:21.0.3:18:19
	android:name
		ADDED from com.android.support:recyclerview-v7:21.0.3:18:60
